#include "render.h"
#include <GLFW/glfw3.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include "linear_algebra.h"
#include "memory.h"
#include "camera.h"
#include "obj_loader.h"


typedef struct RenderObject{
    Vec3 colour;
    GLuint vao;
    GLuint textureID;
    uint32_t numVertices;
} RenderObject;

static GLFWwindow *window = NULL;
static GLuint program = 0;
static GLuint quadProgram = 0;

static RenderObject *renderObjects = NULL;
static size_t numRenderObjects = 0;
static float time = 0;
static float horizontalAngle = 0;
static float verticalAngle = 0;
static Vec3 pos = {.x = 0, .y = 1, .z = 2};
static Mat4 viewMatrix;
static Mat4 projectionMatrix;
static uint32_t renderMode = 0;

static void compile_shader(MemoryArena *arena, char *shader, GLuint id);

/*
 * initialises all OpenGL related things
 * compiles and links shaders
 */

void init_gl(uint32_t width, uint32_t height) {

    //initialise GLFW and create window

    if (glfwInit() == 0) {
        fprintf(stderr, "Error, failed to initialise GLFW\n");
        exit(EXIT_FAILURE);
    }

    glfwWindowHint(GLFW_SAMPLES, 1);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow((int) width, (int) height, "Mango Whale", NULL, NULL);
    if (window == NULL) {
        fprintf(stderr, "Error - unable to create OpenGl window\n");
        exit(EXIT_FAILURE);
    }

    glfwMakeContextCurrent(window);
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    glfwPollEvents();


    glewExperimental = GL_TRUE;
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        glfwTerminate();
    }

    //load and compile shaders
    MemoryArena *shaderArena = create_arena(1024 * 1024 * 64);
    {
        char *vertexShader = "shaders/simple.vert";
        char *fragmentShader = "shaders/simple.frag";

        GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
        GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

        compile_shader(shaderArena, vertexShader, vertexShaderID);
        compile_shader(shaderArena, fragmentShader, fragmentShaderID);


        //link shader programs
        program = glCreateProgram();

        GLint result = GL_FALSE;
        glAttachShader(program, vertexShaderID);
        glAttachShader(program, fragmentShaderID);
        glLinkProgram(program);
        glGetProgramiv(program, GL_LINK_STATUS, &result);

        if (result == GL_FALSE){
            fprintf(stderr, "Error linking shaders\n");
            exit(EXIT_FAILURE);
        }
    }

    {
        char *vertexShader = "shaders/quad.vert";
        char *fragmentShader = "shaders/quad.frag";

        GLuint vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
        GLuint fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

        compile_shader(shaderArena, vertexShader, vertexShaderID);
        compile_shader(shaderArena, fragmentShader, fragmentShaderID);


        //link shader programs
        quadProgram = glCreateProgram();

        GLint result = GL_FALSE;
        glAttachShader(quadProgram, vertexShaderID);
        glAttachShader(quadProgram, fragmentShaderID);
        glLinkProgram(quadProgram);
        glGetProgramiv(quadProgram, GL_LINK_STATUS, &result);

        if (result == GL_FALSE){
            fprintf(stderr, "Error linking shaders\n");
            exit(EXIT_FAILURE);
        }
    }

    time = (float) glfwGetTime();

    glEnable(GL_FRAMEBUFFER_SRGB);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.7f, 0.2f, 1.0f, 1.0f);

    delete_arena(shaderArena);
}

/*
 * opens and compiles a given shader file
 */

static void compile_shader(MemoryArena *arena, char *shader, GLuint id){

    FILE *file = fopen(shader, "r");
    if (file == NULL){
        fprintf(stderr, "Error opening file \"%s\"\n", shader);
        exit(EXIT_FAILURE);
    }

    //get size of file and allocate buffer
    fseek(file, 0, SEEK_END);
    size_t length = (size_t) ftell(file);
    char *buffer = Arena_Allocate(char, (sizeof *buffer * (length + 1)), arena);

    //read file into buffer
    fseek(file, 0, SEEK_SET);
    fread(buffer, 1, length, file);
    buffer[length] = '\0';
    fclose(file);

    glShaderSource(id, 1, (const GLchar* const*) &buffer, NULL);
    glCompileShader(id);

    GLint result = GL_FALSE;
    glGetShaderiv(id, GL_COMPILE_STATUS, &result);

    if (result == GL_FALSE){
        fprintf(stderr, "Error compiling shader \"%s\"\n", shader);
        exit(EXIT_FAILURE);
    }
}


/*
 * buffers data onto the gpu
 * operates on a sub section of the model, where the subsection is continuous and all triangles in that section are of the same material type
 */

RenderObject loadSubMesh(ObjData *data, size_t start, uint32_t size){

    GLuint vaoID, vertexBuffer, normalBuffer, textureBuffer;

    glGenBuffers(1, &vertexBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    size_t vertexBufferSize = sizeof(*data->vertices) * size;
    glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr) vertexBufferSize, NULL, GL_STATIC_DRAW);

    void *verticesPointer = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    memcpy(verticesPointer, data->vertices + start, vertexBufferSize);
    glUnmapBuffer(GL_ARRAY_BUFFER);

    glGenBuffers(1, &normalBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    size_t normalBufferSize = sizeof(*data->vertexNormals) * size;
    glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr) normalBufferSize, NULL, GL_STATIC_DRAW);

    void *normalsPointer = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    memcpy(normalsPointer, data->vertexNormals + start , normalBufferSize);
    glUnmapBuffer(GL_ARRAY_BUFFER);


    glGenBuffers(1, &textureBuffer);
    glBindBuffer(GL_ARRAY_BUFFER, textureBuffer);
    size_t texCoordsBufferSize = sizeof(*data->textureCoords) * size;
    glBufferData(GL_ARRAY_BUFFER, (GLsizeiptr) texCoordsBufferSize, NULL, GL_STATIC_DRAW);

    void *texCoordsPointer = glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY);
    memcpy(texCoordsPointer, data->textureCoords + start, texCoordsBufferSize);
    glUnmapBuffer(GL_ARRAY_BUFFER);

    glGenVertexArrays(1, &vaoID);
    glBindVertexArray(vaoID);

    glBindBuffer(GL_ARRAY_BUFFER, vertexBuffer);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(0);

    glBindBuffer(GL_ARRAY_BUFFER, normalBuffer);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(1);

    glBindBuffer(GL_ARRAY_BUFFER, textureBuffer);
    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 0, NULL);
    glEnableVertexAttribArray(2);

    //unbind everything
    glBindVertexArray(0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);

    //clean up no longer needed buffers
    glDeleteBuffers(1, &vertexBuffer);
    glDeleteBuffers(1, &normalBuffer);
    glDeleteBuffers(1, &textureBuffer);

    return (RenderObject) {.vao = vaoID, .numVertices = size};
}

static GLuint load_texture(uint8_t *texture, uint32_t width, uint32_t height){

    if (texture == NULL) return 0;
    GLuint textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei) width, (GLsizei) height, 0, GL_RGB,  GL_UNSIGNED_BYTE, texture);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glBindTexture(GL_TEXTURE_2D, 0);
    return textureID;
}

/*
 * loads given model onto the gpu
 */

void load_model(MemoryArena *arena, ObjData *data){

    size_t totalObjects = 1;
    for (size_t i = 0; i + 1 < data->numVertices / 3; i++){
        if (data->materialIndices[i] != data->materialIndices[i + 1]) ++totalObjects;
    }
    numRenderObjects = totalObjects;
    renderObjects = Arena_Allocate(RenderObject, (sizeof *renderObjects) * totalObjects, arena);


    //load the mesh...
    size_t start = 0;
    for (size_t i = 0; i < numRenderObjects; i++){
        uint32_t size = 0;
        size_t index = data->materialIndices[start / 3];
        for (size_t vert = 0; start + vert < data->numVertices; vert += 3){
            if (data->materialIndices[(start + vert) / 3] != index) break;
            size += 3;
        }
        renderObjects[i] = loadSubMesh(data, start, size);

        MtlMaterial *material = &data->materials[index];
        renderObjects[i].colour = material->diffuse;
        renderObjects[i].textureID = load_texture(material->texture, material->textureWidth, material->textureHeight);
        start += size;
    }
}



/*
 * calls apporpriate opengl functions to render the scene
 */

void render(uint32_t width, uint32_t height){


    glViewport(0, 0, (GLsizei) width, (GLsizei) height);
    glUseProgram(program);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

    Mat4 renderMatrix;
    Mat4xMat4(&projectionMatrix, &viewMatrix, &renderMatrix);

    GLint matrixID = glGetUniformLocation(program, "matrix");
    glUniformMatrix4fv(matrixID, 1, GL_TRUE, &renderMatrix.data[0][0]);

    GLint useTextureID = glGetUniformLocation(program, "useTexture");
    glUniform1i(useTextureID, GL_FALSE);

    for (size_t i = 0; i < numRenderObjects; i++){

        GLint diffuseID = glGetUniformLocation(program, "diffuseColour");
        glUniform3fv(diffuseID, 1, renderObjects[i].colour.data);

        GLint useTextureID = glGetUniformLocation(program, "useTexture");
        glUniform1i(useTextureID, renderObjects[i].textureID == 0 ? GL_FALSE : GL_TRUE);

        glActiveTexture(GL_TEXTURE0);
        glBindVertexArray(renderObjects[i].vao);
        glBindTexture(GL_TEXTURE_2D, renderObjects[i].textureID);

        glDrawArrays(GL_TRIANGLES, 0, (GLsizei) renderObjects[i].numVertices);
    }
    glfwSwapBuffers(window);
    glfwPollEvents();


}


/*
 * updates the view position by checking if w/a/s/d keys were pressed
 */

static void updatePos(Vec3 direction, Vec3 right){

    float speed = 1.f;
    float currentTime = (float) glfwGetTime();
    float deltaTime = currentTime - time;
    time = currentTime;

    if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
        speed = 3;
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) {
        speed = 0.5;
    }

    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
        pos = Vec3PlusVec3(pos, Vec3xScalar(direction, deltaTime * speed));
    }
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
        pos = Vec3MinusVec3(pos, Vec3xScalar(direction, deltaTime * speed));
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        pos = Vec3PlusVec3(pos, Vec3xScalar(right, deltaTime * speed));
    }
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        pos = Vec3MinusVec3(pos, Vec3xScalar(right, deltaTime * speed));
    }

}



/*
 * updates the camera based on user input
 * if the R key has been pressed, returns 1 and saves view matrix, else returns 0
 */

uint32_t update_camera(float width, float height, Mat4 *viewRet){

    double xpos, ypos;
    glfwGetCursorPos(window, &xpos, &ypos);

    //reset mouse position for next frame
    glfwSetCursorPos(window, width / 2.f, height / 2.f);

    //update view angles
    const float mouseSpeed = 0.005f;
    horizontalAngle += mouseSpeed * (width / 2.f - (float) xpos);
    verticalAngle += mouseSpeed * (height / 2.f - (float) ypos);

    //convert spherical coords to cartesian to get view direction
    Vec3 direction = {
            .x = cosf(verticalAngle) * sinf(horizontalAngle),
            .y = sinf(verticalAngle),
            .z = cosf(verticalAngle) * cosf(horizontalAngle)
    };


    //calculate up/right vectors and update camera pos
    Vec3 right = {
            .x = sinf(horizontalAngle - 3.14f / 2.0f),
            .y = 0,
            .z = cosf(horizontalAngle - 3.14f / 2.0f)
    };

    Vec3 up = CrossProduct3(right, direction);
    updatePos(direction, right);


    //update projection and view matrices
    projectionMatrix = create_projection_matrix(3.1415f / 2.f, width / height, 0.1f, 100.0f);
    viewMatrix = create_view_matrix(pos, Vec3PlusVec3(pos, direction), up);

    //check if R key pressed
    if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS){
        *viewRet = viewMatrix;
        return 1;
    }

    return 0;
}


void glfw_key_press_callback(GLFWwindow *window, int key, int scancode, int action, int mods){

    (void) window;
    (void) mods;
    (void) scancode;

    if (key == GLFW_KEY_M && action != GLFW_RELEASE){
        renderMode = (renderMode + 1) % 4;
    }
}



void render_result(uint8_t *framebuffer, uint8_t *indirectFramebuffer, uint8_t *directFramebuffer, uint8_t *cacheRecordFramebuffer, uint32_t width, uint32_t height){
    
    glUseProgram(quadProgram);

    GLuint textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei) width, (GLsizei) height, 0, GL_RGB,  GL_UNSIGNED_BYTE, framebuffer);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    GLuint indirectTextureID;
    glGenTextures(1, &indirectTextureID);
    glBindTexture(GL_TEXTURE_2D, indirectTextureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei) width, (GLsizei) height, 0, GL_RGB,  GL_UNSIGNED_BYTE, indirectFramebuffer);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    GLuint directTextureID;
    glGenTextures(1, &directTextureID);
    glBindTexture(GL_TEXTURE_2D, directTextureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei) width, (GLsizei) height, 0, GL_RGB,  GL_UNSIGNED_BYTE, directFramebuffer);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    GLuint cacheTextureID;
    glGenTextures(1, &cacheTextureID);
    glBindTexture(GL_TEXTURE_2D, cacheTextureID);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, (GLsizei) width, (GLsizei) height, 0, GL_RGB,  GL_UNSIGNED_BYTE, cacheRecordFramebuffer);
    glGenerateMipmap(GL_TEXTURE_2D);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);


    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);


    GLint screenWidthID = glGetUniformLocation(quadProgram, "screenWidth");
    glUniform1f(screenWidthID, (float) width);

    GLint screenHeightID = glGetUniformLocation(quadProgram, "screenHeight");
    glUniform1f(screenHeightID, (float) height);



    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureID);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glfwSwapBuffers(window);
    glfwSetKeyCallback(window, glfw_key_press_callback);

    GLuint textures[4] = {textureID, indirectTextureID, directTextureID, cacheTextureID};
    uint32_t oldRenderMode = renderMode;
    char *renderModeNames[4] = {"combined lighting", "indirect lighting", "direct lighting", "cache record positions"};

    while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS){
        glfwPollEvents();

        if (oldRenderMode != renderMode){
            glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
            glActiveTexture(GL_TEXTURE0);
            oldRenderMode = renderMode;
            printf("Rendering %s\n", renderModeNames[renderMode]);
        }

        glBindTexture(GL_TEXTURE_2D, textures[renderMode]);
        glDrawArrays(GL_TRIANGLES, 0, 6);
        glfwSwapBuffers(window);

    }

}










