/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */


#include "sample.h"
#include "linear_algebra.h"
#include <assert.h>
#include <assert.h>
#include <stdint.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <omp.h>


float uniform_sample();
static float cosine_sample();
static Vec2 sample_hemisphere_spherical();
static uint32_t sample_state();

#define PI 3.14159265f
#define EPSILON 0.000005f

static uint32_t *states;
static uint32_t a, c, mod;

void init_sampler(MemoryArena *arena, uint32_t numThreads){
    srand((unsigned int) time(NULL));
    a = 1664525;
    c = 1013904223;
    mod = (uint32_t) RAND_MAX;
    states = Arena_Allocate(int, (sizeof *states) * numThreads, arena);
    for (uint32_t i = 0; i < numThreads; i++){
        states[i] = (uint32_t) rand();
    }
}

static uint32_t sample_state(){
    int threadID = omp_get_thread_num();
    uint32_t x = (uint32_t) (((uint64_t)a * (uint64_t)states[threadID]) + (uint64_t)c) % (uint64_t)mod;
    states[threadID] = x;
    return x;
}

//returns pseudo random number in range (0, 1)
float uniform_sample(){
    uint32_t x = sample_state();
    float ans = (float) x / (float) mod;
    assert(ans >= 0);
    assert(ans <= 1);
    return ans;
}

static float cosine_sample(){
    return acosf(safe_sqrt(uniform_sample()));
}

static Vec2 sample_hemisphere_spherical(){
    return (Vec2) {.x = cosine_sample(), .y = uniform_sample() * 2.f * PI};

}

//converts spherical coords to cartesian, assumes y axis is up axis
Vec3 spherical_to_cartesian(Vec2 spherical){
    Vec3 cartesian = {
            .x = sinf(spherical.x) * cosf(spherical.y),
            .y = cosf(spherical.x),
            .z = sinf(spherical.x) * sinf(spherical.y),
    };
    return cartesian;
}

Vec2 cartesian_to_spherical(Vec3 cartesian){
    float theta = acosf(cartesian.y);
    float azimuth = acosf(cartesian.x / sinf(theta));
    return (Vec2) {.x = theta, .y = azimuth};
}

Vec3 sample_hemisphere(Vec3 normal){
    Vec2 sphericalSample = sample_hemisphere_spherical();

    assert(sphericalSample.x >= 0);
    assert(sphericalSample.x <= (PI / 2));

    assert(sphericalSample.y >= 0);
    assert(sphericalSample.x <= (PI * 2));


    Vec3 cartesianSample = spherical_to_cartesian(sphericalSample);
    cartesianSample = Normalise3(cartesianSample);

    assert(cartesianSample.y >= 0);

    normal = Normalise3(normal);

    Mat4 rotationMatrix = get_rotation_matrix((Vec3) {.x = 0, .y = 1, .z = 0}, normal, EPSILON);

    Vec3 sample = transform_point(&rotationMatrix, cartesianSample);
    assert(DotProduct3(sample, normal) >= 0);

    return Normalise3(sample);
}



Vec3 stratified_sample(float *thetaRet, Vec3 normal, float theta, float azimuth, float maxTheta, float maxAzimuth){
    
    float x = uniform_sample();
    float y = uniform_sample();

    Vec3 ans;
    ans.x = safe_sqrt((theta + x) / maxTheta) * cosf(2.f * PI * (azimuth + y) / maxAzimuth);
    ans.y = safe_sqrt((theta + x) / maxTheta) * sinf(2.f * PI * (azimuth + y) / maxAzimuth);
    ans.z = safe_sqrt(1 - (theta + x) / maxTheta);
    
    *thetaRet = acosf(ans.z);
    Mat4 rotationMatrix = get_rotation_matrix((Vec3) {.x = 0, .y = 0, .z = 1}, normal, EPSILON);
    ans = transform_point(&rotationMatrix, ans);
    return ans;
}
