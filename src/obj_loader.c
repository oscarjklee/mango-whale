/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */


#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <errno.h>
#include <stdint.h>
#include <time.h>
#include <unistd.h>
#include <errno.h>
#include "obj_loader.h"
#include "linear_algebra.h"
#include "ppm_image.h"

#define EPSILON 0.000005f


static FILE* objFile;
static char* directory;

static SkyBox skyBox = {0};
static size_t numPortals = 0;

typedef enum LineType {VERTEX, TEXTURE_COORD, VERTEX_NORMAL, COMMENT, ERROR, FACE, USEMTL, MTLLIB} LineType;

typedef struct Vec3Array{
    Vec3* data;
    size_t size;
    size_t capacity;
} Vec3Array;

typedef struct Vec2Array{
    Vec2* data;
    size_t size;
    size_t capacity;
} Vec2Array;

typedef struct Size_tArray{
    size_t *data;
    size_t size;
    size_t capacity;
} Size_tArray;

typedef struct EmissiveArray{
    EmissiveTriangle *data;
    size_t size;
    size_t capacity;
} EmissiveArray;


static Vec3Array initVec3Array(unsigned int size, MemoryArena* arena);
static Vec2Array initVec2Array(unsigned int size, MemoryArena* arena);
static LineType getNextLine(char* buffer, int* eof);
static int parseFace(char* text, int* indices);
static Vec3 readVec3(char* line);
static Vec2 readVec2(char* line);
static void addToArray2(Vec2Array *array, char *text, MemoryArena *arena);
static void addToArray3(Vec3Array *array, char *text, MemoryArena *arena);
static void readMaterial(FILE* file, MtlMaterial* material, char* line, MemoryArena *arena);
static unsigned int parseMtlFile(char* line, char*** materialNamesReturn, MtlMaterial** materialsReturn, MemoryArena* arena);
static void addToIndexedVec3(Vec3Array *array, Vec3Array *indexedArray, MemoryArena *arena, int indices);
static void addToIndexedVec2(Vec2Array *array, Vec2Array *indexedArray, MemoryArena *arena, int indices);
static size_t getMtl(char* line, char** materialNames, size_t numMaterials);
static void setDefaultMaterial(MtlMaterial* material);
static void addToMaterialIndices(Size_tArray* array, MemoryArena *arena, size_t materialIndex);
static Size_tArray initSize_tArray(unsigned int size, MemoryArena* arena);
static void updateEmissives(MtlMaterial *materials, EmissiveArray *emissives, Vec3 *vertices, MemoryArena *arena, size_t triangleIndex, size_t materialIndex);

//create buffer of length *size*
static Vec3Array initVec3Array(unsigned int size, MemoryArena* arena){
    Vec3Array array;
    array.data = Arena_Allocate(Vec3 , size * (sizeof *array.data), arena);
    array.size = 0;
    array.capacity = size;
    return array;
}

static Vec2Array initVec2Array(unsigned int size, MemoryArena* arena){
    Vec2Array array;
    array.data = Arena_Allocate(Vec2 , size * (sizeof *array.data), arena);
    array.size = 0;
    array.capacity = size;
    return array;
}

static Size_tArray initSize_tArray(unsigned int size, MemoryArena* arena){
    Size_tArray array;
    array.data = Arena_Allocate(size_t , size * (sizeof *array.data), arena);
    array.size = 0;
    array.capacity = size;
    return array;
}

static struct EmissiveArray initEmissiveArray(unsigned int size, MemoryArena *arena){
    struct EmissiveArray array;
    array.data = Arena_Allocate(EmissiveTriangle , size * (sizeof *array.data), arena);
    array.size = 0;
    array.capacity = size;
    return array;
}


//read next line from file
//return data type of the line
//update eof indicator and return if end of file
static LineType getNextLine(char* buffer, int* eof) {
    static int lineNum = 1;
    if (fgets(buffer, 1024, objFile) == NULL){
        *eof = feof(objFile);

        if (*eof == 0){ //it returned NULL and it *wasn't* the end of the file - something went wrong
            fclose(objFile);
            printf("Super-Lightweight-Obj-Loader Error:\nError reading obj file, lineNum = %d\n"
                   "fgets() returned NULL pointer and feof() returned 0\n", lineNum);
            return ERROR;
        }

        fclose(objFile);

        return COMMENT;
    }
    lineNum++;

    int i;
    for (i = 0; i < 1024 && buffer[i] == ' '; i++); //strip whitespace

    if (buffer[i] == '#') return COMMENT;
    if (buffer[i] == 'f') return FACE;

    if (strstr(buffer + i, "usemtl")) return USEMTL;
    if (strstr(buffer + i, "mtllib")) return MTLLIB;

    if (buffer[i] == 'v'){
        switch (buffer[i + 1]) {
            case 'n':
                return VERTEX_NORMAL;
            case 't':
                return TEXTURE_COORD;
            case ' ':
                return VERTEX;
            default:
                return ERROR;
        }
    }
    return COMMENT;
}

static Vec3 readVec3(char* line){
    Vec3 data;
    for (; *line == ' '; line++); //strip whitespace
    data.x = (float) atof(line);

    for (; *line != ' '; line++); //strip number
    for (; *line == ' '; line++); //strip whitespace
    data.y = (float) atof(line);

    for (; *line != ' '; line++); //strip number
    for (; *line == ' '; line++); //strip whitespace
    data.z = (float) atof(line);

    return data;
}

static Vec2 readVec2(char* line){
    Vec2 data;
    for (; *line == ' '; line++); //strip whitespace
    data.x = (float) atof(line);

    for (; *line != ' '; line++); //strip number
    for (; *line == ' '; line++); //strip whitespace
    data.y = (float) atof(line);
    return data;
}


//add the data from a given line to the given array
//grow the buffer size with Arena_Realloc() when needed
static void addToArray3(Vec3Array *array, char *text, MemoryArena *arena){
    for (; *text != ' '; text++); //strip text

    Vec3 ans = readVec3(text);

    if (array->size + 1 > array->capacity){
        array->data = Arena_Realloc(Vec3, (sizeof *array->data) * array->size, array->data, arena, array->capacity * 2 * (sizeof *array->data));
        array->capacity *= 2;
    }

    array->data[array->size++] = ans;

}

static void addToArray2(Vec2Array *array, char *text, MemoryArena *arena){
    for (; *text != ' '; text++); //strip text

    Vec2 ans = readVec2(text);

    if (array->size + 1 > array->capacity){
        array->data = Arena_Realloc(Vec2, (sizeof *array->data) * array->size, array->data, arena, array->capacity * 2 * (sizeof *array->data));
        array->capacity *= 2;
    }

    array->data[array->size++] = ans;
}


static int parseFace(char* text, int* indices){

    int i = 0;
    for (; i < 1024 && text[i] == ' '; i++); //strip whitespace

    indices[0] = atoi(text + i) - 1;
    for (; i < 1024 && text[i] >= '0' && text[i] <= '9'; i++); //strip number

    if (text[i++] == ' '){ //face only contains vertex index
        indices[1] = -1;
        indices[2] = -1;
        assert(0);
        return i;
    }


    if (text[i] == '/') indices[1] = -1; //no texture index

    else {
        indices[1] = atoi(text + i) - 1;
        for (; i < 1024 && text[i] >= '0' && text[i] <= '9'; i++); //strip number
    }

    if (text[i++] == ' '){
        indices[2] = -1; //no normal index
        assert(0);
        return i;
    }
    indices[2] = atoi(text + i) - 1;
    for (; i < 1024 && text[i] >= '0' && text[i] <= '9'; i++); //strip number
    return i;
}


static void addToIndexedVec3(Vec3Array *array, Vec3Array *indexedArray, MemoryArena *arena, int index){
    if (index == -1) return;

    if (indexedArray->size + 1 > indexedArray->capacity){
        indexedArray->data = Arena_Realloc(Vec3, (sizeof *indexedArray->data) * indexedArray->size, indexedArray->data,
                                           arena, indexedArray->capacity * 2 * (sizeof *indexedArray->data));
        indexedArray->capacity *= 2;
    }

    indexedArray->data[indexedArray->size++] = array->data[index];
}


static void addToIndexedVec2(Vec2Array *array, Vec2Array *indexedArray, MemoryArena *arena, int index){
    if (index == -1) return;

    if (indexedArray->size + 1 > indexedArray->capacity){
        indexedArray->data = Arena_Realloc(Vec2, (sizeof *indexedArray->data) * indexedArray->size, indexedArray->data,
                                           arena, indexedArray->capacity * 2 * (sizeof *indexedArray->data));
        indexedArray->capacity *= 2;
    }

    indexedArray->data[indexedArray->size++] = array->data[index];
}




static unsigned int parseMtlFile(char* line, char*** materialNamesReturn, MtlMaterial** materialsReturn, MemoryArena* arena){
    while (*line == ' ') line++;
    line += 7; // move past "mtllib" (space included)

    int i = 0;
    for (; line[i] != '\n' && line[i] != '\r'; i++) assert(line[i] != '\r'); //read through file name until \n encountered
    line[i] = '\0'; //replace \n with null terminator


    char string[1024] = {0};
    strcat(string, directory);
    strcat(string, line);

    FILE* mtlFile = fopen(string, "r");
    assert(mtlFile);

    unsigned int numMaterials = 0;
    MtlMaterial* materials = Arena_Allocate(MtlMaterial, 64 * (sizeof *materials), arena);
    char** materialNames = Arena_Allocate(char*, 1024 * (sizeof  *materialNames), arena);

    char mtlLine[1024];

    char* status = fgets(mtlLine, 1024, mtlFile);

    while (status != NULL){

        if (!strstr(mtlLine, "newmtl")){
            status = fgets(mtlLine, 1024, mtlFile);
            continue;
        }

        int j = 6;
        for (; mtlLine[j] == ' '; j++); //read through whitespace
        for (i = j; mtlLine[i] != '\n'; i++); //find new line char
        mtlLine[i] = '\0'; //replace with null terminator

        materialNames[numMaterials] = Arena_Allocate(char, strlen(mtlLine + j) + 1, arena);
        strcpy(materialNames[numMaterials], &mtlLine[j]);
        readMaterial(mtlFile, &materials[numMaterials], mtlLine, arena);

        ++numMaterials;
    }
    *materialNamesReturn = materialNames;
    *materialsReturn = materials;
    return numMaterials;
}

static void setDefaultMaterial(MtlMaterial* material){
    material->texture = NULL;

    material->diffuse = (Vec3){.x = 0, .y = 0, .z = 0};
    material->ambient = (Vec3){.x = 1, .y = 1, .z = 1};

    material->specular = (Vec3){.x = 0, .y = 0, .z = 0};
    material->emissive = (Vec3){.x = 0, .y = 0, .z = 0};

    material->transmissionFilter = (Vec3){.x = 0, .y = 0, .z = 0};

    material->alpha = 0;
    material->transparency = 0;
    material->refractionIndex = 1;
    material->reflectivity = 0;
    material->luminosity = 0;

    material->textureWidth = 0;
    material->textureHeight = 0;
    material->render = 1;

}

static void readMaterial(FILE* file, MtlMaterial* material, char* line, MemoryArena *arena) {

    setDefaultMaterial(material);
    char string[1024] = {0};

    while (fgets(line, 1024, file) != NULL && !strstr(line, "newmtl")) {
        int i;
        for (i = 0; line[i] == ' '; i++);

        if (strncmp(line + i, "map_Kd", 6) == 0){//texture map
            for (i = i + 6; line[i] == ' '; i++); //eat up identifier and any trailing whitespace
            int j;
            for (j = i; line[j] != '\n'; j++); //extract the texture file name
            line[j] = '\0';

            memset(string, 0, 1024);
            strcat(string, directory);
            strcat(string, line + i);

            material->texture = read_image_from_ppm(string, &material->textureWidth, &material->textureHeight, arena);
            assert(material->texture);
            assert(material->textureWidth);
            assert(material->textureHeight);

            continue;
        }

        if (strncmp(line + i, "map_SkyBox", 9) == 0){ //skybox

            size_t index = line[i + 10] - 48;
            if (index > 5){
                fprintf(stderr, "Error - expected 0 to 5 after \"map_SkyBox in mtl file\"\n");
                fprintf(stderr, "Instead, line is: %s\n", line);
                exit(EXIT_FAILURE);
            }
            
            for (i = i + 11; line[i] == ' '; i++); //eat up identifier and any trailing whitespace
            int j;
            for (j = i; line[j] != '\n'; j++); //extract the texture file name
            line[j] = '\0';

            memset(string, 0, 1024);
            strcat(string, directory);
            strcat(string, line + i);

            skyBox.texture[index] = read_image_from_ppm(string, &skyBox.textureWidth, &skyBox.textureHeight, arena);
            continue;
        }


        if (strncmp(line + i, "portal", 6) == 0){ //portal
            material->transparency = 1;
            material->transmissionFilter = (Vec3) {.x = 1, .y = 1, .z = 1};
            material->render = 0;
            continue;
        }



        switch (line[i]) {
            case 'K':
                switch (line[i + 1]) {
                    case 'a':
                        material->ambient = readVec3(&line[i + 2]);
                        break;
                    case 'd':
                        material->diffuse = readVec3(&line[i + 2]);
                        break;
                    case 's':
                        material->specular = readVec3(&line[i + 2]);
                        break;
                    case 'e':
                        material->emissive = readVec3(&line[i + 2]);
                        break;
                    default:
                        assert(0); //should not happen
                }
                break;

            case 'N':
                switch (line[i + 1]) {
                    case 'i':
                        material->refractionIndex = (float) atof(&line[i + 2]);
                        break;
                    case 's':
                        material->alpha = (float) atof(&line[i + 2]);
                        break;
                    case 'm':
                        material->reflectivity = (float) atof(&line[i + 2]);
                        break;
                    default:
                        assert(0);
                }
                break;

            case 'd':
                material->transparency = 1.f - (float) atof(&line[i + 1]);
                break;

            case 'L':
                material->luminosity = (float) atof(&line[i + 2]);
                break;

            case 'T':
                switch (line[i + 1]) {
                    case 'f':
                        material->transmissionFilter = readVec3(&line[i + 2]);
                        break;
                    default:
                        assert(0);
                }
                break;

            default:
                break;
        }
    }
    if (material->emissive.x > 0 && material->emissive.y > 0 && material->emissive.z > 0 && material->luminosity == 0){
        material->luminosity = 1;
    }
}

static size_t getMtl(char* line, char** materialNames, size_t numMaterials){
    for (; *line == ' '; line++){}
    int i;
    for (i = 0; line[i] != '\n'; i++){}
    line[i] = '\0'; //replace new line char with null terminator, line is now the name of the material

    for (size_t j = 0; j < numMaterials; j++){
        if (strcmp(line, materialNames[j]) == 0){
            return j;
        }
    }
    return (size_t) -1;
}

static void addToMaterialIndices(Size_tArray* array, MemoryArena *arena, size_t materialIndex){

    if (array->size + 1 > array->capacity){
        array->data = Arena_Realloc(size_t, (sizeof *array->data) * array->size, array->data, arena,
                                    array->capacity * 2 * (sizeof *array->data));
        array->capacity *= 2;
    }

    array->data[array->size++] = materialIndex;
}

static void updateEmissives(MtlMaterial *materials, EmissiveArray *emissives, Vec3 *vertices, MemoryArena *arena, size_t triangleIndex, size_t materialIndex){
    if (Compare3(materials[materialIndex].emissive, (Vec3) {.x = 0, .y = 0, .z = 0}, EPSILON)){
        return; //triangle is not emissive, no action needed
    }

    Vec3 v0 = vertices[triangleIndex + 0];
    Vec3 v1 = vertices[triangleIndex + 1];
    Vec3 v2 = vertices[triangleIndex + 2];

    Vec3 e1 = Vec3MinusVec3(v1, v0);
    Vec3 e2 = Vec3MinusVec3(v2, v0);
    Vec3 cross = CrossProduct3(e1, e2);

    float area = Magnitude3(cross) * 0.5f;
    float luminosity = materials[materialIndex].luminosity;

    EmissiveTriangle newEmissive = {.triangleIndex = triangleIndex / 3, .weighting = area * luminosity};

    if (emissives->capacity == emissives->size){
        size_t currentSize =  (sizeof *emissives->data) * emissives->size; //size of array in bytes
        emissives->data = Arena_Realloc(EmissiveTriangle, currentSize, emissives->data, arena, 2 * currentSize);
        emissives->capacity *= 2;
    }

    emissives->data[emissives->size++] = newEmissive;

}


int load_obj_file(char* dir, char* file, MemoryArena* arena, ObjData *objData){

    directory = dir;
    char string[1024] = {0};
    strcat(string, directory);
    strcat(string, file);

    objFile = fopen(string, "r");
    if (objFile == NULL) return EXIT_FAILURE;
    char currentLine[1024];

    MemoryArena *tempArena = create_arena(1024 * 1024);
    Vec3Array vertexArray = initVec3Array(1024, tempArena);

    Vec3Array normalsArray = initVec3Array(1024, tempArena);
    Vec2Array texturesArray = initVec2Array(1024, tempArena);

    Vec3Array indexedVertexArray = initVec3Array(1024, arena);
    Vec3Array indexedNormalsArray = initVec3Array(1024, arena);
    Vec2Array indexedTexturesArray = initVec2Array(1024, arena);
    Vec3Array indexedPortalsArray = initVec3Array(1024, arena);

    Size_tArray materialIndicesArray = initSize_tArray(1024, arena);
    EmissiveArray emissiveArray = initEmissiveArray(1024, arena);

    MtlMaterial* materials = NULL;
    char** materialNames = NULL;
    unsigned int numMaterials = 0;

    int eof = 0;
    size_t currentMaterial = (size_t) -1;

    while (!eof){
        LineType lineType = getNextLine(currentLine, &eof);
        switch (lineType) {

            case FACE: ;//semicolon suppress compiler warning about declaration immediately after label

                if (currentMaterial == (size_t) -1){
                    printf("Error - no material has been set before defining faces\n");
                    return EXIT_FAILURE;
                }

                int indices[3];
                int offset = 1; //offset = 1 to skip the initial 'f' char
                for (int i = 0; i < 3; i++){
                    offset += parseFace(currentLine + offset, indices);
                    if (materials[currentMaterial].render == 0){
                        addToIndexedVec3(&vertexArray, &indexedPortalsArray, arena, indices[0]);
                        continue;
                    }
                    addToIndexedVec3(&vertexArray, &indexedVertexArray, arena, indices[0]);
                    addToIndexedVec2(&texturesArray, &indexedTexturesArray, arena, indices[1]);
                    addToIndexedVec3(&normalsArray, &indexedNormalsArray, arena, indices[2]);
                }

                if (materials[currentMaterial].render == 0) break;
                addToMaterialIndices(&materialIndicesArray, arena, currentMaterial);
                size_t triangleIndex = (indexedVertexArray.size - 3); //triangle has just been pushed onto triangle soup array (vertexArray)
                updateEmissives(materials, &emissiveArray, indexedVertexArray.data, arena, triangleIndex, currentMaterial);
                break;

            case COMMENT:
                break;

            case MTLLIB:
                numMaterials += parseMtlFile(currentLine, &materialNames, &materials, arena);
                break;

            case VERTEX_NORMAL:
                addToArray3(&normalsArray, currentLine, tempArena);
                break;

            case TEXTURE_COORD:
                addToArray2(&texturesArray, currentLine, tempArena);
                break;

            case VERTEX:
                addToArray3(&vertexArray, currentLine, tempArena);
                break;

            case USEMTL:
                currentMaterial = getMtl(currentLine + 6, materialNames, numMaterials);
                if (currentMaterial != (size_t) -1) break;
                printf("Error - failed to detect valid mtl name, line was \"%s\"\n", currentLine);
                return EXIT_FAILURE;

            case ERROR:
                printf("Error detected on this line: %s\n", currentLine);
                return EXIT_FAILURE;
        }
    }

    //check that either no skybox faces or all skybox faces defined
    uint32_t status = skyBox.texture[0] != NULL;
    for (int i = 1; i < 6; i++){
        if ((skyBox.texture[i] != NULL) != status){
            fprintf(stderr, "Error - expected either 0 skybox faces or 6 skybox faces\n");
            exit(EXIT_FAILURE);
        }
    }
    
    objData->skyBox = skyBox.texture[0] ? &skyBox : NULL;
    objData->vertices = indexedVertexArray.data;
    objData->portalVertices = indexedPortalsArray.data;
    objData->textureCoords = indexedTexturesArray.data;
    objData->vertexNormals = indexedNormalsArray.data;
    objData->materialIndices = materialIndicesArray.data;
    objData->numVertices = indexedVertexArray.size;
    objData->materials = materials;

    for (size_t i = 1; i < emissiveArray.size; i++){
        emissiveArray.data[i].weighting += emissiveArray.data[i - 1].weighting;
        assert(emissiveArray.data[i].weighting);
    }

    objData->emissiveTriangles = emissiveArray.data;
    objData->numEmissives = emissiveArray.size;
    objData->numPortals = indexedPortalsArray.size / 3;

    delete_arena(tempArena);
    return EXIT_SUCCESS;
}
