/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MANGO_WHALE_BOUNDING_VOLUME_HIERARCHY_H
#define MANGO_WHALE_BOUNDING_VOLUME_HIERARCHY_H
#include "common_definitions.h"
#include "string.h"


//flattened bvh node, is 32 bytes so it should fit snuggly into cache for any modern processor
typedef struct BvhNodePublic{
    Vec3 bounds[2];
    union {
        uint32_t childIndex;
        uint32_t triangleIndex;
    };
    uint32_t leaf; //0 if not leaf, 1 if leaf
} BvhNodePublic;

BvhNodePublic* create_bvh(Triangle* triangles, uint32_t numTriangles, MemoryArena* arena, uint32_t* arraySize);

#endif //MANGO_WHALE_BOUNDING_VOLUME_HIERARCHY_H
