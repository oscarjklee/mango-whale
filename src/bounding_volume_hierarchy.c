/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */

#include "bounding_volume_hierarchy.h"
#include <assert.h>

//we create a new data type for bvh construction
//there is a lot of additional information about the triangles that is needed for bvh construction, but not necessarily
//needed elsewhere
//if we do end up using this elsewhere then it can be removed

typedef struct BvhNode{
    struct BvhNode* children[2];
    Vec3 bounds[2];
    uint32_t morton_code;
    uint32_t triangleIndex;
} BvhNode;


typedef struct BvhTriangle{
    Vec3 centre;
    uint32_t index;
    uint32_t mortonCode;
} BvhTriangle;


typedef struct node_pair {
    uint32_t partner;
    float volume;
} node_pair;


typedef struct stack_parameters{
    BvhNode*** arrayReturn;
    BvhNode** lhs;
    BvhNode** rhs;
    uint32_t* newArrSize;
    uint32_t start;
    uint32_t numNodes;
    uint32_t lhsSize;
    uint32_t rhsSize;
    uint32_t partitionIndex;
} stack_parameters;

typedef struct flattening_stack{
    BvhNode* node;
    uint32_t parentIndex;
} flattening_stack;



static inline int morton_comp(const void* p1, const void* p2);
static void generate_morton_codes(Vec3 bound1, Vec3 bound2, BvhTriangle* triangles, uint32_t numTriangles);
static inline uint32_t spread_bits(uint32_t a);
static inline Vec3 calculate_centres(Triangle* triangle);
static inline float calculate_merged_surface_area(BvhNode* a, BvhNode* b);
static BvhNode* approximate_agglomerative_clustering(BvhTriangle* triangles, uint32_t numTriangles);
static void build_tree(BvhNode** nodes, uint32_t start, uint32_t numNodes, uint32_t childrenPerNode, uint32_t* newArr_size, BvhNode*** returnArray);
static void combine_clusters(BvhNode** nodes, uint32_t numNodes, uint32_t targetNum);
static inline node_pair find_best_match(BvhNode** nodes, uint32_t numNodes, size_t target_node);
static inline uint32_t get_best_node_pair(node_pair* pairs, uint32_t numNodes);
static BvhNode* combine_nodes(BvhNode* a, BvhNode* b);
static uint32_t partition_nodes(BvhNode** nodes, uint32_t start, uint32_t numNodes);
static uint32_t calculate_target_node_count(uint32_t numNodes);
static inline BvhNode* create_node(BvhTriangle* bvhTriangle);
static void push_to_stack(stack_parameters newItem);
static void pop_from_stack(BvhNode** arr, uint32_t arrSize);
static BvhNodePublic* flatten_tree(BvhNode* root, uint32_t numTriangles, uint32_t* arraySize);
static inline void merge_bounds(BvhNode * a, BvhNode* b, BvhNode* c);


static MemoryArena* mainArena; //arena used for bvh that bvh which will be returned to the user is stored on
static MemoryArena* auxiliaryArena; //arena used for temporary storage when constructing bvh, will be flushed before exiting

static stack_parameters* buildTreeStack;
static uint64_t stackPointer;

static Triangle* trianglePrimitives;


//function for sorting nodes based on morton code
static inline int morton_comp(const void* p1, const void* p2){
    BvhTriangle* x = (BvhTriangle*) p1;
    BvhTriangle* y = (BvhTriangle*) p2;
    uint32_t a = x->mortonCode;
    uint32_t b = y->mortonCode;
    return (a > b) - (a < b);
}


//follows algorithm for morton code generation from pbr book
//we shift the bits sequentially, starting with the first 2, then the next and so on
//the magic numbers make sure that only the bits we want to move are kept from the shift
static inline uint32_t spread_bits(uint32_t a){

    if (a >= (1 << 10)){
        a = (a << 10) - 1;
    }

    a = (a | (a << 16)) & 0x30000FF;
    a = (a | (a << 8))  & 0x300F00F;
    a = (a | (a << 4))  & 0x30C30C3;
    a = (a | (a << 2))  & 0x9249249;
    return a;
}



//will calculate the morton codes for each triangle
//is its own function because of the helper variables needed
static void generate_morton_codes(Vec3 bound1, Vec3 bound2, BvhTriangle* triangles, uint32_t numTriangles){

    const uint32_t numPartitions = 1 << 10; //number of partitions we use for each axis

    float x_increment = (bound2.x - bound1.x) / (float) numPartitions;
    float y_increment = (bound2.y - bound1.y) / (float) numPartitions;
    float z_increment = (bound2.z - bound1.z) / (float) numPartitions;

    if (x_increment == 0) x_increment = 1;
    if (y_increment == 0) y_increment = 1;
    if (z_increment == 0) z_increment = 1;

    assert(x_increment);
    assert(y_increment);
    assert(z_increment);

    for (size_t i = 0; i < numTriangles; i++){
        assert(triangles[i].centre.x >= bound1.x);
        assert(triangles[i].centre.y >= bound1.y);
        assert(triangles[i].centre.z >= bound1.z);
        const uint32_t x = (uint32_t) ((triangles[i].centre.x - bound1.x) / x_increment);
        const uint32_t y = (uint32_t) ((triangles[i].centre.y - bound1.y) / y_increment);
        const uint32_t z = (uint32_t) ((triangles[i].centre.z - bound1.z) / z_increment);
        triangles[i].mortonCode = (spread_bits(x) << 2) + (spread_bits(y) << 1) + spread_bits(z);
    }

}


//given a triangle, calculate its centre point
static inline Vec3 calculate_centres(Triangle* triangle){
    Vec3 centre;
    centre.x = (triangle->points[0].x + triangle->points[1].x + triangle->points[2].x) / 3.f;
    centre.y = (triangle->points[0].y + triangle->points[1].y + triangle->points[2].y) / 3.f;
    centre.z = (triangle->points[0].z + triangle->points[1].z + triangle->points[2].z) / 3.f;
    return centre;
}



//calculate the surface area we would have if we merged the 2 nodes
static inline float calculate_merged_surface_area(BvhNode* a, BvhNode* b){

    BvhNode temp;
    merge_bounds(a, b, &temp);
    const float width = temp.bounds[1].x - temp.bounds[0].x;
    const float height = temp.bounds[1].y - temp.bounds[0].y;
    const float depth = temp.bounds[1].z - temp.bounds[0].z;

    return 2 * ((width * height) + (width * depth) + (depth * height));
}

//takes a triangle and wraps it in a node
static inline BvhNode* create_node(BvhTriangle* bvhTriangle){
    BvhNode* newNode = Arena_Allocate(BvhNode, sizeof *newNode, auxiliaryArena);
    Triangle triangle = trianglePrimitives[bvhTriangle->index];
    newNode->bounds[0].x = MIN(MIN(triangle.points[0].x, triangle.points[1].x), triangle.points[2].x);
    newNode->bounds[0].y = MIN(MIN(triangle.points[0].y, triangle.points[1].y), triangle.points[2].y);
    newNode->bounds[0].z = MIN(MIN(triangle.points[0].z, triangle.points[1].z), triangle.points[2].z);

    newNode->bounds[1].x = MAX(MAX(triangle.points[0].x, triangle.points[1].x), triangle.points[2].x);
    newNode->bounds[1].y = MAX(MAX(triangle.points[0].y, triangle.points[1].y), triangle.points[2].y);
    newNode->bounds[1].z = MAX(MAX(triangle.points[0].z, triangle.points[1].z), triangle.points[2].z);
    newNode->children[0] = newNode->children[1] = NULL;
    newNode->morton_code = bvhTriangle->mortonCode;
    newNode->triangleIndex = bvhTriangle->index;

    assert(newNode->bounds[0].x <= newNode->bounds[1].x);
    assert(newNode->bounds[0].y <= newNode->bounds[1].y);
    assert(newNode->bounds[0].z <= newNode->bounds[1].z);

    assert(newNode->triangleIndex != (uint32_t) -1);
    return newNode;
}


//implementation of approximate agglomerative clustering algorithm as described in
// "Efficient BVH Construction via Approximate Agglomerative Clustering"
// Yan Gu, Yong He, Kayvon Fatahalian, Guy Blelloch
static BvhNode* approximate_agglomerative_clustering(BvhTriangle* triangles, uint32_t numTriangles){

    BvhNode** nodeArr = Arena_Allocate(BvhNode*, (sizeof *nodeArr) * numTriangles, auxiliaryArena);
    for (size_t i = 0; i < numTriangles; i++){
        nodeArr[i] = create_node(&triangles[i]);
    }

    //we call build_tree, which gives us a top level cluster containing topClusterArrSize nodes
    uint32_t topClusterArrSize;
    BvhNode** bvhTopCluster;
    build_tree(nodeArr, 0, numTriangles, 128, &topClusterArrSize, &bvhTopCluster);

    //combine these clusters into 1 cluster
    combine_clusters(bvhTopCluster, topClusterArrSize, 1);

    //we could wrap this cluster in a node, but because it is the top level we do not need to
    //so we can just return the cluster as a root node - we just have to find it
    for (size_t i = 0; i < topClusterArrSize; i++){
        if (bvhTopCluster[i] != NULL) return bvhTopCluster[i];
    }

    return NULL; //if we get here something has gone wrong
}


//function will recursively split the nodes array into sub-arrays until it reaches sub arrays of size <= chilrenPerNode
//it will then start to cluster the nodes and recursively build a bvh tree according to AAC algorithm
static void build_tree(BvhNode** nodes, uint32_t start, uint32_t numNodes, uint32_t childrenPerNode,
                            uint32_t* newArr_size, BvhNode*** returnArray){

    //initialise call stack
    buildTreeStack = Arena_Allocate(stack_parameters, (sizeof *buildTreeStack) * numNodes, auxiliaryArena); //does not need to be this big
    stackPointer = 0;


    stack_parameters initial = {.arrayReturn = returnArray, .lhs = NULL, .rhs = NULL, .newArrSize = newArr_size,
                                .start = start, .numNodes = numNodes, .lhsSize = 0, .rhsSize = 0};
    push_to_stack(initial);

    while (stackPointer > 0){
        BvhNode** mergedArray;
        uint32_t mergedArraySize;
        #define current buildTreeStack[stackPointer - 1] //improves readability
        assert(current.numNodes);
        if (current.numNodes > childrenPerNode){ //the current array is too big - we need to partition it further

            if (current.lhs == NULL) { //need to calculate lhs
                current.partitionIndex = partition_nodes(nodes, current.start, current.numNodes);

                stack_parameters left = {.arrayReturn = &current.lhs, .lhs = NULL, .rhs = NULL,
                                         .newArrSize = &current.lhsSize, start = current.start,
                                         .numNodes = current.partitionIndex, .lhsSize = 0, .rhsSize = 0};
                push_to_stack(left);

                continue;
            }

            if (current.rhs == NULL){ //need to calculate rhs
                assert(current.lhsSize);
                stack_parameters right = {.arrayReturn = &current.rhs, .lhs = NULL, .rhs = NULL,
                                          .newArrSize = &current.rhsSize, .start = current.start + current.partitionIndex,
                                          .numNodes = current.numNodes - current.partitionIndex,
                                          .lhsSize = 0, .rhsSize = 0};
                push_to_stack(right);
                continue;
            }

            //merge the 2 child arrays

            assert(current.lhsSize && current.rhsSize);
            mergedArraySize = current.lhsSize + current.rhsSize;
            mergedArray = Arena_Allocate(BvhNode*, (sizeof *mergedArray) * (mergedArraySize), auxiliaryArena);

            memcpy(mergedArray, current.lhs, current.lhsSize * (sizeof *current.lhs));
            memcpy(mergedArray + current.lhsSize, current.rhs, current.rhsSize * (sizeof *current.rhs));
        }

        else{ //array is small enough, we can use nodes directly
            mergedArraySize = current.numNodes;
            mergedArray = nodes + current.start;
        }

        const uint32_t targetNodes = calculate_target_node_count(mergedArraySize);
        combine_clusters(mergedArray, mergedArraySize, targetNodes);

        const uint32_t numLeftoverNodes = MIN(targetNodes, mergedArraySize);
        BvhNode** newArr = Arena_Allocate(BvhNode*, (sizeof *newArr) * numLeftoverNodes , auxiliaryArena);

        size_t j = 0;
        for (size_t i = 0; i < mergedArraySize; i++){
            if (mergedArray[i] != NULL){ //node was not merged away in combine_clusters()
                newArr[j++] = mergedArray[i];
            }
        }
        assert(j == numLeftoverNodes);
        pop_from_stack(newArr, numLeftoverNodes);
    }
    #undef current
}

static inline void push_to_stack(stack_parameters newItem){
    buildTreeStack[stackPointer++] = newItem;
}

static inline void pop_from_stack(BvhNode** arr, uint32_t arrSize){
    stack_parameters top = buildTreeStack[--stackPointer];
    *top.arrayReturn = arr;
    *top.newArrSize = arrSize;
}


//function takes a series of bvh nodes and will merge them together until the number of remaining nodes == targetNum
static void combine_clusters(BvhNode** nodes, uint32_t numNodes, uint32_t targetNum){

    if (numNodes <= targetNum) return;

    node_pair best_match[numNodes];
    for (size_t i = 0; i < numNodes; i++){
        best_match[i] = find_best_match(nodes, numNodes, i);
    }

    //best_match now contains the best node for each node to pair up with
    //we will continuously merge the best 2 nodes together until only targetNum nodes remain
    uint32_t remainingNodes = numNodes;

    while (remainingNodes-- > targetNum){
        //merge the 2 best nodes together
        uint32_t index1 = get_best_node_pair(best_match, numNodes);
        uint32_t index2 = best_match[index1].partner;

        assert(index2 != numNodes * 2);
        assert(index1 != index2);
        assert(nodes[index1] != nodes[index2]);

        nodes[index1] = combine_nodes(nodes[index1], nodes[index2]);
        nodes[index2] = NULL;

        //check if we merged into final node
        if (remainingNodes <= targetNum) return;

        //otherwise update the best match array
        best_match[index1] = find_best_match(nodes, numNodes, index1);
        best_match[index2].partner = numNodes * 2; //should point to a non existent node

        for (size_t i = 0; i < numNodes; i++){
            if ((best_match[i].partner == index1 || best_match[i].partner == index2)){
                best_match[i] = find_best_match(nodes, numNodes, i);
            }
        }
    }
}

//given an array of nodes, for each node it will find its best match
//the best match of a node x is a node y such that merging node y with x will result in the smallest possible bounding
//box out of all possible nodes that could be merged with x
static inline node_pair find_best_match(BvhNode** nodes, uint32_t numNodes, size_t target_node){

    //get first valid partnership
    uint32_t i = 0;
    while (i == target_node || nodes[i] == NULL) ++i;

    node_pair pair = {.partner = i, .volume = calculate_merged_surface_area(nodes[i], nodes[target_node])};

    //perform linear search for any better ones
    for (++i; i < numNodes; i++){
        if (i == target_node || nodes[i] == NULL) continue;
        const float volume = calculate_merged_surface_area(nodes[i], nodes[target_node]);
        if (volume < pair.volume){
            pair.volume = volume;
            pair.partner = i;
        }
    }
    return pair;
}

//will perform linear search through all nodes in the array *pairs* and return the best possible pair of nodes to merge
static inline uint32_t get_best_node_pair(node_pair* pairs, uint32_t numNodes){
    uint32_t best;
    uint32_t i = 0;
    while (pairs[i].partner >= numNodes) i++;

    for (best = i++; i < numNodes; i++){
        if (pairs[i].volume < pairs[best].volume && pairs[i].partner < numNodes) best = i;
    }
    return best;
}


//partitions a subsection of the given array based on the nodes' morton codes
//it compares the morton codes of the first and last nodes in the array and finds the MSB that is different
//it then does a binary search in the sub array to find the element such that every node to the left has the
//different bit set to 0, and every node to the right plus that node has it set to 1
//it then returns this node
static uint32_t partition_nodes(BvhNode** nodes, uint32_t start, uint32_t numNodes){
    uint32_t startMortonCode = nodes[start]->morton_code;
    uint32_t endMortonCode = nodes[start + numNodes - 1]->morton_code;

    if (startMortonCode == endMortonCode) return numNodes / 2; //all morton codes the same, split down middle

    uint32_t different_bits = startMortonCode ^ endMortonCode;
    uint64_t leading_bit = 1;
    while ((leading_bit <<= 1) <= different_bits){}
    leading_bit >>= 1; //we overshot by 1 bit before
    const uint64_t mask = (leading_bit << 1) - 1;

    //we split the nodes into 2 groups, left side has all nodes < leading_bit, right side all nodes >= leading_bit
    //do a binary search to find the middle
    uint32_t a = start, b = start + numNodes;
    uint32_t mid = (a + b) / 2;
    while (a < mid && mid < b){
        if ((nodes[mid]->morton_code & mask) < leading_bit) {
            a = mid;
        }
        else {
            b = mid;
        }
        mid = (a + b) / 2;
    }

    //make sure mid is the index of the last node with morton code < leading bit
    while ((nodes[mid]->morton_code & mask) >= leading_bit){
        --mid;
    }
    //return the starting index of the right hand side partition
    return mid + 1 - start;
}

//update c's bounds with the new bounding box made by merging nodes a and b together
static inline void merge_bounds(BvhNode * a, BvhNode* b, BvhNode* c){
    c->bounds[0].x = MIN(a->bounds[0].x, b->bounds[0].x);
    c->bounds[0].y = MIN(a->bounds[0].y, b->bounds[0].y);
    c->bounds[0].z = MIN(a->bounds[0].z, b->bounds[0].z);

    c->bounds[1].x = MAX(a->bounds[1].x, b->bounds[1].x);
    c->bounds[1].y = MAX(a->bounds[1].y, b->bounds[1].y);
    c->bounds[1].z = MAX(a->bounds[1].z, b->bounds[1].z);
}

//merges nodes a and b into a parent node
static BvhNode* combine_nodes(BvhNode* a, BvhNode* b) {

    BvhNode* newNode = Arena_Allocate(BvhNode, sizeof(*newNode), auxiliaryArena);
    newNode->children[0] = a;
    newNode->children[1] = b;
    newNode->morton_code = 0;
    newNode->triangleIndex = (uint32_t) -1;
    merge_bounds(a, b, newNode);
    return newNode;
}


//determine how many nodes we should be left with after clustering
static uint32_t calculate_target_node_count(uint32_t numNodes){
    return MAX(numNodes / 2, 1);
}

//we perform a preorder traversal of the tree and add each node we visit to an array
//use a stack to keep track of which nodes have been visited
static BvhNodePublic* flatten_tree(BvhNode* root, uint32_t numTriangles, uint32_t* arraySize){
    BvhNodePublic* array = Arena_Allocate(BvhNodePublic, (sizeof *array) * numTriangles * 2, mainArena);
    flattening_stack* stack = Arena_Allocate(flattening_stack, (sizeof *stack) * numTriangles * 2, auxiliaryArena);

    stack[0].node = root;
    stack[0].parentIndex = (uint32_t) -1;
    int ptr = 0;
    uint32_t index = 0;

    while (ptr >= 0) {
        assert(index < numTriangles * 2);
        BvhNode * node = stack[ptr].node;

        if (node->children[0] == NULL && node->children[1] == NULL) {
            if (node->triangleIndex != (uint32_t) - 1) { //node is a leaf
                array[index].triangleIndex = node->triangleIndex;
                array[index].bounds[0] = node->bounds[0];
                array[index].bounds[1] = node->bounds[1];
                array[index++].leaf = 1;
            }
            --ptr;
            continue;
        }

        assert(node->triangleIndex == (uint32_t) - 1);

        if (node->children[0] != NULL) {
            stack[ptr].parentIndex = index;
            stack[++ptr].node = node->children[0];
            stack[ptr].parentIndex = (uint32_t) - 1;

            node->children[0] = NULL;
            array[index].bounds[0] = node->bounds[0];
            array[index].bounds[1] = node->bounds[1];

            array[index++].leaf = 0;
            continue;
        }

        if (node->children[1] != NULL) {
            assert(stack[ptr].parentIndex != (uint32_t) - 1);
            assert(stack[ptr].parentIndex < index);
            array[stack[ptr].parentIndex].childIndex = index; //update parent node with index of the right child
            stack[++ptr].node = node->children[1];
            node->children[1] = NULL;
            continue;
        }
    }

    *arraySize = index;
    return array;
}

//takes an array of triangles and returns a bounding volume hierarchy
BvhNodePublic* create_bvh(Triangle* triangles, uint32_t numTriangles, MemoryArena* arena, uint32_t* arraySize){
    printf("Num triangles = %u\n", numTriangles);
    mainArena = arena;
    auxiliaryArena = create_arena((sizeof *auxiliaryArena) * 16 * numTriangles);
    trianglePrimitives = triangles;

    BvhTriangle* bvhTriangles = Arena_Allocate(BvhTriangle, (sizeof *bvhTriangles) * numTriangles, auxiliaryArena);

    bvhTriangles[0].index = 0;
    bvhTriangles[0].centre = calculate_centres(&triangles[0]);

    Vec3 MinBound = bvhTriangles[0].centre;
    Vec3 MaxBound = bvhTriangles[0].centre;


    for (uint32_t i = 1; i < numTriangles; i++){
        bvhTriangles[i].index = i;
        bvhTriangles[i].centre = calculate_centres(&triangles[i]);

        for (int j = 0; j < 3; j++){
            if (bvhTriangles[i].centre.data[j] > MaxBound.data[j]){
                MaxBound.data[j] = bvhTriangles[i].centre.data[j];
            }
            else if (bvhTriangles[i].centre.data[j] < MinBound.data[j]){
                MinBound.data[j] = bvhTriangles[i].centre.data[j];
            }
        }
    }
    generate_morton_codes(MinBound, MaxBound, bvhTriangles, numTriangles);

    //in the future this should be replaced by a radix sort for potentially significant improvements
    qsort(bvhTriangles, numTriangles, sizeof(BvhTriangle), morton_comp);

    BvhNode* root = approximate_agglomerative_clustering(bvhTriangles, numTriangles);


    BvhNodePublic* flattenedBvh = flatten_tree(root, numTriangles, arraySize);
    delete_arena(auxiliaryArena);
    return flattenedBvh;
}



