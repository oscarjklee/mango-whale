/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef MANGO_WHALE_RENDER_H
#define MANGO_WHALE_RENDER_H

#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <stdint.h>
#include "obj_loader.h"
#include "linear_algebra.h"

void init_gl(uint32_t width, uint32_t height);
void load_model(MemoryArena *arena, ObjData *data);
void render(uint32_t width, uint32_t height);
void render_result(uint8_t *framebuffer, uint8_t *indirectFramebuffer, uint8_t *directFramebuffer, uint8_t *cacheRecordFramebuffer, uint32_t width, uint32_t height);

uint32_t update_camera(float width, float height, Mat4 *viewRet);


#endif //MANGO_WHALE_RENDER_H

