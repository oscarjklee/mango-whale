/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */

#include "camera.h"
#include <math.h>
#include <assert.h>
#include "linear_algebra.h"
#include "sample.h"

#define EPSILON 0.00000005
static Vec3 get_screen_coords(float i, float j);

static float framebufferWidth;
static float framebufferHeight;



void init_camera(uint32_t width, uint32_t height){
    framebufferWidth = (float) width;
    framebufferHeight = (float) height;
}


Ray get_next_ray(uint32_t row, uint32_t col){

    float xShift = uniform_sample();
    float yShift = uniform_sample();
    Vec3 direction = Normalise3(get_screen_coords((float) col + xShift, (float) row + yShift));

    Ray ray = {
            .direction = direction,
            .origin = (Vec3) {.x = 0, .y = 0, .z = 0},
            .inverse = InverseVector(direction),
    };
    return ray;
}


static Vec3 get_screen_coords(float i, float j){

    //i, j are in device coordinate system, convert to normalised device coordinate system
    float x = (2 * (i / framebufferWidth)) - 1;
    float y = (2 * (j / framebufferHeight)) - 1;

    float aspectRatio = framebufferWidth / framebufferHeight;
    //scale ndcs by aspect ratio
    if (aspectRatio > 1){
        x *= aspectRatio;
    }
    else {
        y /= aspectRatio;
    }
    return (Vec3) {.x = x, .y = y, .z = -1};
}


/*
 * creates a perspective projection matrix, based on glm::perspective
 */

Mat4 create_projection_matrix(float fov, float aspectRatio, float nearPlane, float farPlane){


    float tanVal = tanf(fov / 2);

    Mat4 ans = {0};
    ans.data[0][0] = 1 / (aspectRatio * tanVal);
    ans.data[1][1] = 1 / tanVal;
    ans.data[2][2] = -(farPlane + nearPlane) / (farPlane - nearPlane);
    ans.data[2][3] = -1;
    ans.data[3][2] = -(2 * farPlane * nearPlane) / (farPlane - nearPlane);

    Mat4 ret;
    Transpose4(&ans, &ret);

    return ret;
}



/*
 * creates a view matrix, based on glm::lookAt
 */

Mat4 create_view_matrix(Vec3 eye, Vec3 centre, Vec3 up){

    Vec3 f = Normalise3(Vec3MinusVec3(centre, eye));
    Vec3 u = Normalise3(up);
    Vec3 s = Normalise3(CrossProduct3(f, u));
    u = CrossProduct3(s, f);

    Mat4 ans = Identity4;
    ans.data[0][0] = s.x;
    ans.data[1][0] = s.y;
    ans.data[2][0] = s.z;
    ans.data[0][1] = u.x;
    ans.data[1][1] = u.y;
    ans.data[2][1] = u.z;
    ans.data[0][2] = -f.x;
    ans.data[1][2] = -f.y;
    ans.data[2][2] = -f.z;
    ans.data[3][0] = -DotProduct3(s, eye);
    ans.data[3][1] = -DotProduct3(u, eye);
    ans.data[3][2] =  DotProduct3(f, eye);

    Mat4 ret;
    Transpose4(&ans, &ret);
    return ret;
}





