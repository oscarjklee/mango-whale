//
// Created by oscar on 2/5/24.
//

#ifndef MANGO_WHALE_SHAPES_H
#define MANGO_WHALE_SHAPES_H
#include "linear_algebra.h"
#include "common_definitions.h"

float calculate_spherical_triangle_area(Vec3 *vertices);
Vec3 get_random_point_on_triangle(Triangle *triangle);

#endif //MANGO_WHALE_SHAPES_H
