/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MANGO_WHALE_SHADER_H
#define MANGO_WHALE_SHADER_H
#include <stdint.h>
#include "linear_algebra.h"
#include "light.h"
#include "obj_loader.h"
#include "common_definitions.h"
#include "ray.h"

void init_shader(ObjData *obj, Triangle *triangleArr, MemoryArena *arena, Triangle *portalTris, size_t numPortalTris);
Vec3 sample_texture(Vec2* texCoords, Vec3 barycentrics, size_t materialIndex);
Vec3 next_event_estimation(Vec3 *shadingNormals, Vec3 surfaceNormal, uint32_t* threadBuffer, Vec3 barycentrics, Vec3 point, uint32_t numSamples);
float get_light_sample_probability(uint32_t *threadBuffer, Ray shadowRay, int32_t lightIndex);
Vec3 get_skybox_colour(Vec3 dir);
Vec3 sample_skybox(uint32_t *threadBuffer, Vec3 point, Vec3 normal);


#endif //MANGO_WHALE_SHADER_H

