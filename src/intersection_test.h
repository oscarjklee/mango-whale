/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MANGO_WHALE_INTERSECTION_TEST_H
#define MANGO_WHALE_INTERSECTION_TEST_H
#include "common_definitions.h"
#include "bounding_volume_hierarchy.h"
#include "ray.h"
#include "linear_algebra.h"
#include <stdint.h>

void init_intersection(BvhNodePublic* bvh, BvhNodePublic *lightBvh, Triangle* triangles, Triangle *emissiveTriangles);
uint32_t intersection_test(uint32_t *indexStack, Ray ray, Vec3* barycentricCoords, Vec3* intersectionPoint, float maxDistance, float minDistance);
uint32_t intersection_test_early_term(uint32_t *indexStack, Ray ray, float maxTval, float minTval);
uint64_t get_num_casts();

uint32_t intersection_test_all_lights(uint32_t *indexStack, uint32_t *returnIndices, Ray ray);
#endif //MANGO_WHALE_INTERSECTION_TEST_H
