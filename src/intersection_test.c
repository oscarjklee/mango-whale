/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */

#include "intersection_test.h"
#include "linear_algebra.h"
#include <float.h>
#include <assert.h>
#include <omp.h>
#include <stdatomic.h>
#include <stdint.h>
#include <stdio.h>


#define EPSILON 0.000005

static BvhNodePublic* bvhArr;
static BvhNodePublic* lightBvhArr;

static Triangle* trianglesArr;
static Triangle* emissiveTrianglesArr;

static atomic_uint_least64_t numCasts = 0;


static Vec3 calculate_triangle_ray_barycentrics(Triangle* triangle, Ray ray);
static float ray_box_intersection_test(Ray ray, Vec3 bounds1, Vec3 bounds2, float maxDistance, float minDistance);
static inline size_t check_barycentrics(Vec3 barycentrics);

void init_intersection(BvhNodePublic* bvh, BvhNodePublic *lightBvh, Triangle* triangles, Triangle *emissiveTriangles){
    bvhArr = bvh;
    lightBvhArr = lightBvh;
    trianglesArr = triangles;
    emissiveTrianglesArr = emissiveTriangles;
}

//searches the bvh for the closest triangle that the ray intersects and
//returns index of triangle that ray hit and barycentrics and intersection point
//returns (uint32_t)-1 if no intersection

uint32_t intersection_test(uint32_t *indexStack, Ray ray, Vec3* barycentricCoords, Vec3* intersectionPoint, float maxTval, float minTval){

    indexStack[0] = 0; //push bvh root onto stack
    float closestTval = maxTval;
    uint32_t closestIndex = 0;
    Vec3 intersection, closestBarycentrics;


    for (int32_t stackPointer = 0; stackPointer >= 0; stackPointer--){

        const uint32_t index = indexStack[stackPointer];
            
        //ray-AABB intersection test
        if (!bvhArr[index].leaf){
            float tVal = ray_box_intersection_test(ray, bvhArr[index].bounds[0], bvhArr[index].bounds[1], closestTval, minTval);
            if (tVal < closestTval && tVal >= minTval){
                indexStack[stackPointer++] = bvhArr[index].childIndex; //push right child
                indexStack[stackPointer++] = index + 1; //push left child onto stack
            }
            continue;
        }

        //leaf node, so ray-triangle intersection test
        Triangle* triangle = &trianglesArr[bvhArr[index].triangleIndex];

        Vec3 barycentrics = calculate_triangle_ray_barycentrics(triangle, ray);
        if (!check_barycentrics(barycentrics)) continue; //no intersection

        Mat3 transpose;
        Transpose3(&triangle->data, &transpose);
        Vec3 coords = Mat3xVec3(&transpose, barycentrics); //calculate intersection coords from barycentrics

        int shift = (ray.direction.x != 0) ? 0 : ((ray.direction.y != 0) ? 1 : 2);
        float tVal = (coords.data[shift] - ray.origin.data[shift]) / ray.direction.data[shift]; //get the t val

        if (tVal < closestTval && tVal >= minTval){ //check the t val is in range
            intersection = coords;
            closestTval = tVal;
            closestBarycentrics = barycentrics;
            closestIndex = bvhArr[index].triangleIndex;
        }
    }

    if (closestTval == maxTval){
        return (uint32_t) -1;
    }

    *barycentricCoords = closestBarycentrics;
    *intersectionPoint = intersection;
    return closestIndex;
}



uint32_t intersection_test_all_lights(uint32_t *indexStack, uint32_t *returnIndices, Ray ray){

    indexStack[0] = 0; //push bvh root onto stack
    const float maxDistance = 999999999.f;
    uint32_t numIntersections = 0;

    for (int32_t stackPointer = 0; stackPointer >= 0; stackPointer--){
        
        const uint32_t index = indexStack[stackPointer];

        //ray-AABB intersection test
        if (!lightBvhArr[index].leaf){
            float tVal = ray_box_intersection_test(ray, lightBvhArr[index].bounds[0], lightBvhArr[index].bounds[1], maxDistance, 0.f);
            if (tVal != FLT_MAX){
                indexStack[stackPointer++] = lightBvhArr[index].childIndex; //push right child
                indexStack[stackPointer++] = index + 1; //push left child onto stack
            }
            continue;
        }

        //leaf node, so ray-triangle intersection test
        Triangle* triangle = &emissiveTrianglesArr[lightBvhArr[index].triangleIndex];
        Vec3 barycentrics = calculate_triangle_ray_barycentrics(triangle, ray);
        if (!check_barycentrics(barycentrics)) continue; //no intersection

        returnIndices[numIntersections++] = lightBvhArr[index].triangleIndex;
    }

    return numIntersections;
}




//searches the bvh for a triangle that intersects with the ray
//stops searching and immediately returns 1 if an intersection is found
//returns 0 if no intersection
uint32_t intersection_test_early_term(uint32_t *indexStack, Ray ray, float maxTval, float minTval){
    int32_t stackPointer = 0;
    indexStack[0] = 0; //push bvh root onto stack


    while (stackPointer >= 0){
        uint32_t index = indexStack[stackPointer];

        //if leaf node, check if ray intersects triangle
        if (bvhArr[index].leaf){
            Triangle* triangle = &trianglesArr[bvhArr[index].triangleIndex];

            Vec3 barycentrics = calculate_triangle_ray_barycentrics(triangle, ray);

            if (!check_barycentrics(barycentrics)) { //ray does not intersect with triangle
                --stackPointer;
                continue;
            }

            Mat3 transpose;
            Transpose3(&triangle->data, &transpose);
            Vec3 coords = Mat3xVec3(&transpose, barycentrics); //calculate intersection coords from barycentrics

            Vec3 intersectionVector = Vec3MinusVec3(coords, ray.origin); //vector from ray origin to intersection
            assert(ray.direction.x);
            float tVal = intersectionVector.x / ray.direction.x; //get the t val

            if (tVal < maxTval && tVal >= minTval){ //check the t val is in range
                return 1; //ray hit
            }

            --stackPointer;
            continue;
        }
        //check if ray intersects node
        float tVal = ray_box_intersection_test(ray, bvhArr[index].bounds[0], bvhArr[index].bounds[1], maxTval, minTval);
        if (tVal < maxTval && tVal >= minTval){
            indexStack[stackPointer++] = bvhArr[index].childIndex; //push right child
            indexStack[stackPointer++] = index + 1; //push left child onto stack
        }
        --stackPointer;
    }
    return 0; //no hit
}



static inline size_t check_barycentrics(Vec3 barycentrics){
    return (1 - EPSILON <= barycentrics.x + barycentrics.y + barycentrics.z &&
            barycentrics.x + barycentrics.y + barycentrics.z <= 1 + EPSILON &&
            -EPSILON <= barycentrics.x && - EPSILON <= barycentrics.y && -EPSILON <= barycentrics.z);
}


static inline Vec3 calculate_triangle_ray_barycentrics(Triangle* triangle, Ray ray){

    if (DotProduct3(triangle->normal, ray.direction) == 0){
        return (Vec3) {.x = 0, .y = 0, .z = 0};
    }

    const float mult = -1.f / DotProduct3(triangle->normal, ray.direction);

    const Vec3 originEdge = Vec3MinusVec3(ray.origin, triangle->points[0]);
    const Vec3 temp = CrossProduct3(originEdge, ray.direction);

    Vec3 column = {.x =  DotProduct3(triangle->normal, originEdge),
                         .y =  DotProduct3(temp, triangle->edge2),
                         .z = -DotProduct3(temp, triangle->edge1)
    };
    
    column = Vec3xScalar(column, mult);
    column.x = 1.f - column.y - column.z;

    return column;
}




static inline float ray_box_intersection_test(Ray ray, Vec3 bounds1, Vec3 bounds2, float maxDistance, float minDistance){

    Vec3 lower = Vec3MinusVec3(bounds1, ray.origin);
    lower = ComponentMult(lower, ray.inverse);

    Vec3 upper = Vec3MinusVec3(bounds2, ray.origin);
    upper = ComponentMult(upper, ray.inverse);

    const Vec4 min = {.x = MIN(lower.x, upper.x), .y = MIN(lower.y, upper.y), .z = MIN(lower.z, upper.z), .w = minDistance};
    const Vec4 max = {.x = MAX(lower.x, upper.x), .y = MAX(lower.y, upper.y), .z = MAX(lower.z, upper.z), .w = maxDistance};

    const float max1 = MAX(min.x, min.y);
    const float max2 = MAX(min.z, min.w);

    const float min1 = MIN(max.x, max.y);
    const float min2 = MIN(max.z, max.w);


    float tMin = MAX(max1, max2);
    float tMax = MIN(min1, min2);


    if (tMin <= tMax && tMax > 0){
        return tMin >= 0 ? tMin : tMax;
    }
    return FLT_MAX;
}


uint64_t get_num_casts(){
    return (uint64_t) numCasts;
}

