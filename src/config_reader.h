/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef MANGO_WHALE_CONFIG_READER_H
#define MANGO_WHALE_CONFIG_READER_H
#include <stdint.h>

typedef struct ConfigParameters{
    char objFile[1024];
    char directory[1024];
    char outputFile[1024];

    uint32_t screenWidth;
    uint32_t screenHeight;

    uint32_t minSamples;
    uint32_t maxSamples;
    uint32_t maxDepth;
    uint32_t lightSamplesLevel1;
    uint32_t lightSamplesLevel2;
    float tolerance;

    uint32_t gradients;
    float minDistance;
    float maxDistance;

    uint32_t ambientOcclusionRender;

} ConfigParameters;

uint32_t parse_config_file(char *filename, ConfigParameters *configParameters);
void create_config_file(char *filename);


#endif //MANGO_WHALE_CONFIG_READER_H
