//
// Created by oscar on 2/5/24.
//

#include "shapes.h"
#include "linear_algebra.h"
#include "sample.h"
#include <assert.h>


#define EPSILON 0.00005f

float calculate_spherical_triangle_area(Vec3 *vertices){
    Vec3 a = vertices[0];
    Vec3 b = vertices[1];
    Vec3 c = vertices[2];
    assert(fabs(1 - Magnitude3(a)) <= 0.000005);
    assert(fabs(1 - Magnitude3(b)) <= 0.000005);
    assert(fabs(1 - Magnitude3(c)) <= 0.000005);


    float top = DotProduct3(a, CrossProduct3(b, c));
    float bottom = 1 + DotProduct3(a, b) + DotProduct3(a, c) + DotProduct3(b, c);

    float area = fabsf(2 * atan2f(top, bottom));
    assert(area <= 2 * 3.1416);
    return area;
}



//returns a random point on the given triangle
Vec3 get_random_point_on_triangle(Triangle *triangle){


    float a = uniform_sample();
    float b = uniform_sample() * (1 - a);
    float c = 1 - a - b;

    //a, b and c are NOT uniformly sampled -- E[a] > E[b]
    //so we have 3 biased barycentrics, but if we randomly assign each barycentric to a vertex all will be well

    size_t shift = (size_t) (uniform_sample() * 3);
    assert(shift == 0 || shift == 1 || shift == 2);

    Vec3 barycentrics;
    float coefficients[3] = {a, b, c};

    for (size_t i = 0; i < 3; i++){
        barycentrics.data[(i + shift) % 3] = coefficients[i];
    }

    assert(barycentrics.x >= 0);
    assert(barycentrics.y >= 0);
    assert(barycentrics.z >= 0);

    assert(barycentrics.x <= 1);
    assert(barycentrics.y <= 1);
    assert(barycentrics.z <= 1);


    Mat3 transpose;
    Transpose3(&triangle->data, &transpose);

    return Mat3xVec3(&transpose, barycentrics);

}



