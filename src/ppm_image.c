/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include "memory.h"
#include "ppm_image.h"

//takes an image, its width and height and writes it to a given filename
int write_image_to_ppm(char* filename, unsigned char* image, unsigned int width, unsigned int height){
    FILE* file = fopen(filename, "wb");
    if (file == NULL) return 1;

    fprintf(file, "P6\n %u %u\n 255\n", width, height);

    size_t write = fwrite(image, sizeof(unsigned char), width * height * 3, file);
    fclose(file);

    if (write != width * height * 3){
        printf("Error writing to file %s\n", filename);
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}

//reads from a given ppm file
//will parse the files width and height and store it in provided pointers
//if arena is set to NULL then it will allocate memory with malloc(), which will need to be freed later
//else it will store the image buffer inside the given arena
//returns pointer to raw RGB image data
unsigned char* read_image_from_ppm(char* filename, unsigned int* width, unsigned int* height, MemoryArena* arena){
    FILE* file = fopen(filename, "rb");
    if (file == NULL) goto fail;

    int currentChar = getc(file);
    for (; currentChar != ' ' && currentChar != '\n' && currentChar != '\t';currentChar = getc(file)); //strip magic number

    for (; currentChar == ' ' || currentChar == '\n' || currentChar == '\t'; currentChar = getc(file)); //strip whitespace

    char buffer[20];
    int bufferLen = 0;
    for (; currentChar != ' ' && currentChar != '\n' && currentChar != '\t'; currentChar = getc(file)){ //read width
        buffer[bufferLen++] = (char)currentChar;
        if (bufferLen == 19) goto fail; //width was too big
    }
    buffer[bufferLen] = '\0';

    *width = (unsigned int) atoi(buffer);

    for (; currentChar == ' ' || currentChar == '\n' || currentChar == '\t'; currentChar = getc(file)); //strip whitespace

    bufferLen = 0;
    for (; currentChar != ' ' && currentChar != '\n' && currentChar != '\t'; currentChar = getc(file)){ //read height
        buffer[bufferLen++] = (char)currentChar;
        if (bufferLen == 19) goto fail; //height was too big
    }
    buffer[bufferLen] = '\0';
    *height = (unsigned int) atoi(buffer);

    for (; currentChar == ' ' || currentChar == '\n' || currentChar == '\t'; currentChar = getc(file)); //strip whitespace
    for (; currentChar != ' ' && currentChar != '\n' && currentChar != '\t';currentChar = getc(file)); //strip max colour val

    //there is 1 remaining whitespace which we have just read, followed by the binary image
    unsigned char* image;
    const unsigned int numValues = *width * *height * 3;

    if (arena == NULL){ //no arena, use malloc
        image = malloc(numValues * sizeof(unsigned char));
    }
    else{
        image = Arena_Allocate(unsigned char, numValues * sizeof(unsigned char), arena);
    }

    if (image == NULL) goto fail;
    size_t read = fread(image, sizeof(unsigned char), numValues, file);
    if (read != numValues) goto fail_and_free;

    fclose(file);
    return image;


    fail_and_free:
    if (arena == NULL) free(image);

    fail:
    printf("Error reading file: %s\n", filename);
    fclose(file);
    return NULL;
}
