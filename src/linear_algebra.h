/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef MANGO_WHALE_LINEAR_ALGEBRA_H
#define MANGO_WHALE_LINEAR_ALGEBRA_H
#include <float.h>
#include <math.h>
#include <stdint.h>
#include <stddef.h>
#include <math.h>
#include <assert.h>
#include <stdio.h>


#define Vec3Print(vec) (printf("%s = (%lf, %lf, %lf)\n", #vec, vec.x, vec.y, vec.z))
#define Vec4Print(vec) (printf("%s = (%lf, %lf, %lf, %lf)\n", #vec, vec.x, vec.y, vec.z, vec.w))

#define MAX(a, b) (a > b ? a : b)
#define MIN(a, b) (a < b ? a : b)
#define SWAP(a, b) {typeof(a) temp = a; a = b; b = temp;}

typedef struct Vec2{
    union {
        struct {
            float x;
            float y;
        };
        float data[2];
    };
} Vec2;


typedef struct Vec3{
    union {
        struct {
            float x;
            float y;
            float z;
        };
        float data[3];
    };
} Vec3;

typedef struct Vec4{
    union {
        struct {
            float x;
            float y;
            float z;
            float w;
        };
        float data[4];
    };
} Vec4;

typedef struct Mat3{
    union {
        struct {
            Vec3 r0;
            Vec3 r1;
            Vec3 r2;
        };
        float data[3][3];
    };
} Mat3;

typedef struct Mat4{
    union {
        struct {
            Vec4 r0;
            Vec4 r1;
            Vec4 r2;
            Vec4 r3;
        };
        float data[4][4];
    };
} Mat4;



[[maybe_unused]] static Mat4 Identity4 = {.data = {
        {1, 0, 0, 0},
        {0, 1, 0, 0},
        {0, 0, 1, 0},
        {0, 0, 0, 1}
}};

[[maybe_unused]] static Mat3 Identity3 = {.data = {
        {1, 0, 0},
        {0, 1, 0},
        {0, 0, 1},
}};

[[maybe_unused]] static Mat4 Zero4 = {.data = {
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0},
        {0, 0, 0, 0}
}};

[[maybe_unused]] static Mat3 Zero3 = {.data = {
        {0, 0, 0},
        {0, 0, 0},
        {0, 0, 0},
}};



[[maybe_unused]] static Mat4 Negate4 = {.data = {
        {-1, 0, 0, 0},
        {0, -1, 0, 0},
        {0, 0, -1, 0},
        {0, 0, 0, 1}
}};

static inline float safe_sqrt(float x){

    float y = MAX(x, 0.f);
    assert(y >= 0.f);
    assert(!isnan(y));

    if (y <= FLT_MIN) return 0.f; //FPE without this... needs looking into further but im suspicous of -ffast-math

    return sqrtf(y);
}


static inline Vec3 Mat3xVec3(Mat3 *A, Vec3 B){
    Vec3 ans = {.x = 0,.y = 0,.z = 0};

    for (size_t i = 0; i < 3; i++){
        for (size_t j = 0; j < 3; j++){
            ans.data[i] += A->data[i][j] * B.data[j];
        }
    }
    return ans;
}

static inline Vec4 Mat4xVec4(Mat4 *A, Vec4 B){
    Vec4 ans = {.x = 0,.y = 0,.z = 0,.w = 0};
    for (size_t i = 0; i < 4; i++){
        for (size_t j = 0; j < 4; j++){
            ans.data[i] += A->data[i][j] * B.data[j];
        }
    }
    return ans;
}

static inline Vec3 Vec3PlusVec3(Vec3 A, Vec3 B){
    Vec3 ans = {
            .x = A.x + B.x,
            .y = A.y + B.y,
            .z = A.z + B.z
    };
    return ans;
}

static inline Vec2 Vec2PlusVec2(Vec2 A, Vec2 B){
    Vec2 ans;
    for (int i = 0; i < 2; i++){
        ans.data[i] = A.data[i] + B.data[i];
    }
    return ans;
}


static inline Vec4 Vec4PlusVec4(Vec4 A, Vec4 B){
    Vec4 ans;
    for (int i = 0; i < 4; i++){
        ans.data[i] = A.data[i] + B.data[i];
    }
    return ans;
}

static inline Vec3 Vec3MinusVec3(Vec3 A, Vec3 B){
    Vec3 ans = {
            .x = A.x - B.x,
            .y = A.y - B.y,
            .z = A.z - B.z
    };
    return ans;
}

static inline Vec4 Vec4MinusVec4(Vec4 A, Vec4 B){
    Vec4 ans = {
            .x = A.x - B.x,
            .y = A.y - B.y,
            .z = A.z - B.z,
            .w = A.w - B.w
    };
    return ans;
}


static inline void Mat3xMat3(Mat3 *A, Mat3 *B, Mat3 *ans){

    Mat3 ret = {0};
    for (int i = 0; i < 3; i++){
        for (int j = 0; j < 3; j++){
            for (int k = 0; k < 3; k++){
                ret.data[i][j] += (A->data[i][k] * B->data[k][j]);
            }
        }
    }
    *ans = ret;
}

static inline void Mat4xMat4(Mat4 *A, Mat4 *B, Mat4 *ans){

    Mat4 ret = {0};
    for (int i = 0; i < 4; i++){
        for (int j = 0; j < 4; j++){
            for (int k = 0; k < 4; k++){
                ret.data[i][j] += (A->data[i][k] * B->data[k][j]);
            }
        }
    }
    *ans = ret;
}

static inline Vec3 Vec3xScalar(Vec3 A, float b){
    Vec3 ans;
    ans.x = A.x * b;
    ans.y = A.y * b;
    ans.z = A.z * b;
    return ans;
}

static inline Vec2 Vec2xScalar(Vec2 A, float b){
    Vec2 ans;
    ans.x = A.x * b;
    ans.y = A.y * b;
    return ans;
}


static inline Vec4 Vec4xScalar(Vec4 A, float b){
    Vec4 ans;
    ans.x = A.x * b;
    ans.y = A.y * b;
    ans.z = A.z * b;
    ans.w = A.w * b;
    return ans;
}

static inline void Mat3xScalar(Mat3 *A, float b, Mat3 *ans){
    for (int i = 0; i < 3; i++){
        for (int j = 0; j < 3; j++){
            ans->data[i][j] = A->data[i][j] * b;
        }
    }
}

static inline void Transpose3(Mat3 *A, Mat3 *ans){

    //copy diagonal
    ans->data[0][0] = A->data[0][0];
    ans->data[1][1] = A->data[1][1];
    ans->data[2][2] = A->data[2][2];

    for (int i = 0; i < 3; i++){
        for (int j = i + 1; j < 3; j++){
            float temp = A->data[i][j];
            ans->data[i][j] = A->data[j][i];
            ans->data[j][i] = temp;
        }
    }
    
}

static inline void Transpose4(Mat4 *A, Mat4 *ans){

    //copy diagonal
    ans->data[0][0] = A->data[0][0];
    ans->data[1][1] = A->data[1][1];
    ans->data[2][2] = A->data[2][2];
    ans->data[3][3] = A->data[3][3];

    for (int i = 0; i < 4; i++){
        for (int j = i + 1; j < 4; j++){
            float temp = A->data[i][j];
            ans->data[i][j] = A->data[j][i];
            ans->data[j][i] = temp;
        }
    }
}


static inline void Mat4xScalar(Mat4* A, float b, Mat4* ans){
    for (int i = 0; i < 4; i++){
        for (int j = 0; j < 4; j++){
            ans->data[i][j] = A->data[i][j] * b;
        }
    }
}

//returns 0 if normal, else 1 if nan/inf/other non real number
static inline uint32_t check_nan(Vec3 A){
    return (!isnormal(A.x) && !isnormal(A.y) &&!isnormal(A.z)) && (A.x != 0 && A.y != 0 && A.z != 0); 
}

static inline float Magnitude3(Vec3 A){

    // if (check_nan(A)) return 0;

    // Vec3Print(A);
    float a = A.x * A.x;
    float b = A.y * A.y;
    float c = A.z * A.z;

    float x = a + b + c;    
    return safe_sqrt(x);
}

static inline float Magnitude4(Vec4 A){
    return safe_sqrt((A.x * A.x) + (A.y * A.y) + (A.z * A.z) + (A.w * A.w));
}

static inline Vec3 Normalise3(Vec3 A){

    if (A.x == 0 && A.y == 0 && A.z == 0) return (Vec3) {.x = 0, .y = 0, .z = 0};

    assert(!isnan(A.x) && !isnan(A.y) && !isnan(A.z));
    float mag = Magnitude3(A);
    if (mag == 0) Vec3Print(A);
    return Vec3xScalar(A, 1 / Magnitude3(A));
}

static inline float DotProduct3(Vec3 A, Vec3 B){
    float x = 0;
    for (int i = 0; i < 3; i++){
        x += A.data[i] * B.data[i];
    }
    return x;
}

static inline Vec3 CrossProduct3(Vec3 A, Vec3 B){
    Vec3 ans;
    ans.x = (A.y * B.z) - (A.z * B.y);
    ans.y = (A.z * B.x) - (A.x * B.z);
    ans.z = (A.x * B.y) - (A.y * B.x);
    return ans;
}

static inline Vec3 ComponentMult(Vec3 A, Vec3 B){
    Vec3 ans;
    ans.x = A.x * B.x;
    ans.y = A.y * B.y;
    ans.z = A.z * B.z;
    return ans;
}

static inline float Distance3(Vec3 A, Vec3 B){
    return Magnitude3(Vec3MinusVec3(A, B));
}

//returns 1 if A == B (error for each element <= eps), else returns 0
static inline int32_t Compare3(Vec3 A, Vec3 B, float eps){
    for (int i = 0; i < 3; ++i) {
        if (fabs(A.data[i] - B.data[i]) > eps) return 0;
    }
    return 1;
}

static inline Vec3 Reflect(Vec3 dir, Vec3 normal){
    float angle = 2 * DotProduct3(dir, normal);
    Vec3 diff = Vec3xScalar(normal, angle);
    Vec3 ans = Vec3MinusVec3(dir, diff);
    return Normalise3(ans);
}

static inline Vec3 InverseVector(Vec3 A){

    if (A.x == 0) A.x = 0.00001f;
    if (A.y == 0) A.y = 0.00001f;
    if (A.z == 0) A.z = 0.00001f;

    Vec3 ans = {
            .x = (float) 1 / A.x,
            .y = (float) 1 / A.y,
            .z = (float) 1 / A.z,
    };
    return ans;
}


static inline Vec4 Vec3ToVec4(Vec3 A, float w){
    Vec4 ans = {
            .x = A.x,
            .y = A.y,
            .z = A.z,
            .w = w
    };
    return ans;
}


static inline Vec3 Vec4ToVec3(Vec4 A){
    
    Vec3 ans = {
            .x = A.x / A.w,
            .y = A.y / A.w,
            .z = A.z / A.w
    };
    return ans;
}

// creates a rotation matrix that will rotate the given vector to the given direction
static inline Mat4 get_rotation_matrix(Vec3 vector, Vec3 direction, float eps){


    if (DotProduct3(vector, direction) >= 1 - eps){ //vector already aligned with direction, no rotation needed
        return Identity4;
    }

    if (DotProduct3(vector, direction) <= eps - 1){ //vector is in opposite direction - just flip sign
         return Negate4;
    }

    const float angle = (float) acosf(DotProduct3(vector, direction));
    const float sinAngle = (float) sin(angle);
    const float cosfAngle = (float) cosf(angle);
    const float oneMinuscosf = 1 - cosfAngle;

    Vec3 rotationAxis = Normalise3(CrossProduct3(vector, direction));

    Mat4 rotationMatrix = {.data =
            {
                    {
                            (rotationAxis.x * rotationAxis.x * oneMinuscosf) + cosfAngle,
                            (rotationAxis.x * rotationAxis.y * oneMinuscosf) - (sinAngle * rotationAxis.z),
                            (rotationAxis.x * rotationAxis.z * oneMinuscosf) + (sinAngle * rotationAxis.y),
                            0
                    },

                    {       
                            (rotationAxis.x * rotationAxis.y * oneMinuscosf) + (sinAngle * rotationAxis.z),
                            (rotationAxis.y * rotationAxis.y * oneMinuscosf) + cosfAngle,
                            (rotationAxis.y * rotationAxis.z * oneMinuscosf) - (sinAngle * rotationAxis.x),
                            0
                    },

                    {   
                            (rotationAxis.x * rotationAxis.z * oneMinuscosf) - (sinAngle * rotationAxis.y),
                            (rotationAxis.y * rotationAxis.z * oneMinuscosf) + (sinAngle * rotationAxis.x),
                            (rotationAxis.z * rotationAxis.z * oneMinuscosf) + cosfAngle,
                            0
                    },

                    {
                            0, 0, 0, 1
                    }
            }
    };
    return rotationMatrix;
}

/*
 * multiplies a given 3x1 point/vector by a given 4x4 matrix, converts vector to 4x1 first then converts back
 */

static inline Vec3 transform_point(Mat4 *matrix, Vec3 point){
    Vec4 newVec = Vec3ToVec4(point, 1);
    Vec4 transformed = Mat4xVec4(matrix, newVec);
    Vec3 ans = Vec4ToVec3(transformed);

    return ans;
}


static inline Vec3 ClampDown(Vec3 A, Vec3 clamp){
    Vec3 ans = {
            .x = MIN(A.x, clamp.x),
            .y = MIN(A.y, clamp.y),
            .z = MIN(A.z, clamp.z)
    };
    return ans;
}

static inline Vec3 ClampUp(Vec3 A, Vec3 clamp){
    Vec3 ans = {
            .x = MAX(A.x, clamp.x),
            .y = MAX(A.y, clamp.y),
            .z = MAX(A.z, clamp.z)
    };
    return ans;
}



static inline Vec3 ShiftPoint(Vec3 A, Vec3 dir){
    const float stepSize = 0.000005f;
    return Vec3PlusVec3(A, Vec3xScalar(dir, stepSize));
}


/*
 * chops off the translation component of a provided view matrix to get a rotation matrix
*/
static inline void get_rotation_from_view(Mat4 *viewMatrix, Mat4 *ret){
    Mat4 ans = *viewMatrix;
    ans.data[0][3] = 0;
    ans.data[1][3] = 0;
    ans.data[2][3] = 0;
    ans.data[3][3] = 1;
    *ret = ans;
}



#endif //MANGO_WHALE_LINEAR_ALGEBRA_H
