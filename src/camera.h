/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MANGO_WHALE_CAMERA_H
#define MANGO_WHALE_CAMERA_H
#include "common_definitions.h"
#include "ray.h"

void init_camera(uint32_t width, uint32_t height);
Ray get_next_ray(uint32_t row, uint32_t col);
Mat4 create_projection_matrix(float fov, float aspectRatio, float nearPlane, float farPlane);
Mat4 create_view_matrix(Vec3 eye, Vec3 centre, Vec3 up);


#endif //MANGO_WHALE_CAMERA_H
