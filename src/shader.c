/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */

#include "shader.h"
#include "common_definitions.h"
#include "intersection_test.h"
#include "linear_algebra.h"
#include "memory.h"
#include "obj_loader.h"
#include "ray.h"
#include "sample.h"
#include "shapes.h"
#include <assert.h>
#include <math.h>
#include <stdint.h>
#include <omp.h>
#include <stdio.h>

#define EPSILON 0.0000005f

//mod but defined to return positive value in case of negative x
#define MOD(x, y) (x < 0 ? ((int64_t)x % (int64_t) y) + (int64_t) y - 1 : (int64_t) x % (int64_t) y)
#define DECIMAL(x) (x - truncf(x) + (float)(x < 0)) //return decimal component of x, add 1 if x is negative

static Triangle *triangles;
static ObjData *objData;
static const float pi = 3.14159265f;
static uint32_t **lightIndices;
static Triangle *portals;
static size_t numPortals;

static Vec3 calculate_diffuse(Light *light, Vec3 normal, Vec3 lightColour, Vec3 lightDirection, float projectedArea);
static Vec3 calculate_texel_colour(int64_t x, int64_t y, unsigned char* texture, uint32_t width);
static Vec3 calculate_shading_normals(Vec3 *shadingNormals, Vec3 barycentrics);

static Vec3 calculate_light_transmission(uint32_t *threadBuffer, Vec3 point, Light *light, Vec3 *dir, size_t depth);
static uint32_t check_found(size_t index, float target);
static size_t light_sample();
static Vec3 update_diffuse(uint32_t *threadBuffer, Light *light, Vec3 shadingNormal, Vec3 point, size_t triangleIndex);
static Vec3 get_skybox_coords(Vec3 dir);


//initialises static variables with constant parameters
void init_shader(ObjData *obj, Triangle *triangleArr, MemoryArena *arena, Triangle *portalTris, size_t numPortalTris){
    objData = obj;
    triangles = triangleArr;

    size_t numThreads = (size_t) omp_get_max_threads();
    lightIndices = Arena_Allocate(uint32_t *, numThreads * sizeof *lightIndices, arena);

    for (size_t i = 0; i < numThreads; i++){
        lightIndices[i] = Arena_Allocate(uint32_t, objData->numEmissives * sizeof *lightIndices[i], arena);
    }

    portals = portalTris;
    numPortals = numPortalTris;
}

//calculate how much light reaches the point from the light
//returns Vec3 representing light transmission in each rgb channel
static Vec3 calculate_light_transmission(uint32_t *threadBuffer, Vec3 point, Light *light, Vec3 *dir, size_t depth){

    if (depth == 0) return (Vec3) {.x = 0, .y = 0, .z = 0}; //recursion depth reached, assume full occlusion

    Ray shadowRay;
    shadowRay.direction = light ? Normalise3(Vec3MinusVec3(light->pos, point)) : * dir;
    shadowRay.inverse = InverseVector(shadowRay.direction);
    Vec3 shift = Vec3xScalar(shadowRay.direction, EPSILON); //shift ray along by some small epsilon
    shadowRay.origin = Vec3PlusVec3(point, shift);

    float distance = light ? Distance3(point, light->pos) * (1.f - EPSILON) : FLT_MAX;

    Vec3 intersection, barycentrics;
    uint32_t intersectionIndex = intersection_test(threadBuffer, shadowRay, &barycentrics, &intersection, distance, EPSILON);

    if (intersectionIndex == (uint32_t) -1) return (Vec3) {.x = 1, .y = 1, .z = 1}; //no intersections == no occlusion

    MtlMaterial *materialData = &objData->materials[objData->materialIndices[intersectionIndex]]; //pointer to relevant material data

    if (materialData->transparency == 0){
        if (materialData->luminosity == 0) return (Vec3) {.x = 0, .y = 0, .z = 0}; //intersection with opaque object == full occlusion

        return (Vec3) {.x = 1, .y = 1, .z = 1}; //intersected with another light... needs a proper system, but this works when theres only 1 light in the scene
    }

    Vec3 currentContribution = Vec3xScalar(materialData->transmissionFilter, materialData->transparency);

    shift = Vec3xScalar(shadowRay.direction, EPSILON); //shift ray along by some small epsilon
    Vec3 newPoint = Vec3PlusVec3(intersection, shift);

    return ComponentMult(currentContribution, calculate_light_transmission(threadBuffer, newPoint, light,  dir, depth - 1));
}


//returns weighted shading normals calculated via barycentric interpolation
static Vec3 calculate_shading_normals(Vec3 *shadingNormals, Vec3 barycentrics){
    Vec3 a = Vec3xScalar(shadingNormals[0], barycentrics.x);
    Vec3 b = Vec3xScalar(shadingNormals[1], barycentrics.y);
    Vec3 c = Vec3xScalar(shadingNormals[2], barycentrics.z);

    Vec3 shadingNormal = Vec3PlusVec3(Vec3PlusVec3(a, b), c);
    return shadingNormal;
}


//returns the colour stored in the texture for a given intersection point via barycentric interpolation
Vec3 sample_texture(Vec2 *texCoords, Vec3 barycentrics, size_t materialIndex){

    if (objData->materials[materialIndex].texture == NULL){
        return objData->materials[materialIndex].diffuse;
    }

    uint32_t textureWidth = objData->materials[materialIndex].textureWidth;
    uint32_t textureHeight = objData->materials[materialIndex].textureHeight;
    unsigned char *texture = objData->materials[materialIndex].texture;

    Vec2 weightedTexCoords[3]; //weight each vertex's texture coord by the barycentrics
    for(int i = 0; i < 3; i++) {
        weightedTexCoords[i] = Vec2xScalar(texCoords[i], barycentrics.data[i]);
    }
    //and add them together
    Vec2 finalTexCoord = Vec2PlusVec2(Vec2PlusVec2(weightedTexCoords[0], weightedTexCoords[1]), weightedTexCoords[2]);

    finalTexCoord.x *= (float) textureWidth; //scale texture by width
    finalTexCoord.y *= (float) textureHeight;

    Vec2 decimals = {.x = DECIMAL(finalTexCoord.x), .y = DECIMAL(finalTexCoord.y)}; //extract decimal part of coord

    finalTexCoord.x = (float) MOD(finalTexCoord.x, textureWidth); //clamp with integer modulo
    finalTexCoord.y = (float) MOD(finalTexCoord.y, textureHeight);

    float xCompMult = fabsf(0.5f - decimals.x);
    float yCompMult = fabsf(0.5f - decimals.y);

    //tex coords assume bottom left origin, texture itself is top left, so flip y axis
    finalTexCoord.y = (float) textureHeight - finalTexCoord.y - 1.f;

    //calculate main sample coords
    int64_t xCoord = (int64_t) (decimals.x < 0.5 ? finalTexCoord.x - 1 : finalTexCoord.x + 1);
    int64_t yCoord = (int64_t) (decimals.y < 0.5 ? finalTexCoord.y - 1 : finalTexCoord.y + 1);

    //take modulo again in case sample coords have caused wrapping
    xCoord = MOD(xCoord, textureWidth);
    yCoord = MOD(yCoord, textureHeight);

    //calculate values of 4 texels
    Vec3 texColour1 = calculate_texel_colour(xCoord, yCoord, texture, textureWidth);
    Vec3 texColour2 = calculate_texel_colour((int64_t) finalTexCoord.x, yCoord, texture, textureWidth);
    Vec3 texColour3 = calculate_texel_colour(xCoord, (int64_t) finalTexCoord.y, texture, textureWidth);
    Vec3 texColour4 = calculate_texel_colour((int64_t) finalTexCoord.x, (int64_t) finalTexCoord.y, texture, textureWidth);

    //scale appropriately
    texColour1 = Vec3xScalar(texColour1, xCompMult * yCompMult);
    texColour2 = Vec3xScalar(texColour2, (1.f - xCompMult) * yCompMult);
    texColour3 = Vec3xScalar(texColour3, xCompMult * (1.f -  yCompMult));
    texColour4 = Vec3xScalar(texColour4, (1.f - xCompMult) * (1.f - yCompMult));

    //add them together
    Vec3 filteredColour = Vec3PlusVec3(Vec3PlusVec3(texColour1, texColour2), Vec3PlusVec3(texColour3, texColour4));
    return filteredColour;
}


//calculates the rgb colour of a given texture coord
static Vec3 calculate_texel_colour(int64_t x, int64_t y, unsigned char* texture, uint32_t width){
    int64_t texIndex = (y * width) + x;
    Vec3 texColour = {
            .x = texture[(texIndex * 3) + 0],
            .y = texture[(texIndex * 3) + 1],
            .z = texture[(texIndex * 3) + 2]
    };
    texColour = Vec3xScalar(texColour, 1.f / 255.f);

    return texColour;
}


//returns lambertian shading
static Vec3 calculate_diffuse(Light *light, Vec3 normal, Vec3 lightColour, Vec3 lightDirection, float projectedArea){

    float dot = MAX(DotProduct3(normal, lightDirection), 0);

    return Vec3xScalar(lightColour, light->strength * dot * projectedArea);
}


/*
 * casts shadow ray(s) and calculates the diffuse shading from a given point to a given light
 */

static Vec3 update_diffuse(uint32_t *threadBuffer, Light *light, Vec3 shadingNormal, Vec3 point, size_t triangleIndex){

    //cast a shadow ray toward the light, records what light reaches the point
    Vec3 lightTransmission = calculate_light_transmission(threadBuffer, point, light, NULL, 16);

    Vec3 lightColour = ComponentMult(lightTransmission, light->colour);
    if (Compare3(lightColour, (Vec3){.x = 0, .y = 0, .z = 0}, EPSILON)) {
        return (Vec3) {.x = 0, .y = 0, .z = 0}; //light source fully occluded
    }

    //calculate diffuse values
    Vec3 lightDirection = Normalise3(Vec3MinusVec3(light->pos, point));

    float distance = Distance3(light->pos, point);
    float distanceScale = MIN(1.f / (distance * distance), 1.f);
    Vec3 lightNormal = Normalise3(triangles[triangleIndex].normal);
    
    float angleMult = fabsf(DotProduct3(Vec3xScalar(lightDirection, -1), lightNormal));
    float mult = angleMult * distanceScale;

    Vec3 newDiffuse = calculate_diffuse(light, shadingNormal, lightColour, lightDirection, mult);

    return newDiffuse;
}


/*
 * performs next event estimation by randomly sampling a point on a light source in the scene, from which it casts a
 * shadow ray and applies diffuse/lambertian shading
 */
Vec3 next_event_estimation(Vec3 *shadingNormals, Vec3 surfaceNormal, uint32_t *threadBuffer, Vec3 barycentrics, Vec3 point, uint32_t numSamples){

    if (objData->numEmissives == 0){
        if (objData->skyBox == NULL) return (Vec3){.x = 0, .y = 0, .z = 0};
        return sample_skybox(threadBuffer, point, surfaceNormal);
    }
    if (objData->skyBox){
        float rand = uniform_sample();
        if (rand > 0.5) return sample_skybox(threadBuffer, point, surfaceNormal);
    }

    //calculate smooth normal for shading
    Vec3 shadedNormal = calculate_shading_normals(shadingNormals, barycentrics);
    if (DotProduct3(shadedNormal, surfaceNormal) < 0){
        shadedNormal = Vec3xScalar(shadedNormal, -1); //ensure orientation with surface normal
    }

    Vec3 result = {.x = 0, .y = 0, .z = 0};

    for (uint32_t i = 0; i < numSamples; i++){
        //sample the a light in the scene
        size_t lightIndex = light_sample();
        size_t triangleIndex = objData->emissiveTriangles[lightIndex].triangleIndex;
        size_t materialIndex = objData->materialIndices[triangleIndex];

        //choose a random point on the light's surface
        Vec3 lightPoint = get_random_point_on_triangle(&triangles[triangleIndex]);
        Ray shadowRay;
        shadowRay.direction = Normalise3(Vec3MinusVec3(lightPoint, point));
        if (DotProduct3(surfaceNormal, shadowRay.direction) < 0) continue; //light is behind point
        shadowRay.origin = point;
        shadowRay.inverse = InverseVector(shadowRay.direction);


        float sampleProbability = get_light_sample_probability(threadBuffer, shadowRay, (int32_t) lightIndex);
        assert(sampleProbability <= 1);
        float lambertianProbability = MAX(DotProduct3(surfaceNormal, shadowRay.direction), 0.f) / pi;
        float balanceHeuristicMult = 1.f / (sampleProbability + lambertianProbability);


        // Vec3 cross = CrossProduct3(triangles[triangleIndex].edge1, triangles[triangleIndex].edge2);
        // float area = Magnitude3(cross) * 0.5;

        //create a light object (point light) at that point on the light's surface
        Light light = {
                .pos = lightPoint,
                .colour = objData->materials[materialIndex].emissive,
                .direction = Normalise3(triangles[triangleIndex].normal),
                .strength = pi * objData->emissiveTriangles[lightIndex].weighting * balanceHeuristicMult
        };

        result = Vec3PlusVec3(result, update_diffuse(threadBuffer, &light, shadedNormal, point, triangleIndex));
    }
    return Vec3xScalar(result, 1.f / (float) numSamples);

}

//returns the probability that the given light was sampled through the uniform power sampling
float get_light_sample_probability(uint32_t *threadBuffer, Ray shadowRay, int32_t lightIndex){

    if (objData->numEmissives == 0){
        if (objData->skyBox == NULL){
            return 1.f; //no lights and no skybox, sampling doesn't make sense
        }
        if (objData->numPortals == 0) return 1.f / (2 * pi); //no lights and no portals -- just random hemisphere sampling
    } 

    //there's at least 1 light or portal
    uint32_t *lights = lightIndices[omp_get_thread_num()];
    uint32_t numLights = intersection_test_all_lights(threadBuffer, lights, shadowRay);

    //check if floating point shenanigans caused a no reg for the light we're targetting...
    //this is very rare, but possible for rays cast at *very* grazing angles
    if (numLights == 0){
        if (lightIndex < 0) return 0.f;
        lights[0] = (uint32_t) lightIndex;
        numLights = 1;
    }

    float combinedWeight = 0.f;
    if (objData->numPortals == 0 && objData->skyBox) combinedWeight += 1.f / (2 * pi); //add probability of random uniform skybox sample

    for (uint32_t i = 0; i < numLights; i++){

        size_t lightIndex = lights[i];
        if (lightIndex >= objData->numEmissives){
            combinedWeight += 1.f / (float)objData->numPortals;
            continue;
        }
        float sampleWeight = objData->emissiveTriangles[lightIndex].weighting;
        if (lightIndex > 0){
            sampleWeight -= objData->emissiveTriangles[lightIndex - 1].weighting;
        }
        combinedWeight += sampleWeight;
    }
    if (objData->numEmissives > 0 && objData->skyBox) combinedWeight *= 0.5f; //50% chance of skybox sample, 50% of a light

    float totalWeight = objData->emissiveTriangles[objData->numEmissives - 1].weighting;
    return combinedWeight / totalWeight;

}

//performs a uniform sample on the lights in the scene, weighted by the lights intensity * area
//returns the index of the triangle that was selected
static size_t light_sample(){

    float totalWeight = objData->emissiveTriangles[objData->numEmissives - 1].weighting;
    float sample = uniform_sample() * totalWeight;

    //we are searching for a light x, such that lights[x - 1].weighting <= sample <= lights[x].weighting
    //we do this through a binary search on the lights

    size_t a = 0;
    size_t b = objData->numEmissives - 1;
    size_t mid = (a + b) / 2;

    while (!check_found(mid, sample)){
        if (objData->emissiveTriangles[mid].weighting < sample){
            a = mid + 1;
        }
        else {
            b = mid - 1;
        }
        mid = (a + b) / 2;
    }

    return mid;
}

//returns 1 if lights[index] is the targeted light
static uint32_t check_found(size_t index, float target){
    if (objData->emissiveTriangles[index].weighting >= target){
        return (index == 0 || objData->emissiveTriangles[index - 1].weighting <= target);
    }
    return 0;
}


Vec3 sample_skybox(uint32_t *threadBuffer, Vec3 point, Vec3 normal){
    
    Vec3 sampleDir;
    if (numPortals > 0){
        size_t sampleIndex = (size_t)(uniform_sample() * (float) numPortals);
        sampleIndex = MIN(sampleIndex, numPortals - 1);
        Triangle *portalTri = &portals[sampleIndex];
        Vec3 samplePoint = get_random_point_on_triangle(portalTri);
        sampleDir = Normalise3(Vec3MinusVec3(samplePoint, point));
    }
    else sampleDir = sample_hemisphere(normal);

    Vec3 transmission = calculate_light_transmission(threadBuffer, point, NULL, &sampleDir, 16);
    return ComponentMult(transmission, get_skybox_colour(sampleDir));    
}


Vec3 get_skybox_colour(Vec3 dir){
    if (objData->skyBox == NULL) return (Vec3) {.x = 0, .y = 0, .z = 0};
    Vec3 coords = get_skybox_coords(dir);
    coords.y = 1 - coords.y;

    uint8_t *texture = objData->skyBox->texture[(size_t) coords.z];
    uint32_t x = (uint32_t) ((float) objData->skyBox->textureWidth * coords.x);
    uint32_t y = (uint32_t) ((float) objData->skyBox->textureHeight * coords.y);

    uint32_t index = (y * objData->skyBox->textureHeight) + x;

    Vec3 rgb = {
        .x = texture[(index * 3) + 0] / 255.f,
        .y = texture[(index * 3) + 1] / 255.f,
        .z = texture[(index * 3) + 2] / 255.f
    };
    return rgb;
}


//returns a Vec3 containing the texture coordinates of a cubemap/skybox for a given direction
//.x and .y are the (u,v) coords respectively and .z is the texture index
//0 = +x, 1 = -x, 2 = +y, 3 = -y, 4 = +z, 5 = -z
//heavily inspired by wikipedia's example
static Vec3 get_skybox_coords(Vec3 dir){
    const float absX = fabsf(dir.x);
    const float absY = fabsf(dir.y);
    const float absZ = fabsf(dir.z);
    
    const float max = MAX(MAX(absX, absY), absZ);

    float uc, vc;
    Vec3 ret;

    if (dir.x > 0 && max == absX) { //+x
        uc = -dir.z;
        vc = dir.y;
        ret.z = 0;
    }
    else if (max == absX) { //-x
        uc = dir.z;
        vc = dir.y;
        ret.z = 1;
    }
    else if (dir.y > 0 && max == absY) { //+y
        uc = dir.x;
        vc = -dir.z;
        ret.z = 2;
    }
    else if (max == absY) { //-y
        uc = dir.x;
        vc = dir.z;
        ret.z = 3;
    }
    else if (dir.z > 0 && max == absZ) { //+z
        uc = dir.x;
        vc = dir.y;
        ret.z = 4;
    }
    else{
        //-z
        uc = -dir.x;
        vc = dir.y;
        ret.z = 5;
    }

    // Convert range from -1 to 1 to 0 to 1
    ret.x = 0.5f * (uc / max + 1.0f);
    ret.y = 0.5f * (vc / max + 1.0f);
    return ret;
}




