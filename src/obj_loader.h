
/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */




#ifndef SUPER_LIGHTWEIGHT_OBJ_LOADER_H
#define SUPER_LIGHTWEIGHT_OBJ_LOADER_H
#include "common_definitions.h"
#include "memory.h"
#include "linear_algebra.h"
#include <omp.h>
#include <stdint.h>

typedef struct MtlMaterial{
    uint8_t *texture;
    Vec3 diffuse;
    Vec3 ambient;
    Vec3 specular;
    Vec3 emissive;
    Vec3 transmissionFilter;
    float alpha;
    float transparency;
    float refractionIndex;
    float reflectivity; //non standard, 0 - 1, 1 = perfect mirror, represented by Nm
    float luminosity; //brightness of emission (non standard), represented by Lu
    uint32_t textureWidth;
    uint32_t textureHeight;
    uint32_t render;
} MtlMaterial;

typedef struct SkyBox{
    uint8_t *texture[6];
    uint32_t textureWidth;
    uint32_t textureHeight;
} SkyBox;

typedef struct EmissiveTriangle{
    size_t triangleIndex;
    float weighting; //light surface area * luminosity
} EmissiveTriangle;


typedef struct ObjData{
    SkyBox *skyBox;
    Vec3 *vertices;
    Vec3 *vertexNormals;
    Vec2 *textureCoords;
    Vec3 *portalVertices;
    MtlMaterial *materials;
    EmissiveTriangle *emissiveTriangles;
    size_t *materialIndices;
    size_t numVertices;
    size_t numEmissives;
    size_t numPortals;
}ObjData;


int load_obj_file(char* dir, char* file, MemoryArena* arena, ObjData *objData);



#endif //SUPER_LIGHTWEIGHT_OBJ_LOADER_H
