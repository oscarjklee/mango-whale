/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */


#include "cache.h"
#include "linear_algebra.h"
#include "memory.h"
#include <assert.h>
#include <omp.h>
#include <complex.h>
#include <float.h>
#include <math.h>
#include <stdint.h>
#include <stdio.h>
#include <stdatomic.h>
#include <sys/types.h>
#include <stdatomic.h>

#define PI 3.14159265f

static float minSpacing;
static float maxSpacing;

/*
 * contains all the data needed for the interpolation of a cache record (duh)
 */
typedef struct CacheRecord{
    omp_lock_t translationLock;
    Mat3 translationGradient;
    Mat3 rotationGradient;
    Vec3 position;
    Vec3 normal;
    Vec3 irradiance;
    _Atomic float minDistance;
    _Atomic float minDistanceClamped;
    float minBound;
    float maxBound;
} CacheRecord;


/*
 * cache node for the octree data structure
 */
typedef struct CacheNode{
    struct CacheNode* _Atomic children[8]; 
    CacheRecord * _Atomic * _Atomic records;
    Vec3 bounds[2];
    volatile atomic_uint_fast64_t numRecords;
    size_t recordCapacity;
    omp_lock_t childCreationLock;
    omp_lock_t insertionLock;
} CacheNode;



#define RECORDS_ARRAY_DEPTH 20

typedef struct Cache{
    CacheNode cacheRoot;
    CacheRecord *recordsArray[RECORDS_ARRAY_DEPTH];
    size_t recordsArraySize;
    size_t recordsArrayLevel;
    size_t recordsArrayCapacity;

    omp_lock_t recordAdditionLock;

    atomic_uint_fast64_t numRecords;
    atomic_uint_fast64_t numHits;
    atomic_uint_fast64_t numMisses;

    float errorTolerance;
    uint32_t useGradients;

} Cache;


static uint32_t sphere_box_intersection_test(Vec3 b1, Vec3 b2, Vec3 centre, float radius);
static void insert_into_node(CacheNode *node, CacheRecord *record);
static void insert_into_tree(CacheNode *node, CacheRecord *record, float sphereVolume, float radius);
static void create_node(CacheNode *parent, uint32_t index);

static uint32_t step_test(CacheRecord *record, Vec3 point, Vec3 normal);
static float calculate_weighting_alternative(Cache *cache, CacheRecord *record, Vec3 point, Vec3 normal);
static uint32_t get_child_node(CacheNode *node, Vec3 point);
static void clamp_min_distance(CacheRecord *record);
static Vec3 get_irradiance(Cache *cache, CacheRecord *record, Vec3 normal, Vec3 point);
static void gradient_limit_record(CacheRecord *record);
static void neighbour_clamping(Cache *cache, CacheRecord *record);
static void init_cache(Cache *cache, Vec3 minBound, Vec3 maxBound, float tolerance, uint32_t gradient);

static MemoryArena *arena;
static Cache *caches;
static Vec3 cameraOrigin;
static size_t numCaches;


/*
 * initialises the irradiance caching system
 * creates a cache for each recursive layer
 */
    
void init_irradiance_cache(MemoryArena *memoryArena, Vec3 minBound, Vec3 maxBound, Vec3 origin, float tolerance, uint32_t gradient, uint32_t depth, float minDistance, float maxDistance){
    
    if (tolerance == 0){
        numCaches = 0;
        return;
    }

    numCaches = MIN(depth - 1, 3);
    numCaches = depth - 1;
    arena = memoryArena;
    cameraOrigin = origin;
    minSpacing = minDistance;
    maxSpacing = maxDistance;
    caches = Arena_Allocate(Cache, sizeof *caches * numCaches, arena);

    float mult = 1;
    for (size_t i = 0; i < numCaches - 1; i++){
        init_cache(&caches[i], minBound, maxBound, tolerance * mult, gradient);
        mult = mult * sqrtf(2.f);
    }
                         
    //if theres only 1 cache and we've been told to turn on caching, then we do it anyway 
    //this is common in ambient occlusion renders for example
    init_cache(&caches[numCaches - 1], minBound, maxBound, tolerance * mult, numCaches == 1 && gradient);
}



/*
 * initialises an individual cache layer
 */

static void init_cache(Cache *cache, Vec3 minBound, Vec3 maxBound, float tolerance, uint32_t gradient){
    cache->errorTolerance = tolerance;
    cache->useGradients = gradient;

    for (size_t i = 0; i < 8; i++) cache->cacheRoot.children[i] = NULL;
    cache->cacheRoot.records = NULL;
    cache->cacheRoot.bounds[0] = minBound;
    cache->cacheRoot.bounds[1] = maxBound;
    cache->cacheRoot.numRecords = 0;
    cache->cacheRoot.recordCapacity = 0;
    omp_init_lock(&cache->cacheRoot.childCreationLock);
    omp_init_lock(&cache->cacheRoot.insertionLock);

    cache->recordsArrayCapacity = 1024;
    cache->recordsArray[0] = Arena_Allocate(CacheRecord, sizeof *cache->recordsArray[0] * cache->recordsArrayCapacity, arena);
    cache->recordsArraySize = 0;
    cache->recordsArrayLevel = 0;

    cache->numRecords = 0;
    cache->numHits = 0;
    cache->numMisses = 0;

    omp_init_lock(&cache->recordAdditionLock);
}

/*
 * updates the tolerance relative to the starting tolerance
 * eg, if starting tolerance was 1 and new tolerance is 2, the tolerance of all caches will be doubled
 */

void set_tolerance(float tolerance){
    float mult = tolerance / caches[0].errorTolerance;
    for (size_t i = 0; i < numCaches; i++){
        caches[i].errorTolerance *= mult;
    }
}


/*
 * functions for searching the cache for valid records
 */

/*
 * returns a Vec3 representing the irradiance at the given point
 * in the case of a cache miss returns {-1, -1, -1}
 */

Vec3 search_cache(Vec3 point, Vec3 normal, uint32_t level){
    assert(level > 0);
    assert(level <= numCaches);

    if (numCaches == 0 || caches[0].errorTolerance == 0){
        fprintf(stderr, "Warning, attempting to search cache when no caches exist\n");
        return (Vec3) {.x = -1, .y = -1, .z = -1};
    } 

    Cache *cache = &caches[level - 1];
    float totalWeighting = 0;
    Vec3 totalIrradiance = {.x = 0, .y = 0, .z = 0};

    CacheNode *node = &cache->cacheRoot;

    while (node != NULL) {
        for (size_t i = 0; i < node->numRecords; i++) {

            CacheRecord *record = (CacheRecord*) node->records[i];
            if (!step_test(record, point, normal)) continue;

            float recordWeighting = calculate_weighting_alternative(cache, record, point, normal);
            if (recordWeighting <= 0) continue;

            //valid record found, interpolate
            Vec3 interpolatedIrradiance = get_irradiance(cache, record, normal, point);
            Vec3 recordContribution = Vec3xScalar(interpolatedIrradiance, recordWeighting);

            totalIrradiance = Vec3PlusVec3(totalIrradiance, recordContribution);
            totalWeighting += recordWeighting;
        }

        uint32_t childIndex = get_child_node(node, point);
        node = node->children[childIndex];
    }

    if (totalWeighting == 0){ //no valid records found -- cache miss
        ++cache->numMisses;
         return (Vec3) {.x = -1, .y = -1, .z = -1};
    }

    // assert(totalIrradiance.x != -1);
    ++cache->numHits;
    return ClampUp(Vec3xScalar(totalIrradiance, 1 / totalWeighting), (Vec3) {.x = 0, .y = 0, .z = 0});

}

//applies gradients to the record's irradiance
static Vec3 get_irradiance(Cache *cache, CacheRecord *record, Vec3 normal, Vec3 point){

    if (!cache->useGradients) return record->irradiance;

    Vec3 axis = CrossProduct3(record->normal, normal);
    Mat3 rotation = {
        .r0 = Vec3xScalar(record->rotationGradient.r0, record->irradiance.x),
        .r1 = Vec3xScalar(record->rotationGradient.r1, record->irradiance.y),
        .r2 = Vec3xScalar(record->rotationGradient.r2, record->irradiance.z)
    };

    Vec3 rotationChange = {
        .x = DotProduct3(axis, rotation.r0),
        .y = DotProduct3(axis, rotation.r1),
        .z = DotProduct3(axis, rotation.r2)
    };

    Mat3 translation = {
        .r0 = Vec3xScalar(record->translationGradient.r0, record->irradiance.x),
        .r1 = Vec3xScalar(record->translationGradient.r1, record->irradiance.y),
        .r2 = Vec3xScalar(record->translationGradient.r2, record->irradiance.z)
    };

    Vec3 shift = Vec3MinusVec3(point, record->position);
    Vec3 translationChange = {
        .x = DotProduct3(shift, translation.r0),
        .y = DotProduct3(shift, translation.r1),
        .z = DotProduct3(shift, translation.r2)
    };
    // return record->irradiance;
    return Vec3PlusVec3(record->irradiance, Vec3PlusVec3(translationChange, rotationChange));
}


/*
 * step test to check if record is infront of point
 * returns 1 if point passes, 0 if fails
*/
static uint32_t step_test(CacheRecord *record, Vec3 point, Vec3 normal){

    Vec3 distance = Vec3MinusVec3(point, record->position);
    Vec3 norm = Vec3PlusVec3(normal, record->normal);
    norm = Vec3xScalar(norm, 0.5);

    float step = DotProduct3(distance, norm);
    return (step > -0.000005);
}



/*
 * calculates the weighting applied to a given record based off a point's position and normal
 */

static float calculate_weighting_alternative(Cache *cache, CacheRecord *record, Vec3 point, Vec3 normal){
    if (record->minDistanceClamped == 0) return 0;

    float scaledDistance = Distance3(record->position, point) / (0.5f * record->minDistanceClamped);

    float dot = DotProduct3(record->normal, normal);
    float top = safe_sqrt(1 - dot);
    float bottom = safe_sqrt(1.f - cosf((10.f / 90.f) * (PI / 2.f)));
    float scaledNormal = top / bottom;

    float mult = 1.f / cache->errorTolerance;
    float weighting = 1.f - (mult * MAX(scaledDistance, scaledNormal)); 
    return weighting;
}


/*
 * returns the index of the given node's child such that point lies inside the child
 */

static uint32_t get_child_node(CacheNode *node, Vec3 point){

    Vec3 nodeCentre = Vec3xScalar(Vec3PlusVec3(node->bounds[0], node->bounds[1]), 0.5);
    uint32_t index = 0;

    if (point.x > nodeCentre.x) index += 4;
    if (point.y > nodeCentre.y) index += 2;
    if (point.z > nodeCentre.z) index += 1;

    return index;
}



/*
 * functions for inserting a record into the cache
 */



static CacheRecord* store_record(Cache *cache, CacheRecord *record){
    CacheRecord *recordPointer;

    omp_set_lock(&cache->recordAdditionLock);

    if (cache->recordsArraySize == cache->recordsArrayCapacity){
        
        cache->recordsArrayLevel++;
        assert(cache->recordsArrayLevel < RECORDS_ARRAY_DEPTH);

        cache->recordsArrayCapacity *= 2;
        cache->recordsArray[cache->recordsArrayLevel] = Arena_Allocate(CacheRecord, sizeof *(cache->recordsArray[0]) * cache->recordsArrayCapacity, arena);
        cache->recordsArraySize = 0;
    }

    cache->recordsArray[cache->recordsArrayLevel][cache->recordsArraySize] = *record;
    recordPointer = &(cache->recordsArray[cache->recordsArrayLevel][cache->recordsArraySize]);
    cache->recordsArraySize++;

    omp_unset_lock(&cache->recordAdditionLock);
    return recordPointer;

}


/*
 * takes the components of a record and inserts it into the tree
 */

void insert_record(Mat3 *translationGradient, Mat3 *rotationGradient, Vec3 position, Vec3 normal, Vec3 val, float harmonicMean, uint32_t level){
    assert(level > 0);
    assert(level <= numCaches);

    if (numCaches == 0){ //cache disabled
        printf("Warning - attempting to insert record to irradiance cache when caching is disabled\n");
        return;
    }

    Cache *cache = &caches[level - 1];
    ++cache->numRecords;
    CacheRecord record = {
            .translationGradient = cache->useGradients ? *translationGradient : Zero3,
            .rotationGradient = cache->useGradients ? *rotationGradient : Zero3,
            .position = position,
            .normal = normal,
            .irradiance = val,
            .minDistance = harmonicMean
    };

    float distance = MAX(Distance3(record.position, cameraOrigin), 1);
    record.minBound = minSpacing * distance;
    record.maxBound = maxSpacing * distance;

    clamp_min_distance(&record);

    CacheRecord *recordPointer = store_record(cache, &record);

    if (cache->useGradients) gradient_limit_record(recordPointer);
    neighbour_clamping(cache, recordPointer);

    float validityRadius = cache->errorTolerance * recordPointer->minDistanceClamped;
    float sphereVolume = 4.f / 3.f * PI * (validityRadius * validityRadius * validityRadius);
    assert(!isinf(sphereVolume));
    assert(sphereVolume != 0);
    assert(sphereVolume > 0);

    insert_into_tree(&cache->cacheRoot, recordPointer, sphereVolume, validityRadius);
}


static void gradient_limit_record(CacheRecord *record){

    omp_set_lock(&record->translationLock);
    Mat3 translatedIrradiance = {
        .r0 = Vec3xScalar(record->translationGradient.r0, record->irradiance.x),
        .r1 = Vec3xScalar(record->translationGradient.r1, record->irradiance.y),
        .r2 = Vec3xScalar(record->translationGradient.r2, record->irradiance.z)
    };

    float magnitudes[3] = {Magnitude3(translatedIrradiance.r0), Magnitude3(translatedIrradiance.r1), Magnitude3(translatedIrradiance.r2)};
    float m0 = magnitudes[0] != 0 ? record->irradiance.x / magnitudes[0] : 1000.f;
    float m1 = magnitudes[1] != 0 ? record->irradiance.y / magnitudes[1] : 1000.f;
    float m2 = magnitudes[2] != 0 ? record->irradiance.z / magnitudes[2] : 1000.f;
    float min = MIN(m0, MIN(m1, m2));

    record->minDistance = MIN(record->minDistance, min);
    clamp_min_distance(record);

    float mult = MIN(1.f, record->minDistance / record->minBound);
    Mat3xScalar(&record->translationGradient, mult, &record->translationGradient);
    omp_unset_lock(&record->translationLock);

}


static void clamp_min_distance(CacheRecord *record){
    float max = MAX(record->minDistance, record->minBound);
    record->minDistanceClamped = MIN(max, record->maxBound);
}



/*
 * recursively traverses the tree, inserting the record into the valid nodes in the valid layer
 * a node is valid if the record's validity sphere overlaps/intersects with the node's bounds
 * the valid layer is the 1st layer such that the validity sphere's volume is greater than the volume of the nodes on that layer
 */



static void insert_into_tree(CacheNode *node, CacheRecord *record, float sphereVolume, float radius){

    assert(isnormal(sphereVolume));
    
    size_t stackSize = 1024 * 128;
    CacheNode *nodesArr[stackSize]; //possible issue in the future... should consider replacing with dynamic buffer for each thread like in intersection_test.c
    size_t current = 0;
    size_t size = 1;
    nodesArr[0] = node;

    while (current < size){
        
        CacheNode *currentNode = nodesArr[current++];
        Vec3 nodeSize = Vec3MinusVec3(currentNode->bounds[1], currentNode->bounds[0]);
        float nodeVolume = nodeSize.x * nodeSize.y * nodeSize.z;

        if (sphereVolume > nodeVolume){
            insert_into_node(currentNode, record);
            continue;
        }

        for (uint32_t i = 0; i < 8; i++){

            if (currentNode->children[i] == NULL){
                omp_set_lock(&currentNode->childCreationLock);

                if (currentNode->children[i] == NULL){
                    create_node(currentNode, i);                
                }

                omp_unset_lock(&currentNode->childCreationLock);
                assert(currentNode->children[i]);
            }

            Vec3 b1 = currentNode->children[i]->bounds[0];
            Vec3 b2 = currentNode->children[i]->bounds[1];

            if (sphere_box_intersection_test(b1, b2, record->position, radius)) {
                assert(size < stackSize);
                nodesArr[size++] = currentNode->children[i];
            }
        }
    }
}

/*
 * inserts the given record into the given node
 */

static void insert_into_node(CacheNode *node, CacheRecord *record){
    omp_set_lock(&node->insertionLock);
    
    if (node->recordCapacity == 0){
        node->records = Arena_Allocate(CacheRecord*, sizeof *node->records, arena);
        assert(node->records);
        node->recordCapacity = 1;
    }

    else if (node->numRecords == node->recordCapacity){
        size_t oldSize = (sizeof *node->records) * node->numRecords;
        node->records = Arena_Realloc(CacheRecord*, oldSize , node->records, arena, oldSize * 2);
        assert(node->records);
        node->recordCapacity *= 2;
    }

    node->records[node->numRecords] = record;
    ++node->numRecords; //only increment num records *after* the record has been added - possible parallel issues otherwise

    omp_unset_lock(&node->insertionLock);
}

/*
 * creates a node and inserts it into the tree
 */

static void create_node(CacheNode *parent, uint32_t index){
    CacheNode *newNode = Arena_Allocate(CacheNode, sizeof *newNode, arena);
    newNode->records = 0;
    newNode->numRecords = 0;
    newNode->recordCapacity = 0;

    Vec3 parentCentre = Vec3xScalar(Vec3PlusVec3(parent->bounds[0], parent->bounds[1]), 0.5);

    //we calculate the node's centre using the bits of its index in range [0, 7]
    //bit 2 represents the x axis, if set the centre is to the right of its parent
    //bit 1 represents the y axis, if set the centre is above its parent
    //bit 0 represents the z axis, if set the centre is in front of its parent

    Vec3 newNodeCentre = {
            .x = (parentCentre.x + parent->bounds[(index & 4) >> 2].x) * 0.5f,
            .y = (parentCentre.y + parent->bounds[(index & 2) >> 1].y) * 0.5f,
            .z = (parentCentre.z + parent->bounds[(index & 1) >> 0].z) * 0.5f
    };

    Vec3 newNodeOffsets = Vec3xScalar(Vec3MinusVec3(parent->bounds[1], parent->bounds[0]), 0.25);

    newNode->bounds[0] = Vec3MinusVec3(newNodeCentre, newNodeOffsets);
    newNode->bounds[1] = Vec3PlusVec3(newNodeCentre, newNodeOffsets);
    omp_init_lock(&newNode->insertionLock);
    omp_init_lock(&newNode->childCreationLock);

    for (size_t i = 0; i < 8; i++) newNode->children[i] = NULL;

    parent->children[index] = newNode;
}
 

/*
 * implementation of Jim Arvo's algorithm for sphere / AABB intersection test
 * returns 1 in the case of intersection else returns 0
 */

static uint32_t sphere_box_intersection_test(Vec3 b1, Vec3 b2, Vec3 centre, float radius){

    float distanceSquared = 0;

    for (int i = 0; i < 3; i++) {

        if (centre.data[i] < b1.data[i]) {
            distanceSquared += (centre.data[i] - b1.data[i]) * (centre.data[i] - b1.data[i]);
        }

        else if (centre.data[i] > b2.data[i]) {
            distanceSquared += (centre.data[i] - b2.data[i]) * (centre.data[i] - b2.data[i]);
        }
    }

    return distanceSquared <= radius * radius;
}



static void neighbour_clamping(Cache *cache, CacheRecord *record){

    //first we check all the neighbours of the new record, and clamp its min distance using the triangle inequality
    CacheNode *node = &cache->cacheRoot;
    while (node != NULL) {
        for (size_t i = 0; i < node->numRecords; i++) {

            if (!step_test(node->records[i], record->position, record->normal)) continue;

            float recordWeighting = calculate_weighting_alternative(cache, node->records[i], record->position, record->normal);
            if (recordWeighting <= 0) continue;

            float distance = Distance3(record->position, node->records[i]->position);
            record->minDistance = MIN(record->minDistance, node->records[i]->minDistance + distance);
            clamp_min_distance(record);

        }

        uint32_t childIndex = get_child_node(node, record->position);
        node = node->children[childIndex];
    }


    //once the record has been properly clamped, we traverse the tree again to clamp all of its neighbours with the new record's value
    node = &cache->cacheRoot;
    while (node != NULL) {
        for (size_t i = 0; i < node->numRecords; i++) {

            if (!step_test(node->records[i], record->position, record->normal)) continue;

            float recordWeighting = calculate_weighting_alternative(cache, node->records[i], record->position, record->normal);
            if (recordWeighting <= 0) continue;

            float distance = Distance3(record->position, node->records[i]->position);
            node->records[i]->minDistance = MIN(node->records[i]->minDistance, record->minDistance + distance);
            clamp_min_distance(node->records[i]);

        }

        uint32_t childIndex = get_child_node(node, record->position);
        node = node->children[childIndex];
    }
}



/*
 * prints some statistics about each cache layer
 */

void print_cache_statistics(){
    if (numCaches == 0){
         printf("Irradiance cache disabled\n");
         return;
    }
    printf("===== Irradiance Cache Statistics =====\n");

    for (size_t i = 0; i < numCaches; i++){
            printf("Number of records in cache %lu = %lu\n", i, caches[i].numRecords);
            printf("Gradients enabled: %s\n", caches[i].useGradients ? "yes" : "no");
            float ratio = (float) caches[i].numMisses / (float) (caches[i].numMisses + caches[i].numHits);
            printf("%f%% cache miss rate (%lu out of %lu)\n\n", ratio, caches[i].numMisses, caches[i].numMisses + caches[i].numHits);
    }
}


