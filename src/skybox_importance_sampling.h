#include "linear_algebra.h"

typedef struct SphericalSamplerNode{

    Vec2 b1; //8 bytes
    Vec2 b2; //8 bytes
    float totalRadiance; //4 bytes
    struct SphericalSamplerNode *children[2];


} SphericalSamplerNode;