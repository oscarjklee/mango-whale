/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */


#ifndef MANGO_WHALE_SAMPLE_H
#define MANGO_WHALE_SAMPLE_H

#include "linear_algebra.h"
#include "memory.h"
#include <stdint.h>


void init_sampler(MemoryArena *arena, uint32_t numThreads);
Vec3 sample_hemisphere(Vec3 normal);
Vec3 stratified_sample(float *thetaRet, Vec3 normal, float theta, float azimuth, float maxTheta, float maxAzimuth);
Vec3 spherical_to_cartesian(Vec2 spherical);
Vec2 cartesian_to_spherical(Vec3 cartesian);

float uniform_sample();



#endif //MANGO_WHALE_SAMPLE_H
