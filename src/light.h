//
// Created by oscar on 8/26/23.
//

#ifndef MANGO_WHALE_LIGHT_H
#define MANGO_WHALE_LIGHT_H
#include "linear_algebra.h"

typedef struct Light{
    Vec3 pos;
    Vec3 colour;
    Vec3 direction;
    float strength;
} Light;

#endif //MANGO_WHALE_LIGHT_H
