/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */

/*
 This file contains a basic memory arena. The user can create an arena with create_arena and delete an arena with
 delete_arena. The user can receive a buffer from the arena with the macro Arena_Allocate.
 If a given arena runs out of space, a child arena will be created. This child arena *has the exact same lifetimes as
 its parent arena* - it is just there to allow any allocation to always succeed.

 Under absolutely no circumstances should the user interact with the arena in any way other than the functions
 create_arena, delete_arena and the macro Arena_Allocate.

 It is likely that as the program grows this memory management scheme will be expanded.

 The user should not make an arena and store it on an existing arena, instead a separate arena should be created.
 */

#ifndef MANGO_WHALE_MEMORY_H
#define MANGO_WHALE_MEMORY_H
#include <stdint.h>

#include <stdlib.h>
#include <stdio.h>
#include <stdalign.h>
#include <string.h>
#include <omp.h>

typedef struct MemoryArena{
    void *data; //pointer to start of arena data
    struct MemoryArena *childArena;
    uint64_t capacity; //capacity in bytes
    uint64_t size; //amount of arena memory currently being used in bytes
    omp_lock_t lock;
} MemoryArena;

MemoryArena* create_arena(uint64_t size);
void delete_arena(MemoryArena* arena);
void* allocate_arena_memory(size_t alignment, MemoryArena* arena, uint64_t size);
void* arena_front_end_alloc(uint64_t size);
void set_current_arena_front_end(MemoryArena* arena);
void* arena_realloc(size_t alignment, MemoryArena* arena, uint64_t new_size, uint64_t old_size, void* old_ptr);

#define Arena_Allocate(type, size, arena) allocate_arena_memory(alignof(type), arena, size)
#define Arena_Realloc(type, old_size, old_ptr, arena, new_size) arena_realloc(alignof(type), arena, new_size, old_size, old_ptr)

#endif //MANGO_WHALE_MEMORY_H
