/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MANGO_WHALE_RAY_H
#define MANGO_WHALE_RAY_H
#include "common_definitions.h"

typedef struct ray{
    Vec3 origin;
    Vec3 direction;
    Vec3 inverse; //component-wise inverse of direction, ie .x = 1/direction.x, ...
} Ray;

#endif //MANGO_WHALE_RAY_H
