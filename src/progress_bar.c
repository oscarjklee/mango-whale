/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */


#include "progress_bar.h"
#include "stdio.h"

void print_progress_bar(ProgressBar *progressBar){
    printf("\r");
    fflush(stdout);

    printf("%s -- %u%% [", progressBar->message, progressBar->progress);
    for (uint8_t i = 0; i < progressBar->progress; i++){
        printf("#");
    }
    for (uint8_t i = 0; i < 100 - progressBar->progress; i++){
        printf("-");
    }
    printf("]");
    fflush(stdout);

}

void update_progress_bar(ProgressBar *progressBar){
    if ( (++progressBar->current * 100) / progressBar->end > progressBar->progress){
        progressBar->progress++;
        print_progress_bar(progressBar);
    }
}

void finish_progress_bar(ProgressBar *progressBar){
    progressBar->progress = 100;
    progressBar->current = progressBar->end;
    print_progress_bar(progressBar);
}

