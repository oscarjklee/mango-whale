/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */

#include "common_definitions.h"
#include "assert.h"
#include <omp.h>

#define Max(a, b) (a > b ? a : b)

static MemoryArena* current_arena_front_end;


void *allocate_arena_memory(size_t alignment, MemoryArena *arena, uint64_t size){

    omp_set_lock(&arena->lock);
    uint64_t offset = ((alignment - 1) & (~arena->size)) + 1;
    uint64_t index = arena->size + offset; //ensure alignment

    assert(index % alignment == 0);

    if (index + size > arena->capacity){ //no room, store on child arena
        if (arena->childArena == NULL){
            arena->childArena = create_arena(Max(arena->capacity, size + alignment)); //make sure new arena is big enough
        }

        void *ret = allocate_arena_memory(alignment, arena->childArena, size);
        assert(ret);
        omp_unset_lock(&arena->lock);
        return ret;

    }

    arena->size += size + offset;
    void *ret = index + (char*) arena->data;
    assert(ret);
    omp_unset_lock(&arena->lock);
    return ret;
}

/*
Deletes the arena and any children, freeing all resources
 */

void delete_arena(MemoryArena* arena){
    if (arena == NULL) return;
    MemoryArena* child = arena->childArena;
    while (child != NULL){ //delete all child arenas
        MemoryArena *temp = child->childArena;
        free(child->data);
        omp_destroy_lock(&child->lock);
        free(child);
        child = temp;
    }
    free(arena->data);
    omp_destroy_lock(&arena->lock);

    free(arena);
}

MemoryArena *create_arena(uint64_t size){

    MemoryArena *arena = malloc(sizeof *arena);
    assert(arena);

    arena->data = malloc(size);
    assert(arena->data);

    arena->size = 0;
    arena->capacity = size;
    arena->childArena = NULL;
    omp_init_lock(&arena->lock);

    return arena;
}

void* arena_front_end_alloc(uint64_t size){
    return Arena_Allocate(max_align_t, size, current_arena_front_end); //purely generic, so use max_align_t
}

void set_current_arena_front_end(MemoryArena* arena){
    current_arena_front_end = arena;
}


void *arena_realloc(size_t alignment, MemoryArena* arena, uint64_t new_size, uint64_t old_size, void* old_ptr){
    void* new_ptr = allocate_arena_memory(alignment, arena, new_size);
    memcpy(new_ptr, old_ptr, old_size);
    return new_ptr;
}

