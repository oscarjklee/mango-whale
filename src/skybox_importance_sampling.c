#include <cassert>
#include <omp.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>

#include "skybox_importance_sampling.h"
#include "linear_algebra.h"
#include "memory.h"
#include "sample.h"
#include "obj_loader.h"


MemoryArena *arena;

SphericalSamplerNode **nodeArrays;
size_t treeDepth;

static Vec2 skybox_to_spherical(uint32_t u, uint32_t v, uint32_t width, uint32_t height, uint32_t face){

    Vec3 cartesian = {.x = 0, .y = 0, .z = 1};
    float relU = 2.f * (((float) u / (float) width) - (float) width / 2.f);
    float relV = 2.f * (((float) v / (float) height) - (float) height / 2.f);
    cartesian.x = relU;
    cartesian.y = relV;

    if (face == 4) return cartesian_to_spherical(cartesian);

    //we now have the right cartesian value for the +z face, so just rotate it to align with the real face
    Vec3 dir;

    if (face != 4){
        switch (face){
            case 0: //+x
                dir = (Vec3) {.x = 1, .y = 0, .z = 0};
                break;
            case 1: //-x
                dir = (Vec3) {.x = -1, .y = 0, .z = 0};
                break;
            case 2: //+y
                dir = (Vec3) {.x = 0, .y = 1, .z = 0};
                break;
            case 3: //-y
                dir = (Vec3) {.x = 0, .y = -1, .z = 0};
                break;
            case 5: //-z
                dir = (Vec3) {.x = 0, .y = 0, .z = -1};
                break;
            default:
                fprintf(stderr, "Error - expected face to be in range 0 <= face <= 5\nInstead face = %u", face);
                exit(EXIT_FAILURE);
                break;
        }
    }
    Mat4 rotationMatrix = get_rotation_matrix((Vec3){.x = 0, .y = 0, .z = 1}, dir, 0.000005f);
    dir = transform_point(&rotationMatrix, dir);

    return cartesian_to_spherical(dir);
}



SphericalSamplerNode merge_nodes(SphericalSamplerNode *n1, SphericalSamplerNode *n2){

    SphericalSamplerNode newNode;
    newNode.b1.x = MIN(n1->b1.x, n2->b1.x);
    newNode.b1.y = MIN(n1->b1.y, n2->b1.y);

    newNode.b2.x = MAX(n1->b2.x, n2->b2.x);
    newNode.b2.y = MAX(n1->b2.y, n2->b2.y);

    newNode.totalRadiance = n1->totalRadiance + n2->totalRadiance;
    newNode.children[0] = n1;
    newNode.children[1] = n2;

    return newNode;

}

void init_tree_allocations(size_t baseSize){
    treeDepth = 0;
    while (baseSize){
        if (baseSize % 2 == 1) ++baseSize;
        baseSize /= 2;
        ++treeDepth;
    }

    size_t allocSize = 1;
    for (size_t i = 0; i < treeDepth; i++){
        nodeArrays[i] = Arena_Allocate(SphericalSamplerNode, sizeof *nodeArrays[i] * allocSize, arena);
        allocSize *= 2;
    }
}


void build_structure(SkyBox *skyBox, MemoryArena *memoryArena){

    arena = memoryArena;
    const uint32_t width = skyBox->textureWidth;
    const uint32_t height = skyBox->textureHeight;

    size_t levelSize = width * height * 6;
    init_tree_allocations(levelSize);
    size_t count = 0;

    for (int face = 0; face < 6; face++){
        for (uint32_t v = 0; v < height; v++){
            for (uint32_t u = 0; u < width; u++){

                Vec2 sphericalDir = skybox_to_spherical(u, v, width, height, face);
                SphericalSamplerNode node;
                node.b1 = sphericalDir;
                node.b2 = sphericalDir;
                size_t index = 3 * ((v * width) + u);
                node.totalRadiance = (float)(skyBox->texture[face][index] + skyBox->texture[face][index + 1] + skyBox->texture[face][index + 2]);
                nodeArrays[treeDepth - 1][count++] = node;
            }
        }
    }
    assert(count == levelSize - 1);

    for (size_t level = 1; level < treeDepth; level++){
        size_t currentLevelSize = levelSize / 2;
        for (size_t i = 0; i < currentLevelSize; i++){
            SphericalSamplerNode *n1 = &nodeArrays[treeDepth - level - 2][2 * i];
            if ((2 * i) + 1 >= levelSize){
                nodeArrays[treeDepth - level - 1][i] = *n1;
                continue;
            }

            SphericalSamplerNode *n2 = &nodeArrays[treeDepth - level - 2][(2 * i) + 1];
            nodeArrays[treeDepth - level - 1][i] = merge_nodes(n1, n2);
        }
    }
}