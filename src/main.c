/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */

#include <fenv.h>
#include <float.h>
#include <assert.h>
#include <math.h>
#include <omp.h>
#include <stdatomic.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include <sys/types.h>
#include <execinfo.h>
#include <signal.h>
#include <stdlib.h>
#include <unistd.h>

#include "common_definitions.h"
#include "camera.h"
#include "intersection_test.h"
#include "light.h"
#include "linear_algebra.h"
#include "memory.h"
#include "ray.h"
#include "shader.h"
#include "sample.h"
#include "progress_bar.h"
#include "ppm_image.h"
#include "obj_loader.h"
#include "bounding_volume_hierarchy.h"
#include "config_reader.h"
#include "cache.h"
#include "render.h"

#define EPSILON 0.000005f
#define SAMPLE_VARIANCE 0.05f
#define MAX_THETAS 2048

typedef struct InnerTranslationGradientData{
    Vec3 *term1Sum;
    Vec3 *term2Sum;

    Vec3 *prevRadiance;
    float *prevDistance;

    Vec3 rayVal;

    float maxTheta;
    float thetaRet;
    float distance;

    float azimuth;
    float theta; 

} InnerTranslationGradientData;


int feenableexcept(int excepts);
static void init_triangles(MemoryArena *arena, Vec3 *vertices, Vec3 *vertexNormals, Mat4 *trans, Mat4 *rot);
static Vec3 path_trace(uint32_t *threadBuffer, float *distance, Vec3 *indirect, Vec3 *direct, uint32_t *cacheMiss, Vec3 filter, Ray ray, uint32_t depth);
static Mat4 get_translation_matrix(Mat4 *viewMatrix);
static uint8_t* ray_trace(MemoryArena *arena, uint8_t **indirectFramebuffrer, uint8_t **directFramebuffer, uint8_t **cacheRecordFramebuffer, uint32_t bvhSize);
static Vec3 cast_over_hemisphere(uint32_t *threadBuffer, uint32_t *cacheMiss, Vec3 filter, Vec3 point, Vec3 normal, uint32_t depth);
static Vec3 calculate_variance(Vec3 sum, Vec3 sumSquared, uint32_t n);
static Vec3 cast_transparent_ray(uint32_t *threadBuffer, Vec3 filter, Ray oldRay, Vec3 intersection, uint32_t depth);
static Vec3 cast_mirror_ray(uint32_t *threadBuffer, Vec3 filter, Ray oldRay, Vec3 normal, Vec3 intersection, uint32_t depth);
static Ray get_indirect_ray(Vec3 origin, Vec3 normal);
static Ray get_indirect_ray_stratified(float *thetaRet, Vec3 origin, Vec3 normal, float theta, float azimuth, float maxTheta, float maxAzimuth);
static void render_pass(uint32_t **threadBuffers, uint8_t *cacheRecordFramebuffer);
static Vec3 cast_stratified_sampling(uint32_t *threadBuffer, Vec3 filter, Vec3 point, Vec3 normal, uint32_t depth);
static Vec3 ambient_occlusion_cast(uint32_t *threadBuffer, uint32_t *cacheMiss, Ray ray);
static Vec3 ray_cast(uint32_t *threadBuffer, float *distance, Vec3 *indirect, Vec3 *direct, uint32_t *cacheMiss, Vec3 filter, Ray ray, uint32_t depth);



static ObjData objData;
static ConfigParameters configParameters;
static Triangle *triangles;
static Triangle *emissiveTriangles;
static Triangle *portalTriangles;

static uint32_t indirectOnly = 1;
static const float pi = 3.14159265f;
static uint32_t numTriangles;

int main(int argc, char **argv){

// #ifdef __gnu_linux__
//     feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW); //crash on NAN or division by 0
// #endif

    printf("Mango whale\n");
    printf("Use WASD and the cursor to move the camera\nPress 'R' to render the scene\n");

    if (argc != 2){
        create_config_file("mango_whale_config.txt");
        printf("Error - expected 1 arg - the name of the config file\n"
               "An empty config file has been created - \"mango_whale_config.txt\"\n");

        exit(EXIT_FAILURE);
    }

    if (parse_config_file(argv[1], &configParameters) == 1){
        printf("Error reading from config file \"%s\"\n", argv[1]);
        exit(EXIT_FAILURE);
    }


    MemoryArena *mainArena = create_arena(1024 * 1024 * 1024);
    MemoryArena *objArena = create_arena(1024 * 1024 * 1024); //initialise memory to store obj data

    if (load_obj_file(configParameters.directory, configParameters.objFile, objArena, &objData)) {
        printf("Error reading obj file\n");
        exit(EXIT_FAILURE); //exit if failed to load obj
    }
    printf("Obj file loaded\n");
    printf("Num emissive triangles = %lu\n", objData.numEmissives);


    init_gl(configParameters.screenWidth, configParameters.screenHeight);
    load_model(mainArena, &objData);

    Mat4 viewMatrix;
    while (!update_camera((float) configParameters.screenWidth, (float) configParameters.screenHeight, &viewMatrix)){
        render(configParameters.screenWidth, configParameters.screenHeight);
    }

    //load and initialise triangles from obj
    numTriangles = (uint32_t) (objData.numVertices / 3);
    Mat4 rotMatrix;
    get_rotation_from_view(&viewMatrix, &rotMatrix);
    Mat4 transMatrix = get_translation_matrix(&viewMatrix);
    init_triangles(mainArena, objData.vertices, objData.vertexNormals, &transMatrix, &rotMatrix);

    //create bvhs - one for all geometry, another for just the lights
    MemoryArena *bvhArena = create_arena(numTriangles * sizeof(BvhNodePublic) * 2);
    uint32_t bvhSize;
    BvhNodePublic *bvh = create_bvh(triangles, numTriangles, bvhArena, &bvhSize);
    printf("BVH created, %u nodes\n", bvhSize);


    MemoryArena *lightBvhArena = NULL;
    BvhNodePublic *lightBvh = NULL;
    if (objData.numEmissives > 0 || objData.numPortals > 0){
        lightBvhArena = create_arena(objData.numEmissives * sizeof(BvhNodePublic) * 2);
        uint32_t lightBvhSize;
        lightBvh = create_bvh(emissiveTriangles, (uint32_t) objData.numEmissives + objData.numPortals, lightBvhArena, &lightBvhSize);
        printf("Light BVH created, %u nodes\n", lightBvhSize);
    }


    //initialise intersection tests with bvh and triangles
    init_intersection(bvh, lightBvh, triangles, emissiveTriangles);
    //initialise shading system
    init_shader(&objData, triangles, mainArena, portalTriangles, objData.numPortals);

    MemoryArena *cacheArena = create_arena(1024 * 1024 * 1024);
    //init irradiance cache
    Vec3 cameraOrigin = {.x = -viewMatrix.r0.w, -viewMatrix.r1.w, -viewMatrix.r2.w};
    init_irradiance_cache(cacheArena, bvh->bounds[0], bvh->bounds[1], cameraOrigin, configParameters.tolerance, configParameters.gradients, configParameters.maxDepth, configParameters.minDistance, configParameters.maxDistance);

    //ray trace
    uint8_t *indirectFramebuffer, *directFramebuffer, *cacheRecordFramebuffer;
    uint8_t *frameBuffer = ray_trace(mainArena, &indirectFramebuffer, &directFramebuffer, &cacheRecordFramebuffer, bvhSize);

    //write framebuffer to file and cleanup resources
    write_image_to_ppm(configParameters.outputFile, frameBuffer, configParameters.screenWidth, configParameters.screenHeight);

    //print some statistics
    // printf("Total rays cast = %lu\n", get_num_casts());
    print_cache_statistics();
    
    render_result(frameBuffer, indirectFramebuffer, directFramebuffer, cacheRecordFramebuffer, configParameters.screenWidth, configParameters.screenHeight);

    delete_arena(mainArena);
    delete_arena(bvhArena);
    delete_arena(lightBvhArena);
    delete_arena(objArena);
    delete_arena(cacheArena);
    return 0;
}



/*
 * initialises threads for ray tracing, and calls the path_trace for each pixel
 */

static uint8_t *ray_trace(MemoryArena *arena, uint8_t **indirectFramebuffrer, uint8_t **directFramebuffer, uint8_t **cacheRecordFramebuffer, uint32_t bvhSize) {

    int maxThreads = omp_get_max_threads();

    //initialise buffers for each thread
    MemoryArena *threadArena = create_arena(sizeof(uint32_t) * bvhSize * (uint32_t) maxThreads);
    uint32_t **threadBuffers = Arena_Allocate(uint32_t*, (sizeof *threadBuffers) * (uint32_t) maxThreads, threadArena);
    for (int i = 0; i < maxThreads; i++) {
        threadBuffers[i] = Arena_Allocate(uint32_t, (sizeof *threadBuffers[i]) * bvhSize, threadArena);
    }

    init_sampler(arena, (uint32_t) maxThreads);

    //read config parameters, improves readability
    const uint32_t screenHeight = configParameters.screenHeight;
    const uint32_t screenWidth = configParameters.screenWidth;
    const uint32_t depth = configParameters.maxDepth;

    init_camera(screenWidth, screenHeight);

    uint8_t *frameBuffer = Arena_Allocate(uint8_t, (sizeof *frameBuffer) * screenWidth * screenHeight * 3, arena);
    *indirectFramebuffrer = Arena_Allocate(uint8_t, (sizeof **indirectFramebuffrer) * screenWidth * screenHeight * 3, arena);
    *directFramebuffer = Arena_Allocate(uint8_t, (sizeof **directFramebuffer) * screenWidth * screenHeight * 3, arena);
    *cacheRecordFramebuffer = Arena_Allocate(uint8_t, (sizeof **cacheRecordFramebuffer) * screenWidth * screenHeight * 3, arena);
    memset(*cacheRecordFramebuffer, 255, (sizeof **cacheRecordFramebuffer) * screenWidth * screenHeight * 3);

    if (configParameters.tolerance > 0){
        render_pass(threadBuffers, *cacheRecordFramebuffer);
        set_tolerance(configParameters.tolerance * 1.6f);
    }


    ProgressBar progressBar = {
            .message = configParameters.tolerance ? "Second Pass Through" : "Render Pass",
            .current = 0,
            .end = screenHeight,
            .progress = 0
    };

    indirectOnly = 0;
    print_progress_bar(&progressBar);

    for (uint32_t i = 0; i < screenHeight; i++) {

        update_progress_bar(&progressBar);

        #pragma omp parallel for schedule(dynamic)
        for (uint32_t j = 0; j < screenWidth; j++) {

            int threadID = omp_get_thread_num();
            Vec3 sum = {.x = 0, .y = 0, .z = 0};
            Vec3 sumSquared = {.x = 0, .y = 0, .z = 0};

            Vec3 indirectSum = {.x = 0, .y = 0, .z = 0};
            Vec3 directSum = {.x = 0, .y = 0, .z = 0};


            uint32_t num = configParameters.tolerance ? configParameters.lightSamplesLevel1  : configParameters.maxSamples;
            uint32_t k;
            uint32_t cacheMiss = 0;
            for (k = 0; k < num; k++) {

                uint32_t cacheTest = 0;
                Vec3 indirect = {.x = 0, .y = 0, .z = 0};
                Vec3 direct = {.x = 0, .y = 0, .z = 0};
                Vec3 rayVal = ray_cast(threadBuffers[threadID], NULL, &indirect, &direct, &cacheTest,(Vec3) {.x = 1, .y = 1, .z = 1}, get_next_ray(i, j), depth);
                
                if (cacheTest == 1) cacheMiss = 1;
                sum = Vec3PlusVec3(sum, rayVal);
                directSum = Vec3PlusVec3(directSum, direct);
                indirectSum = Vec3PlusVec3(indirectSum, indirect);

                if (!configParameters.tolerance){
                    sumSquared = Vec3PlusVec3(sumSquared, ComponentMult(rayVal, rayVal));
                    Vec3 variance = calculate_variance(sum, sumSquared, k);

                    if (variance.x <= SAMPLE_VARIANCE && variance.y <= SAMPLE_VARIANCE && variance.z <= SAMPLE_VARIANCE) {
                        break;
                    }
                }
            }
            sum = Vec3xScalar(sum, 1 / (float) k); //calculate average colour value
            sum = ClampDown(sum, (Vec3) {.x = 1, .y = 1, .z = 1});
            sum = ClampUp(sum, (Vec3) {.x = 0, .y = 0, .z = 0});

            indirectSum = Vec3xScalar(indirectSum, 1 / (float) k); //calculate average colour value
            indirectSum = ClampDown(indirectSum, (Vec3) {.x = 1, .y = 1, .z = 1});
            indirectSum = ClampUp(indirectSum, (Vec3) {.x = 0, .y = 0, .z = 0});

            directSum = Vec3xScalar(directSum, 1 / (float) k); //calculate average colour value
            directSum = ClampDown(directSum, (Vec3) {.x = 1, .y = 1, .z = 1});
            directSum = ClampUp(directSum, (Vec3) {.x = 0, .y = 0, .z = 0});

            //write pixel colour value to frame buffer
            frameBuffer[((screenWidth * (screenHeight - 1 - i)) + j) * 3 + 0] = (uint8_t) (sum.x * 255);
            frameBuffer[((screenWidth * (screenHeight - 1 - i)) + j) * 3 + 1] = (uint8_t) (sum.y * 255);
            frameBuffer[((screenWidth * (screenHeight - 1 - i)) + j) * 3 + 2] = (uint8_t) (sum.z * 255);

            (*indirectFramebuffrer)[((screenWidth * (screenHeight - 1 - i)) + j) * 3 + 0] = (uint8_t) (indirectSum.x * 255);
            (*indirectFramebuffrer)[((screenWidth * (screenHeight - 1 - i)) + j) * 3 + 1] = (uint8_t) (indirectSum.y * 255);
            (*indirectFramebuffrer)[((screenWidth * (screenHeight - 1 - i)) + j) * 3 + 2] = (uint8_t) (indirectSum.z * 255);

            (*directFramebuffer)[((screenWidth * (screenHeight - 1 - i)) + j) * 3 + 0] = (uint8_t) (directSum.x * 255);
            (*directFramebuffer)[((screenWidth * (screenHeight - 1 - i)) + j) * 3 + 1] = (uint8_t) (directSum.y * 255);
            (*directFramebuffer)[((screenWidth * (screenHeight - 1 - i)) + j) * 3 + 2] = (uint8_t) (directSum.z * 255);

            if (cacheMiss == 1){
                (*cacheRecordFramebuffer)[((configParameters.screenWidth * (configParameters.screenHeight - 1 - i)) + j) * 3 + 0] = 0;
                (*cacheRecordFramebuffer)[((configParameters.screenWidth * (configParameters.screenHeight - 1 - i)) + j) * 3 + 1] = 0;
                (*cacheRecordFramebuffer)[((configParameters.screenWidth * (configParameters.screenHeight - 1 - i)) + j) * 3 + 2] = 0;
            }
        }
    }

    for (uint32_t i = 0; i < configParameters.screenHeight; i++){
        for (uint32_t j = 0; j < configParameters.screenWidth; j++){
            for (uint32_t k = 0; k < 3; k++){
                uint8_t colour = frameBuffer[((screenWidth * (screenHeight - 1 - i)) + j) * 3 + k]; 
                (*cacheRecordFramebuffer)[((configParameters.screenWidth * (configParameters.screenHeight - 1 - i)) + j) * 3 + k] &= colour;
            }
        }
    }


    printf("\n"); //new line after progress bar

    delete_arena(threadArena);
    return frameBuffer;
}


/*
 * function performs a single render pass of the scene
 * doesn't store results to framebuffer, instead used to fill irradiance cache
 */

static void render_pass(uint32_t **threadBuffers, uint8_t *cacheRecordFramebuffer){

    uint32_t numSamples = configParameters.screenHeight * configParameters.screenWidth;

    ProgressBar progressBar = {
            .message = "First Pass Through",
            .current = 0,
            .end = 100,
            .progress = 0
    };

    print_progress_bar(&progressBar);

    for (uint32_t i = 0; i < 10; i++) {
        for (uint32_t j = 0; j < 10; j++) {

            #pragma omp parallel for schedule(dynamic)
            for (uint32_t k = 0; k < numSamples / 100; k++){
                uint32_t xRange = configParameters.screenWidth / 10;
                uint32_t yRange = configParameters.screenHeight / 10;

                uint32_t x = (uint32_t) ((float) (xRange - 1) * uniform_sample());
                uint32_t y = (uint32_t) ((float) (yRange - 1) * uniform_sample());
                x += (i * xRange);
                y += (j * yRange);
                assert(x < configParameters.screenWidth);
                assert(y < configParameters.screenHeight);

                int threadID = omp_get_thread_num();
                uint32_t cacheMiss = 0;
                ray_cast(threadBuffers[threadID], NULL, NULL, NULL, &cacheMiss, (Vec3) {.x = 1, .y = 1, .z = 1}, get_next_ray(y, x), configParameters.maxDepth);

                if (cacheMiss == 1){
                    cacheRecordFramebuffer[((configParameters.screenWidth * (configParameters.screenHeight - 1 - y)) + x) * 3 + 0] = 0;
                    cacheRecordFramebuffer[((configParameters.screenWidth * (configParameters.screenHeight - 1 - y)) + x) * 3 + 1] = 0;
                    cacheRecordFramebuffer[((configParameters.screenWidth * (configParameters.screenHeight - 1 - y)) + x) * 3 + 2] = 0;
                }
            }
            update_progress_bar(&progressBar);
            // print_cache_statistics();
        }
    }
    finish_progress_bar(&progressBar);
    printf("\n");
}






//calculates variance of samples
static Vec3 calculate_variance(Vec3 sum, Vec3 sumSquared, uint32_t n){

    if (n < configParameters.minSamples || n == 1) return (Vec3) {.x = FLT_MAX, .y = FLT_MAX, .z = FLT_MAX};

    Vec3 mean = Vec3xScalar(sum, 1 / (float) n);
    Vec3 meanSquared = ComponentMult(mean, mean);
    Vec3 sum2 = ComponentMult(sum, Vec3xScalar(mean, 2));

    Vec3 combined = Vec3PlusVec3(sumSquared, Vec3PlusVec3(sum2, meanSquared));
    combined = Vec3xScalar(combined, 1 / (float) (n - 1));

    return combined;

}


//ensures the surface normal is aligned with the ray
//if the ray comes from "behind" the triangle, then the normal may need flipping
static Vec3 calculateSurfaceNormal(Vec3 rayDir, Vec3 normal){
    if (DotProduct3(rayDir, normal) > -EPSILON){
        return Vec3xScalar(normal, -1);
    }
    return normal;
}


//casts a new ray through a transparent object
static Vec3 cast_transparent_ray(uint32_t *threadBuffer, Vec3 filter, Ray oldRay, Vec3 intersection, uint32_t depth){
    Vec3 newPoint = Vec3PlusVec3(intersection, Vec3xScalar(oldRay.direction, EPSILON)); //shift to avoid self intersection

    Ray newRay = {.direction = oldRay.direction, .origin = newPoint, .inverse = oldRay.inverse};

    return path_trace(threadBuffer, NULL, NULL, NULL, NULL, filter, newRay, depth - 1);

}


//casts a mirror reflection ray in the appropriate direction
static Vec3 cast_mirror_ray(uint32_t *threadBuffer, Vec3 filter,  Ray oldRay, Vec3 normal, Vec3 intersection, uint32_t depth) {
    Ray reflection; //create reflection ray
    reflection.origin = intersection;
    reflection.direction = Reflect(oldRay.direction, Normalise3(normal));
    reflection.inverse = InverseVector(reflection.direction);

    return path_trace(threadBuffer, NULL, NULL, NULL, NULL, filter, reflection, depth); //cast reflection ray
}


//returns 0 if the path is to be killed by russian roulette
//else returns the factor to multiply the result by in order to preserve energy
static float russian_roulette(Vec3 filter){
    float energyEstimate = (filter.x + filter.y + filter.z) * 0.5f;
    if (energyEstimate >= 1) return 1;

    float killChance = (1 - energyEstimate) * 0.99f;
    float randomNumber = uniform_sample();

    if (randomNumber <= killChance){
        return 0;
    }

    assert(killChance != 1);
    return 1 / (1 - killChance);
}


/*
 * casts a given ray by testing for any ray triangle intersections, then performs russian roulette and NEE
 * recursively calls itself to cast another ray and continue the path
 * returns a vec3 containing the rgb result of the given path segment
 */

static Vec3 path_trace(uint32_t *threadBuffer, float *distance, Vec3 *indirect, Vec3 *direct, uint32_t *cacheMiss, Vec3 filter, Ray ray, uint32_t depth){

    if (depth == 0) return (Vec3) {.x = 0, .y = 0, .z = 0}; //kill the path due to exceeding depth limit

    float roulette_energy = russian_roulette(filter);
    if (roulette_energy == 0) return (Vec3) {.x = 0, .y = 0, .z = 0}; //kill the path due to russian roulette

    Vec3 barycentrics, intersection;
    uint32_t index = intersection_test(threadBuffer, ray, &barycentrics, &intersection, FLT_MAX, EPSILON);

    if (index == (uint32_t) -1) {
        if (distance != NULL) *distance = FLT_MAX;
        return get_skybox_colour(ray.direction); //kill the path due to no intersection
    }

    if (distance != NULL) *distance = Distance3(intersection, ray.origin);

    //the ray can hit a triangle from either side, so we need to make sure the normal is oriented correctly
    Vec3 normTemp = calculateSurfaceNormal(ray.direction, Normalise3(triangles[index].normal));
    Vec3 surfaceNormal = Normalise3(normTemp);

    //get material index and material, improves readability
    size_t materialIndex = objData.materialIndices[index];
    MtlMaterial *material = &objData.materials[materialIndex];

    //calculate transparent component
    if (material->transparency > 0){
        float random = uniform_sample();
        if (random <= material->transparency) {
            Vec3 rayVal = cast_transparent_ray(threadBuffer, filter, ray, intersection, depth);
            rayVal = ComponentMult(rayVal, material->transmissionFilter);
            rayVal = Vec3xScalar(rayVal, roulette_energy);
            return rayVal;
        }
    }

    //update the transmission filter with the materials reflectivity
    Vec3 texColour = sample_texture(&objData.textureCoords[index * 3], barycentrics, materialIndex);
    filter = ComponentMult(filter, texColour);

    if (material->luminosity > 0 && (configParameters.tolerance == 0 || depth == configParameters.maxDepth)) {
        float dot = DotProduct3(triangles[index].normal, Vec3xScalar(ray.direction, -1));
        if (dot >= 0) {
            return Vec3xScalar(material->emissive, material->luminosity);
        }
    }

    //calculate mirror component
    if (material->reflectivity > 0){
        float random = uniform_sample();
        if (random <= material->reflectivity) {
            Vec3 rayVal = cast_mirror_ray(threadBuffer, filter, ray, surfaceNormal, intersection, depth - 1);
            rayVal = Vec3xScalar(rayVal, roulette_energy);
            return rayVal;
        }
    }

    Vec3 shiftedPoint = ShiftPoint(intersection, surfaceNormal); //shift the point along the normal

    //calculate indirect and direct lighting
    Vec3 indirectLighting = cast_over_hemisphere(threadBuffer, cacheMiss, filter, intersection, surfaceNormal, depth - 1);
    if (indirectOnly && depth == configParameters.maxDepth) return (Vec3) {.x = 0, .y = 0, .z = 0}; //just building the cache, no more work needed

    uint32_t numSamples;
    if (depth == configParameters.maxDepth) numSamples = 1;
    else if (depth == configParameters.maxDepth - 1 && configParameters.gradients) numSamples = configParameters.lightSamplesLevel2;
    else numSamples = configParameters.lightSamplesLevel2;

    Vec3 directLighting = next_event_estimation(&objData.vertexNormals[index * 3], surfaceNormal, threadBuffer, barycentrics, shiftedPoint, numSamples);
    Vec3 combinedLighting = Vec3PlusVec3(indirectLighting, directLighting);
    Vec3 shadingResult = ComponentMult(texColour, combinedLighting); //apply to material reflectivity

    assert(roulette_energy >= 1);
    shadingResult = Vec3xScalar(shadingResult, roulette_energy); //distribute energy from killed russian roulette rays

    if (direct != NULL) *direct = ComponentMult(texColour, directLighting);
    if (indirect != NULL) *indirect = ComponentMult(texColour, indirectLighting);

    return shadingResult;
}


/*
 * creates a ray in a random (cosine weighted) direction around the hemisphere
 */
static Ray get_indirect_ray(Vec3 origin, Vec3 normal){
    Ray newRay;
    newRay.direction = sample_hemisphere(normal);
    newRay.inverse = InverseVector(newRay.direction);
    newRay.origin = Vec3PlusVec3(origin, Vec3xScalar(newRay.direction, 0.0005f));
    return newRay;
}


/*
 * creates a ray in a stratified direction around the hemisphere
 */

static Ray get_indirect_ray_stratified(float *thetaRet, Vec3 origin, Vec3 normal, float theta, float azimuth, float maxTheta, float maxAzimuth){
    Ray newRay;
    newRay.direction = stratified_sample(thetaRet, normal, theta, azimuth, maxTheta, maxAzimuth);
    newRay.inverse = InverseVector(newRay.direction);
    newRay.origin = Vec3PlusVec3(origin, Vec3xScalar(newRay.direction, 0.0005f));

    return newRay;
}

/*
 * calculates the baseplane vector direction for use in rotation gradient calculation
 */
static Vec3 get_baseplane_vector(float azimuth, float maxAzimuth, float innerShift, float outerShift){
    Vec2 spherical = {.x = pi / 2.f, .y = (2.f * pi * ((azimuth + innerShift) / maxAzimuth)) + outerShift};
    Vec3 cartesian = spherical_to_cartesian(spherical);
    return cartesian;
}

/*
 * updates rotation gradient
 */
static void update_rotation_gradient(Mat3 *rotationGradient, Vec3 baseplaneVector, Vec3 change){
    rotationGradient->r0 = Vec3PlusVec3(rotationGradient->r0, Vec3xScalar(baseplaneVector, change.x));
    rotationGradient->r1 = Vec3PlusVec3(rotationGradient->r1, Vec3xScalar(baseplaneVector, change.y));
    rotationGradient->r2 = Vec3PlusVec3(rotationGradient->r2, Vec3xScalar(baseplaneVector, change.z));
}


/*
 * function casts many rays in random (cosine weighted) directions over a hemisphere
 * inserts result into irradiance cache 
 */
static Vec3 cast_random_sampling(uint32_t *threadBuffer, Vec3 filter, Vec3 point, Vec3 normal, uint32_t depth){

    float minDistance = FLT_MAX;
    uint32_t i;
    Vec3 sum = {.x = 0, .y = 0, .z = 0};
    Vec3 sumSquared = {.x = 0, .y = 0, .z = 0};
    uint32_t div = configParameters.maxDepth - depth;
    for (i = 0 ; i < configParameters.maxSamples >> div; i++) {

        float distance;
        Ray ray = get_indirect_ray(point, normal);
        Vec3 rayVal = ray_cast(threadBuffer, &distance, NULL, NULL, NULL, filter, ray, depth);

        if (DotProduct3(ray.direction, normal) > 0.3) minDistance = MIN(minDistance, distance);

        sum = Vec3PlusVec3(sum, rayVal);
        sumSquared = Vec3PlusVec3(sumSquared, ComponentMult(rayVal, rayVal));
        Vec3 variance = calculate_variance(sum, sumSquared, i);

        if (variance.x <= SAMPLE_VARIANCE && variance.y <= SAMPLE_VARIANCE && variance.z <= SAMPLE_VARIANCE) {
            break;
        }
    }
    if (i != configParameters.maxSamples) ++i;
    Vec3 finalVal = Vec3xScalar(sum, 1 / (float) i);

    insert_record(NULL, NULL, point, normal, finalVal, minDistance, configParameters.maxDepth - depth);
    return finalVal;
}

/*
 * updates the inner loop of translation gradient's value
 */
static void update_inner_translation_gradient(InnerTranslationGradientData *data){

    float prevTheta = acosf(safe_sqrt(1.f - data->theta / data->maxTheta));
    float thetaCurrent = acosf(safe_sqrt(1.f - (data->theta + 0.5f) / data->maxTheta));
    float nextTheta = acosf(safe_sqrt(1.f - (data->theta + 1.f) / data->maxTheta));

    Vec3 term1 = {.x = 0, .y = 0, .z = 0};
    Vec3 term2 = {.x = 0, .y = 0, .z = 0};

    if (data->theta != 0) {
        float cosPrev = cosf(prevTheta);
        float top = cosPrev * cosPrev * sinf(prevTheta);
        float bottom = MIN(data->distance, data->prevDistance[(size_t) data->theta - 1]);
        float mult = bottom == 0.f ? 0.f : top / bottom;
        Vec3 diff = Vec3MinusVec3(data->rayVal, data->prevRadiance[(size_t) data->theta - 1]);
        term1 = Vec3xScalar(diff, mult);
    }
    if (data->azimuth != 0) {
        float top = cosf(thetaCurrent) * (cosf(prevTheta) - cosf(nextTheta));
        float bottom = sinf(data->thetaRet) * MIN(data->distance, data->prevDistance[(size_t) data->theta]);
        float mult = (bottom == 0.f) ? 0.f : top / bottom;
        Vec3 diff = Vec3MinusVec3(data->rayVal, data->prevRadiance[(size_t) data->theta]);
        term2 = Vec3xScalar(diff, mult);
    }

    *data->term1Sum = Vec3PlusVec3(*data->term1Sum, term1);
    *data->term2Sum = Vec3PlusVec3(*data->term2Sum, term2);

}

/*
 * updates the outer loop of translation gradient's value
 */

static void update_outer_translation_gradient(Mat3 *translationGradient, Vec3 term1Sum, Vec3 term2Sum, float azimuth, float maxAzimuth){
    Vec3 baseplaneVectorU = get_baseplane_vector(azimuth, maxAzimuth, 0.5f, 0.f);
    Vec3 baseplaneVectorV2 = get_baseplane_vector(azimuth, maxAzimuth, 0.f, pi / 2.f);

    term1Sum = Vec3xScalar(term1Sum, 2 * pi / (float) maxAzimuth);
    translationGradient->r0 = Vec3PlusVec3(translationGradient->r0, Vec3xScalar(baseplaneVectorU, term1Sum.x));
    translationGradient->r1 = Vec3PlusVec3(translationGradient->r1, Vec3xScalar(baseplaneVectorU, term1Sum.y));
    translationGradient->r2 = Vec3PlusVec3(translationGradient->r2, Vec3xScalar(baseplaneVectorU, term1Sum.z));

    translationGradient->r0 = Vec3PlusVec3(translationGradient->r0, Vec3xScalar(baseplaneVectorV2, term2Sum.x));
    translationGradient->r1 = Vec3PlusVec3(translationGradient->r1, Vec3xScalar(baseplaneVectorV2, term2Sum.y));
    translationGradient->r2 = Vec3PlusVec3(translationGradient->r2, Vec3xScalar(baseplaneVectorV2, term2Sum.z));

}

/*
 * alligns the translation and rotation gradients with the surface normal of the point
 */

static void transform_gradients(Mat3 *translationGradient, Mat3 *rotationGradient, Vec3 normal){
    Mat4 rotationMatrix = get_rotation_matrix((Vec3) {.x = 0, .y = 1, .z = 0}, normal, EPSILON);
    translationGradient->r0 = transform_point(&rotationMatrix, translationGradient->r0);
    translationGradient->r1 = transform_point(&rotationMatrix, translationGradient->r1);
    translationGradient->r2 = transform_point(&rotationMatrix, translationGradient->r2);

    rotationGradient->r0 = transform_point(&rotationMatrix, rotationGradient->r0);
    rotationGradient->r1 = transform_point(&rotationMatrix, rotationGradient->r1);
    rotationGradient->r2 = transform_point(&rotationMatrix, rotationGradient->r2);
}

/*
 * casts many rays over a hemisphere according to a stratified sampling scheme - the stratification is necessary to create gradients
 */

static Vec3 cast_stratified_sampling(uint32_t *threadBuffer, Vec3 filter, Vec3 point, Vec3 normal, uint32_t depth){

    float minDistance = FLT_MAX;
    float div = safe_sqrt((float) (1 << (configParameters.maxDepth - depth - 1)));

    uint32_t maxTheta = (uint32_t) (safe_sqrt((float) configParameters.maxSamples / pi) / div);
    uint32_t maxAzimuth = (uint32_t) (((float)maxTheta * pi) / div);
    uint32_t numSamples = maxTheta * maxAzimuth;


    Mat3 rotationGradient = Zero3;
    Mat3 translationGradient = Zero3;

    Vec3 prevRadiance[MAX_THETAS];
    float prevDistance[MAX_THETAS];

    assert(maxTheta <= MAX_THETAS);
    Vec3 sum = {.x = 0, .y = 0, .z = 0};

    for (uint32_t azimuth = 0; azimuth < maxAzimuth; azimuth++){
        Vec3 localRotationGradient = {.x = 0, .y = 0, .z = 0};

        Vec3 term1Sum = {.x = 0, .y = 0, .z = 0};
        Vec3 term2Sum = {.x = 0, .y = 0, .z = 0};

        for (uint32_t theta = 0; theta < maxTheta; theta++){
            float distance, thetaRet;
            Ray ray = get_indirect_ray_stratified(&thetaRet, point, normal, (float) theta, (float) azimuth, (float) maxTheta, (float) maxAzimuth);
            Vec3 rayVal = ray_cast(threadBuffer, &distance, NULL, NULL, NULL, filter, ray, depth);
            
            //dont consider rays very close to the tangent plane -- they unrepresantitively be too short
            if (theta < (9 * 10) / maxTheta) minDistance = MIN(minDistance, distance);
            sum = Vec3PlusVec3(sum, rayVal);

            Vec3 rotationGradientChange = Vec3xScalar(rayVal, tanf((float) theta / (float) maxTheta * pi / 2.f));
            localRotationGradient = Vec3PlusVec3(localRotationGradient, rotationGradientChange);

            InnerTranslationGradientData data = {
                .term1Sum = &term1Sum,
                .term2Sum = &term2Sum,

                .prevRadiance = &prevRadiance[0],
                .prevDistance = &prevDistance[0],

                .rayVal = rayVal,
                .maxTheta = (float) maxTheta,
                .thetaRet = (float) thetaRet,
                .distance = distance,
                .azimuth = (float) azimuth,
                .theta = (float) theta
            };
            update_inner_translation_gradient(&data);
            prevRadiance[theta] = rayVal;
            prevDistance[theta] = distance;
        }

        Vec3 baseplaneVectorV = get_baseplane_vector((float) azimuth, (float) maxAzimuth, 0.5f, pi / 2.f);
        update_rotation_gradient(&rotationGradient, baseplaneVectorV, localRotationGradient);
        update_outer_translation_gradient(&translationGradient, term1Sum, term2Sum, (float) azimuth, (float) maxAzimuth);
    }

    transform_gradients(&translationGradient, &rotationGradient, normal);
    Vec3 finalVal = Vec3xScalar(sum, 1 / (float) numSamples);
    insert_record(&translationGradient, &rotationGradient, point, normal, finalVal, minDistance, configParameters.maxDepth - depth);

    return finalVal;
}


/*
 * function estimates indirect lighting by either sampling the hemisphere through some scheme or interpolating from the irradiance cache
 */
static Vec3 cast_over_hemisphere(uint32_t *threadBuffer, uint32_t *cacheMiss, Vec3 filter, Vec3 point, Vec3 normal, uint32_t depth){

    if (depth == 0) return (Vec3) {.x = 0, .y = 0, .z = 0};

    // normal = Normalise3(normal);

    //irradaince caching turned off -- regular path tracing
    if (configParameters.tolerance == 0) {
        Ray ray = get_indirect_ray(point, normal);
        Vec3 val = ray_cast(threadBuffer, NULL, NULL, NULL, NULL, filter, ray, depth);

        float cosTheta = DotProduct3(normal, ray.direction);
        float lambertianMult = cosTheta / pi;
        float directLightMult = get_light_sample_probability(threadBuffer, ray, -1);

        float balanceHeuristicMult = 1.f / (lambertianMult + directLightMult);
        return Vec3xScalar(val, balanceHeuristicMult * cosTheta);
    }

    //else search the irradiance cache
    Vec3 cacheResult = search_cache(point, normal, configParameters.maxDepth - depth);

    if (cacheResult.x != -1){ //cache hit
        if (cacheMiss) *cacheMiss = 0;
        cacheResult = ClampUp(cacheResult, (Vec3) {.x = 0, .y = 0, .z = 0});
        return configParameters.ambientOcclusionRender ? cacheResult : Vec3xScalar(cacheResult, 1.f / pi);
    }

    if (cacheMiss) *cacheMiss = 1; //cache miss

    if ((depth == 1 && !configParameters.ambientOcclusionRender) || !configParameters.gradients){ //final cache level -- no gradients
        return cast_random_sampling(threadBuffer, filter, point, normal, depth);
    }

    return cast_stratified_sampling(threadBuffer, filter, point, normal, depth); //not final cache level and gradients enabled

}


static Vec3 ray_cast(uint32_t *threadBuffer, float *distance, Vec3 *indirect, Vec3 *direct, uint32_t *cacheMiss, Vec3 filter, Ray ray, uint32_t depth){

    if (configParameters.ambientOcclusionRender == 0){ //path trace
        return path_trace(threadBuffer, distance, indirect, direct, cacheMiss, filter, ray, depth);
    }

    //ambient occlusion render
    Vec3 point, barycentrics;
    uint32_t index = intersection_test(threadBuffer, ray, &barycentrics, &point, FLT_MAX, EPSILON);

    const float maxDistance = 10.f;
    if (index == (uint32_t) -1){ //no hit

        if (depth == configParameters.maxDepth){ //no hit on primary ray
            return (Vec3) {.x = 0, .y = 0, .z = 0};
        }

        if (distance) *distance = maxDistance;
        return (Vec3) {.x = 1, .y = 1, .z = 1}; //no hit on secondary ray
    }


    if (depth == configParameters.maxDepth){ //hit on secondary ray attempt to search cache
        return ambient_occlusion_cast(threadBuffer, cacheMiss, ray);
    }

    //hit on secondary ray -- calculate ao factor
    
    float intersectionDistance = Distance3(point, ray.origin);
    if (distance) *distance = MIN(maxDistance, intersectionDistance);

    if (intersectionDistance > maxDistance) return (Vec3) {.x = 1, .y = 1, .z = 1}; //hit too far away - treat as no hit

    float ratio = intersectionDistance / maxDistance;
    return (Vec3) {.x = ratio, .y = ratio, .z = ratio}; //hit within range -- return 0 vector

    
}



static Vec3 ambient_occlusion_cast(uint32_t *threadBuffer, uint32_t *cacheMiss, Ray ray){

    Vec3 barycentrics, intersection;
    uint32_t index = intersection_test(threadBuffer, ray, &barycentrics, &intersection, FLT_MAX, EPSILON);

    if (index == (uint32_t) -1) {
        return (Vec3) {.x = 0, .y = 0, .z = 0};
    }

    Vec3 surfaceNormal = Normalise3(calculateSurfaceNormal(ray.direction, Normalise3(triangles[index].normal)));
    Vec3 shiftedPoint = ShiftPoint(intersection, surfaceNormal); //shift the point along the normal

    return cast_over_hemisphere(threadBuffer, cacheMiss, (Vec3) {.x = 1, .y = 1, .z = 1}, shiftedPoint, surfaceNormal, 1);

}



/*
 * returns a translation matrix that will translate the given origin to (0, 0, 0)
 */

static Mat4 get_translation_matrix(Mat4 *viewMatrix){
    Mat4 translationMatrix = Identity4;
    translationMatrix.data[0][3] = viewMatrix->data[0][3];
    translationMatrix.data[1][3] = viewMatrix->data[1][3];
    translationMatrix.data[2][3] = viewMatrix->data[2][3];
    return translationMatrix;
}



static Triangle create_triangle(Mat4 *combinedTransformation, Vec3 *vertices, size_t index){
    Triangle triangle;
    triangle.points[0] = transform_point(combinedTransformation, vertices[(index * 3) + 0]);
    triangle.points[1] = transform_point(combinedTransformation, vertices[(index * 3) + 1]);
    triangle.points[2] = transform_point(combinedTransformation, vertices[(index * 3) + 2]);

    triangle.edge1 = Vec3MinusVec3(triangle.points[1], triangle.points[0]);
    triangle.edge2 = Vec3MinusVec3(triangle.points[2], triangle.points[0]);
    triangle.normal = CrossProduct3(triangle.edge1, triangle.edge2);

    if (!(triangle.normal.x || triangle.normal.y || triangle.normal.z)){
        Vec3 v1 = vertices[(index * 3) + 0];
        Vec3 v2 = vertices[(index * 3) + 1];
        Vec3 v3 = vertices[(index * 3) + 2];
        printf("Triangle %lu is malformed. Its vertices are:\n", index);
        Vec3Print(v1);
        Vec3Print(v2);
        Vec3Print(v3);
        exit(EXIT_FAILURE);
    }
    return triangle;
}

/*
 * takes the triangles read from the obj file and calculates edge vectors and normals
 */

static void init_triangles(MemoryArena *arena, Vec3 *vertices, Vec3 *vertexNormals, Mat4 *trans, Mat4 *rot){
    triangles = Arena_Allocate(Triangle , (sizeof *triangles) * numTriangles, arena);
    emissiveTriangles = Arena_Allocate(Triangle , (sizeof *emissiveTriangles) * (objData.numEmissives + objData.numPortals), arena);
    portalTriangles = Arena_Allocate(Triangle , (sizeof *portalTriangles) * objData.numPortals, arena);


    Mat4 combinedTransformation;
    Mat4xMat4(trans, rot, &combinedTransformation);

    size_t lightCount = 0;
    for (size_t i = 0; i < numTriangles; i++){

        Triangle triangle = create_triangle(&combinedTransformation, vertices, i);
        triangles[i] = triangle;

        vertexNormals[(i * 3) + 0] = transform_point(rot, vertexNormals[(i * 3) + 0]);
        vertexNormals[(i * 3) + 1] = transform_point(rot, vertexNormals[(i * 3) + 1]);
        vertexNormals[(i * 3) + 2] = transform_point(rot, vertexNormals[(i * 3) + 2]);

        MtlMaterial *material = &objData.materials[objData.materialIndices[i]];

        if (material->luminosity > 0){
            emissiveTriangles[lightCount++] = triangle;
        }
    }
    assert(lightCount == objData.numEmissives);

    for (size_t i = 0; i < objData.numPortals; i++){
        Triangle triangle = create_triangle(&combinedTransformation, objData.portalVertices, i);
        portalTriangles[i] = triangle;
        emissiveTriangles[lightCount++] = triangle;
    }
    assert(lightCount == objData.numEmissives + objData.numPortals);

}

















