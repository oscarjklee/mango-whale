/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */


#include "config_reader.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

enum offsetNames {obj, dir, output, width, height, minSamples, maxSamples, depth, tolerance, gradients, minDistance, maxDistance, samplesLevel1, samplesLevel2, aoRender};
static size_t offsets[] = {9, 10, 12, 13, 14, 12, 12, 10, 10, 16, 19, 19, 22, 22, 25};

static size_t get_data_start(char *line, size_t offset);


static size_t get_data_start(char *line, size_t offset){
    size_t i = offset;
    while (line[i] == ' ' || line[i] == '\t') i++;

    if (line[i] == '\n' || line[i] == '\0'){
        return (size_t) -1;
    }

    return i - offset;
}

static size_t get_line_start(char *line, FILE *file, size_t offset){
    if (fgets(line, 512, file) == NULL) {
        return (size_t) -1;
    }
    return get_data_start(line, offset);
}

void create_config_file(char *filename){
    char *text = "obj file:\ndirectory:\noutput file:\nscreen width:\nscreen height:\nmin samples:\nmax samples:\nmax depth:\n"
                 "tolerance:\ncache gradients:\nmin cache distance:\nmax cache distance:\nlevel 1 light samples:\nlevel 2 light samples:\n"
                 "ambient occlusion render:\n";

    FILE *new = fopen(filename, "w");
    fprintf(new, "%s", text);
    fclose(new);
}


uint32_t parse_config_file(char *filename, ConfigParameters *configParameters) {

    FILE *file = fopen(filename, "r");
    if (file == NULL) {
        goto fail;
    }


    char line[1024] = {0};
    size_t start, offset;

    //improve readability
#define readLine() if ((start = get_line_start(line, file, offset)) == (size_t) -1) goto fail;


    offset = offsets[obj];
    readLine();
    assert(line[offset + start] != ' ');
    line[strlen(line) - 1] = '\0';
    strncpy(configParameters->objFile, line + offset + start, 1024 - start - offset);

    offset = offsets[dir];
    readLine();
    line[strlen(line) - 1] = '\0';
    strncpy(configParameters->directory, line + offset + start, 1024 - start - offset);

    offset = offsets[output];
    readLine();
    line[strlen(line) - 1] = '\0';
    strncpy(configParameters->outputFile, line + offset + start, 1024 - start - offset);

    offset = offsets[width];
    readLine();
    configParameters->screenWidth = (uint32_t) atoi(line + offset + start);

    offset = offsets[height];
    readLine();
    configParameters->screenHeight = (uint32_t) atoi(line + offset + start);

    offset = offsets[minSamples];
    readLine();
    configParameters->minSamples = (uint32_t) atoi(line + offset + start);

    offset = offsets[maxSamples];
    readLine();
    configParameters->maxSamples = (uint32_t) atoi(line + offset + start);

    offset = offsets[depth];
    readLine();
    configParameters->maxDepth = (uint32_t) atoi(line + offset + start);

    offset = offsets[tolerance];
    readLine();
    configParameters->tolerance = (float) atof(line + offset + start);

    offset = offsets[gradients];
    readLine();
    configParameters->gradients = (uint32_t) atoi(line + offset + start) == 1;

    offset = offsets[minDistance];
    readLine();
    configParameters->minDistance = (float) atof(line + offset + start);

    offset = offsets[maxDistance];
    readLine();
    configParameters->maxDistance = (float) atof(line + offset + start);

    offset = offsets[samplesLevel1];
    readLine();
    configParameters->lightSamplesLevel1 = (uint32_t) atoi(line + offset + start);

    offset = offsets[samplesLevel2];
    readLine();
    configParameters->lightSamplesLevel2 = (uint32_t) atoi(line + offset + start);

    offset = offsets[aoRender];
    readLine();
    configParameters->ambientOcclusionRender = (uint32_t) atoi(line + offset + start) == 1;

    if (configParameters->ambientOcclusionRender) configParameters->maxDepth = 2;

    fclose(file);
    return 0;

    fail:
    fclose(file);
    return 1;

}



