/*
Copyright (C) 2023  Oscar Lee


This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public
License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied
warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.
If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MANGO_WHALE_CACHE_H
#define MANGO_WHALE_CACHE_H

#include "linear_algebra.h"
#include "memory.h"

void init_irradiance_cache(MemoryArena *memoryArena, Vec3 minBound, Vec3 maxBound, Vec3 origin, float tolerance, uint32_t gradient, uint32_t depth, float minDistance, float maxDistance);
Vec3 search_cache(Vec3 point, Vec3 normal, uint32_t level);
void insert_record(Mat3 *translationGradient, Mat3 *rotationGradient, Vec3 position, Vec3 normal, Vec3 val, float harmonicMean, uint32_t level);
void set_tolerance(float tolerance);
void print_cache_statistics();


#endif //MANGO_WHALE_CACHE_H
