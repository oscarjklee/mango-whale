[[_TOC_]]


# Mango Whale
Mango Whale is a CPU based non real time path tracer. It is written in C and has no external dependencies other than the C standard library, OpenMP and OpenGL.

![image info](readme_images/cornell_example.png)
A sample Cornell Box render.

## Usage

Mango Whale is tested and developed on FreeBSD and Linux operating systems with GCC and Clang. It's written in standard compliant C23 so it *should* work on Windows and other operating systems - but there are no guarantees. 

To build: 
```
cmake -DCMAKE_BUILD_TYPE=[No]Debug .
make
```

To run:
```
./bin/mango_whale [config_file]
```
``[config_file]`` is a file containing data about the scene - ``example_config.txt`` is a sample config file for a basic cornell box render.

A window showing the scene described in the config file will open. Use WASD keys to move the camera around the scene, then press the R key to begin rendering. When rendering is complete, the rendered image will be displayed on the screen and written to a file. 

After the scene has been rendered, pressing the M key will change the render mode. There are 4 modes - combined lighting, direct lighting only, indirect lighting only and cache record positions. The first 3 show different lighting, and the final one shows the position of the cache records (each cache record is represented by a black dot).

## Current Status
As of July 26th 2024 Mango Whale has the following features:
- BVH generation via Approximate Agglomerative Clustering
- Ability to read from wavefront .obj and .mtl files
- Next Event Estimation
- Russian Roulette Path Termination
- Adaptive Sampling
- Mirror reflections
- Support for transparent materials
- Parallelisation through OpenMP
- Area light support
- Basic scene viewer and moveable camera
- Irradiance Caching with gradients

## Fututre Goals

There are a number of additional features that I want to add to Mango Whale. These are the main ones I am working on / planning:

- Support for more BRDFs 
- Support for more advanced texturing, eg normal mapping
- Refraction and the Fresnel Effect
- Improved Irradiance Caching
- Biderectional Path Tracing
- Improved sampling strategies
- Ambient Occlusion rendering
- Position aware light sampling

The fields of Ray/Path tracing and computer graphics is incredibly deep, and as such there is a huge range of possible features to implement, and these features are not exaustive and only represent short/medium term goals. Additionally, because I am currently a masters student in university development is likely to be sporadic - expect long periods of little activity as I focus on academic work, followed by periods of significant development when I have the time.


# Documentation


## Config File
Mango Whale is dependent on a lot of different user inputs and parameters - enough to make passing them to the program through the terminal not very user friendly. To fix this, Mango Whale reads a config file on startup, and the parameters for the program are read from it.


### Structure
The structure of the config file is as follows:

```
obj file:
directory: 
output file: 
screen width:
screen height:
min samples:
max samples:
max depth:
tolerance:
cache gradients:
min cache distance:
max cache distance:
level 1 light samples:
level 2 light samples:
ambient occlusion render:
```

`obj file` is the name of the obj file that is to be rendered (note that this is just the *name* of the file - not its path). Expected format is a string.

`directory` is the path to the directory/folder containing the obj file. All files referenced in the obj file (eg mtl file) are relative to this directory. Expected format is a string.

`output file` is the name of a file that the rendered image should be saved to. Note that this file is not relative to the directory previously specified. Expected format is a string.

`screen width` and `screen height` are the width and height of the screen respectively, measured in pixels. These fields determine the dimensions of the render. Expected format is positvie integers.

`min samples` and `max samples` control the minimum and maximum number of rays cast per pixel. If irradiance caching is enabled, then these refers to the number of rays cast for each record. If irradiance caching is disabled, then these refer to the number of paths cast for each pixel. Expected format is positive integers.

`max depth` represents the maximum number of bounces a path can take. Note that paths can still terminate early due to things like Russian Roulette. Expected format is a positive integer.

`tolerance` controls the error allowed in irradiance caching. **Setting this value to 0 disables irradiance caching**. Expected format is a positive floating point value. 

`cache gradient` is a boolean flag that determines whether or not gradients are used in irradiance cache interpolation. A value of 1 corresponds to gradients being enabled, and any other value corresponds to them being disabled. Expected format is either 0 or 1. 

`min cache distance` and `max cache distance` are heuristics that control the spacing of irradiance cache records. Expected format is positive floating point values.

`level 1 light samples` and `level 2 light samples` control the number of direct illumination samples taken at different levels in the path. Expected format is positive integers.

`ambient occlusion render` is a boolean flag that controls whether or not ambient occlusion is rendered instead of global illumination (GI). If this is set to 1 then ambient occlusion will be rendered. If it is set to any other value then full global illumination will be rendered instead. Expected format is either 0 or 1.


### Recommended Values
This section discusses in more detail what the parameters actually mean, and provides information to take into account when setting values.

#### Min and Max Samples
Where appropriate, Mango Whale makes use of adaptive sampling to improve performance. If the values of rays over a region are all similar (specifically, if the variance is below a threshold (0.05)) then ray casting can be stopped early, saving time. `min samples` represents the minimum number of rays that need to have been cast before a possible early termination, while `maximum samples` represents the maximum number of rays that will be cast before casting gets stopped (regardless of variance). 

It is recommended that `min samples` is at least 32 - any lower runs the rist of samples having low variance due to a quirk in sampling, instead of actual convergence.

If irradiance caching is enabled, then `max samples` is recommended to be at least 1024 for a full GI render. This is because the calculated value is going to be reused potentially millions of times, so it is essential that an incredibly accurate value is calculated. For an ambient occlusion render a lower value can be used, eg 128.

#### Max Depth
Max depth refers to the number of bounces a path will take before it gets terminated. If we wanted *perfect* accuracy we would need to track a potentially infinite number of bounces, but this is of course impossible. If the chosen value is too low, then areas not receiving direct illumination will appear too dark. If the value is too high then computation will be wasted on deep bounces that don't noticeably contribute to the final image. 

The needed value is highly scene dependent - a well lit scene (eg outdoors in the sun) could need as few as 2 bounces to produce a good image. However a scene with a lot of translucent or reflective materials may need far more - possibly up to 8 or even 16. For a standard scene 4 bounces tends to produce good quality image while avoiding excessive computation.

#### Tolerance
Tolerance controls the accepted interpolation when reusing records in the irradiance cache. A higher value results in a faster render time, but reduces the image quality. Choosing an appropriate value is incredibly important and can vary from scene to scene. For some scenes, a value of 0.5 can produce a good image. Other may require a value as low as 0.1, while some work fine with a value of 1. At the end of the day, choosing the right value may require trial and error. It's recommended to start with a high tolerance and work downwards, stopping once an acceptable tradeoff has been reached between image quality and render time.

#### Cache Gradients
Cache gradients are matrices that measure how the calculated irradiance for a record changes as the record moves and rotates. Their usage significantly improves interpolation accuracy and is highly recommended. However, using gradients requires stratified sampling (as opposed to random sampling). In addition to this, the gradients are highly sensitive to noise - so a large number of samples are needed or there may be visible artifacts (eg 1024+). Another consequence of the sensetivity to noise is that gradients cannot be used for the final bounce - by its nature the final bounce will be very noisy, so gradients are disabled for it.

#### Min and Max Cache Distance
Some records may end up with incredibly small or incredibly large validity regions (regions where they can be interpolated from). This is bad for obvious reasons - if the validity region is smaller than an individual pixel then it's somewhat pointless, and the validity region could be so large that geometric features get missed. To fix this, we clamp the record's measure of distance (in our case minimum ray length) from above and below. Setting appropriate values is important - if `min cache distance` is set too low then records can end up clumped together where only 1 is needed, and if its too high there can be visible error in geometrically complex areas. The reverse is true for `max cache distance`.

Regardless of values chosen, the distances fields are global. They are applied the same to a record close to the camera and to a record far away. This isn't ideal, as it leads to a clustering of records far away from the camera (where detail is less noticeable) and sparsity close to the camera (where detail is highly noticable). To mitigate this, the scaling gets multiplied by the distance the record is from the camera. This way, far away records are forced further apart, which significantly reduces render time, while nearby records are more dense, which is important for visual accuracy. 

Krivanek et al recommend setting the maximum spacing to be approxiamtely 64x the minimum spacing.

#### Level 1 and Level 2 Light Samples
These parameters only apply when irradiance caching is enabled. When regular path tracing is being performed, instead increase/decrease the number of paths in the `min samples` and `max samples` paramters to change the number of light samples taken.

While previous parameters are focused on indirect illumination, these parameters are focused on direct illumination. It's important that light sources are sampled frequently enough to avoid heavy noise. How many samples are needed varies from scene to scene, but as a general rule scenes with more lights or complex occlusion patterns need more samples. An complex occlusion pattern refers to a light source which is covered by a wire mesh - even though a point is in illumination, a large amount of samples of the light will return that the point is in shadow.

`level 1 light samples` controls how many light samples are taken at the first bounce. The direct lighting here has the most significant visual impact out of all bounces, so this value should be relatively high. Increasing this value will cause extra primary rays to be cast per pixel, so this not only improves direct lighting but also aliasing.

`level 2 light samples` controls how many light samples are taken at the second bounce. While not as important at the first bounce, it's important that irradiance caching has as little noise as possible (especially with gradients) so ensuring an appropriate number of samples are taken is still important.

#### Ambient Occlusion Rendering
Instead of rendering full GI, Mango Whale supports rendering ambient occlusion only. Ambient occlusion is a technique that seeks to provide a sense of depth for indirect lighting. While not phyiscally accurate (it doesn't actually measure how much illumination the point receives, nor the colour of the illumination) it still significantly improves the visual quality. The main advantage of it is that it is massively cheaper and quicker than calculating GI. The ambient occlusion (Ka) at a point is measured by casting a number of rays over its hemisphere, and assigning each ray a value of 1 if there is no intersection within a certain distance, and a value of 0 if there is. Ka is then calculated as the average value of all rays. 

Because ambient occlusion is based solely on a points nearby geometry, it only requires 1 bounce and changes very little over a surface - making it ideal for irradiance caching. 

When doing an ambient occlusion render, it is recommended to set a very low tolerance (eg 0.005) and high cache spacing boundaries (eg 4 and 256).

## High Level Overview

The following pseudocode describes the control flow of Mango Whale from a high level perspective:

```
load_obj_file();
create_bvh();
render_scene();
transform_triangles();

for each pixel:

    for bounce in depth:
        cast_ray();
        sample_lights();

        if irradiance cache enabled:
            search_cache();
            if cache_miss:
                sample_hemisphere();
                add_to_cache();
            else:
                use_cached_values();
        else:
            sample_hemisphere

    write_value_to_framebuffer();

render_results();
```

### OBJ Reading
Mango Whale reads scene data from files using the OBJ and MTL file format. The parsing of the OBJ file takes place in `obj_loader.c`. Mango Whale uses the standard OBJ format, but has minor differences for the MTL format. Any emissive triangle is defined as a light source, and there is an additional (optional) brightness field denoted by `Lu`, which sets the brightness of the light. If the `Lu` field is not provided then the brightness is assumed to be 1. 

The following structs are used to store scene data:

```
typedef struct MtlMaterial{
    uint8_t *texture;
    Vec3 diffuse;
    Vec3 ambient;
    Vec3 specular;
    Vec3 emissive;
    Vec3 transmissionFilter;
    float alpha;
    float transparency;
    float refractionIndex;
    float reflectivity;
    float intensity;
    uint32_t textureWidth;
    uint32_t textureHeight;
} MtlMaterial;


typedef struct EmissiveTriangle{
    size_t triangleIndex;
    float weighting;
} EmissiveTriangle;


typedef struct ObjData{
    Vec3 *vertices;
    Vec3 *vertexNormals;
    Vec2 *textureCoords;
    MtlMaterial *materials;
    EmissiveTriangle *emissiveTriangles;
    size_t *materialIndices;
    size_t numVertices;
    size_t numEmissives;
}ObjData;
```

The `ObjData` struct stores all scene information. The vertices for triangle `i` are `vertices[(3 * i) + 0]`, `vertices[(3 * i) + 1]` and `vertices[(3 * i) + 2]`. The vertex normals and texture coordinates are accessed in a similar pattern. The material data for triangle `i` is accessed by `materials[materialIndices[i]]]`.

The `MtlMaterial` data is mostly self explanatory. `texture` is an array of RGB values, with each R/G/B taking up 1 byte. `transmissionFilter` determines the colour that transparent objects allow through - eg if it's set to `(1, 0, 0)` then only red light is allowed through. 

For the `EmissiveTriangle` struct, `triangleIndex` can be used to get the data about the triangle itself (as previously described). `weighting` is more interseting. 

#### Light Weighting
In Mango Whale each light has a weighting. Lights with higher weighting are sampled more often when calculating direct illumination for a point. The weighting for light `n` in the `emissiveTriangles` array is given by the sum of all previous weightings. 

This allows for an efficient O(log(n)) algorithm to randomly sample lights based on weighting. A random number `x` is generated such that `0 <= x <= emissiveTriangles[numEmissives - 1].weighting`. We then perform a binary search over the `emissiveTriangles` array, stopping when we have found the light `i` such that `emissiveTriangles[i].weighting <= emissiveTriangles[i + 1].weighting`. Light `i` is then sampled by choosing a random point on its surface to cast a ray to.

The only thing left to do is to choose a method for calculating the weighting of each individual light. For now, Mango Whale simply sets `weighting` equal to the light's area multiplied by its `intensity`. This means that brighter and larger lights are more likely to be sampled - which can drastically improve image quality in scenes with many small, weak lights and only a few large and bright lights. However, an issue with this system is that it doesn't take into account the relative location of lights - nearby lights are likely to be more important than far away lights, even if they are weaker. PBRT discusses light bounding volume hierarchies here: https://pbr-book.org/4ed/Light_Sources/Light_Sampling. I intend to implement a feature like this at some point.

### BVH Creation

We create a Bounding Volume Hierarchy containing the scene geometry in `bounding_volume_hierarchy.c`. This is done via the Approximate Agglomerative Clustering (AAC)algorithm, described in https://dl.acm.org/doi/abs/10.1145/2492045.2492054. The BVH creation is started by calling the fucntion:

`BvhNodePublic *create_bvh(Triangle *triangles, uint32_t numTriangles, MemoryArena *arena, uint32_t *arraySize);`

`triangles` is an array of all triangles in the scene, of legnth `numTriangles`. 

`arena` is the arena on which to store the generated BVH, note that while creating the BVH an auxiliary arena will be used for internal data structures, which will be destroyed before returning. 

`arraySize` is a pointer to a location that can be used to store the size of the returned BVH.

The `BvhNodePublic` struct is defined as follows:

```
typedef struct BvhNodePublic{
    Vec3 bounds[2];
    union {
        uint32_t childIndex;
        uint32_t triangleIndex;
    };
    uint32_t leaf;
} BvhNodePublic;
```

Each `BvhNodePublic` object represents either an axis aligned bounding box (AABB) or a triangle primitive.

`bounnds` contains the bounds of the AABB, such that `bounds[0].x < bounds[1].x`, `bounds[0].y < bounds[1].y` and `bounds[0].z < bounds[1].z`.

`childIndex` contains the index of 1 of the node's 2 children (discussed in more detail later).

`triangleIndex` contains the index within the `triangles` array of the triangle which the node represents, if the node represents a triangle and not an AABB.

`leaf` is a boolean variable, set to 0 if the node represents an AABB and 1 if it represents a triangle.

`create_bvh()` returns an array of BVH nodes. The BVH is fundamentally a tree where each non-leaf node has 2 children, and it is flattened according to the following scheme:

For a given node `bvh[n]`, its left child is stored at `bvh[n + 1]` and its right child is stored at `bhv[bvh[n].childIndex]`. By flattening the tree this way we are able to avoid cache misses from half the nodes we visit when traversing the BVH. The size of `BvhNodePublic` is 32 bytes, so on any modern system this should fit nicely into cache lines.

#### Approximate Agglomerative Clustering
As mentioned earlier, the BVH is constructed using AAC. The linked paper provides a detailed explanation of the algorithm and discusses performance, so this section is just a high level overview. 

First, *morton codes* are calculated for each triangle in the scene. A morton code is calcualted by interleaving the most significant bits of the `(x, y, z)` coordinates of the centre of a triangle into a 4 byte unsigned integer. We then sort all of the triangles based on their morton codes. We do this using the `qsort` function from the C standard library. Using a radix sort instead should be quicker, but this is nowhere near a bottle neck for overall performance so we do not bother. 

Next, each triangle is packaged into an AABB node to allow for simpler BVH building - this leaves us with an array of nodes each with 1 triangle inside. We then partition the array of nodes into small sequential clusters based on morton codes. Each cluster is reduced to a set size using a greedy algorithm based on the surface area heuristic (SAH). We check each node in a cluster against each other node in the cluster, each time calculating what the surface area of the new node would be if the 2 nodes were merged. We then merge the two nodes that produce the smallest surface area. This process continues until the number of nodes in a cluster is below the theshold. 

We then merge each cluster with its neighbour, and repeat the merging process. This continues until we are left with only one cluster. We then merge all of the nodes in this cluster together into 1 (again using the SAH). After this is complete, we flatten the BVH from a tree based structure into an array to allow for better cache access patterns.

### Scene Rendering
Mango Whale uses OpenGL to render a preview of the scene. Triangle data (vertices, normals and texture coordinates) are buffered onto the GPU and view and projection matrices are calculated. When calculating lighting, each point is shaded by the absolute value of the dot product of the surface normal and `(1, 1, 1)`. This just helps to give some sense of depth and direction - it doesn't attempt to be physically plausible. After a frame has been rendered, we check if the WASD, shift, control or R keys have been pressed. WASD is used to move the camera, shift and control increase/decrease camera speed respectively and the R key causes ray tracing to begin.

### Triangle Transformations
After the R key has been pressed in the scene rendering, Mango Whale takes the view matrix that was used to render the scene, and transforms all triangles in the scene by that matrix. 


### Writing Framebuffer To File

After all rays have been cast and the Framebuffer is updated, the contents of the framebuffer are written to a file (default `images/render.ppm`) via the `write_image_to_ppm()` functon in `ppm_image.c`. The ppm image format was chosen because it's simple, although it would not be difficult to integrate an existing image generator for a different file type (eg png, jpeg).




## Irradiance Caching

Irradiance caching is a technique that dramatically improves the speed of calculating indirect lighting. As indirect lighting calcultion can often be where the majority of processing time is spent this can result in a huge overall speedup.

Irradiance caching is motivated by the observation that indirect lighting changes slowly over diffuse surfaces - as such we can reuse previously calculated indirect lighting (stored as irradiance) from a nearby point instead of calculating it all again.

The implementation of irradiance caching follows the implemnentation described in the book "Practical Global Illumination with Irradiance Caching" (Jaroslav Krivánek and Pascal Gautron, 2009). The system is implemented in the `cache.c` file. There are multiple levels of caches implemented, each level corresponds to the number of bounces a ray has taken.

The most important data structure is the cache itself. It needs to allow for fast lookups - if the lookups take too long, we might as well manually calculate irradiance. As such, a multi-reference octree is used to store all cache records. An octree is used because it allows for effective space partitioning, so we only search for records nearby the current position and what makes it multi-reference is that a reference to each record is inserted multiple times. For each node in the tree in which a record is likely to be valid, it gets inserted. This means that when searching the cache only 1 node needs visiting for each layer.

The Cache and CacheNode data strcutures are defined as follows:

```
typedef struct CacheNode{
    struct CacheNode *children[8]; 
    CacheRecord **records;
    Vec3 bounds[2];
    size_t numRecords;
    size_t recordCapacity;
} CacheNode;


#define RECORDS_ARRAY_DEPTH 20

typedef struct Cache{
    CacheNode cacheRoot;
    CacheRecord *recordsArray[RECORDS_ARRAY_DEPTH];
    size_t recordsArraySize;
    size_t recordsArrayLevel;
    size_t recordsArrayCapacity;

    atomic_uint_fast64_t numRecords;
    atomic_uint_fast64_t numHits;
    atomic_uint_fast64_t numMisses;

    float errorTolerance;
    uint32_t useGradients;

} Cache;
```

Each cache node covers a cube bounded by the two `bounds` vectors. `bounds[0]` is the bottom front left corner and `bounds[1]` is the top back right corner. Each cachenode has 8 children, each of which occupies 1 corner of itself. These children are only created when a record needs to be inserted into them.

`records` is an arrary of pointers to cache records. Its size is `numRecords` and its capacity is `recordCapacity`.

Each cache stores its root and a 2 dimensional array of cache records. Once the array representing first index of `recordsArray` is full, an array in the second index will be allocated. This array will be twice the size of the first array. The process repeats once that array is full too. The advantage of this system instead of resizing the array with `realloc` is that pointers to a record are always valid, so after resizing the array we don't need to go and update the references stored in the cache nodes. 

`errorTolerance` describes how wide the acceptance region should be when searching the cache - a bigger tolerance will lead to more cache hits (and therefore a faster render time) but produce a lower quality image. `useGradients` is a boolean varaible determining whether interpolation gradients should be used when interpolation a cache record. Interpolation gradients greatly improve quality - but they are very susceptible to noise. As such, it is not suitable to use gradients in the final cache level.

A cache record itself is represented by this data structure:

```
typedef struct CacheRecord{
    Mat3 translationGradient;
    Mat3 rotationGradient;
    Vec3 position;
    Vec3 normal;
    Vec3 irradiance;
    float minDistance;
    float minDistanceClamped;
    float minBound;
    float maxBound;
} CacheRecord;
```

`translationGradient` and `rotationGradient` are previously mentioned gradients. When applied to the stored `irradiance` they improve interpolation quality. Their derivation and calculation is lengthy, so for reasons of brevity it isn't described here - however the previously mentioned book by Krivánek et al provides a thorough explanation. `position` and `normal` are the position and normal of the cache record. `minDistance`, `minDistanceClamped`, `minBound` and `maxBound` are heuristics that measure how far away the record is from surrounding geometry - if it's very close then the record's reliability is likely limited to just its immediate vicinity.

The `search_cache` function searches the cache. If a suitable record for interpolation is found then it returns the interpolated irradiance. If no record is found then it returns a `Vec3` equal to `{.x = -1, .y = -1, .z = -1}`. In which case, ray tracing is performed to generate a record, and it is inserted into the cache through the `insert_record` function. 

### Multi Pass Rendering
A quirk of irradiance caching is that it requires 2 render passes to produce an acceptable image. If just 1 render pass is performed then the first pixels in an area being rendered will have very few records to inerpolate from, resulting in lower quality interpolation. Meanwhile, the last pixels in an area to be rendered will have a full cache to inerpolate from - resulting in higher quality interpolation. This difference produces noticeable banding within an image. To avoid this, 2 render passes are done. The first is low resolution (1 ray per pixel) and just builds the irradiance cache (no direct lighting calculated). The 2nd then performs a high resolution render, taking into account direct lighting and other effects. 

![image info](readme_images/conference_room_example.png)
A sample conference room render. The image was generated using irradiance caching.


### Ambient Occlusion Rendering
Despite its performance improvements, irradiance caching has several significant weaknesses. It is highly sensitive to noise, moreso than the illumination itself and relies on the assumption that indirect illumination varies little over distance. When rendering ambient occlusion, both of these issues are not present. By its nature, ambient occlusion produces very little noise and changes incredibly little over distance and rotation. As such, irradiance caching can be applied to ambient occlusion rendering to great effect.

![image info](readme_images/ambient_occlusion.png)
A sample conference room render. The image was generated using irradiance caching, and took significantly less time than a full global illumination render. 


## Memory Management


Mango Whale exclusively and extensively uses arena based memory management. An arena object (`MemoryArena`) is defined as follows:

```
typedef struct MemoryArena{
    void *data; 
    struct MemoryArena *childArena;
    uint64_t capacity; 
    uint64_t size;
} MemoryArena;
```

`data` is a pointer to the start of the arena's data.

`childArena` is a pointer to the arena's child arena. If the arena has no child arena then `childArena` is set to `NULL`. This is discussed further later.

`capacity` is the maximum capacity of the arena in bytes.

`size` is the amount of data currently stored in the arena. `size` is always less than or equal to `capacity`. 

An arena is created through the following function:

`MemoryArena *create_arena(uint64_t size)`


### Arena Memory Alloction

Memory is allocated from a given arena through the following function:
 
`void *allocate_arena_memory(size_t alignment, MemoryArena *arena, uint64_t size);`

`alignment` is the value that the allocated memory needs to be alligned to.

`arena` is the arena to be allocated from.

`size` is the size of the allocation, in bytes.

If `size` is greater than the remaining memory in the arena, a child arena will be created to allow the allocation to succeed.

#### Arena Memory Reallocation

An buffer stored on an arena can be "realloced" through the following function:

`void *arena_realloc(size_t alignment, MemoryArena *arena, uint64_t new_size, uint64_t old_size, void *old_ptr)`

### Arena Destruction

Once all objects stored on an arena are no longer needed, an arena can be destroyed through the `delete_arena()` function. This function frees all resources associated with the arena and all of its children.



## Additional Data Structures

Mango whale has multiple additional data structures. Some of these are "private", meaning that they are encapsulated within a certain module, and are not exposed by any interaction with that module. An example of this is a node for the unflattened BVH that is made before creating a flattened BVH. For brevity are not documented here, but can be found documented in the code.

### Vectors
2D, 3D, and 4D vectors are defined in `linear_algebra.h` as follows:

```
typedef struct Vec2{
    union {
        struct {
            float x;
            float y;
        };
        float data[2];
    };
} Vec2;


typedef struct Vec3{
    union {
        struct {
            float x;
            float y;
            float z;
        };
        float data[3];
    };
} Vec3;

typedef struct Vec4{
    union {
        struct {
            float x;
            float y;
            float z;
            float w;
        };
        float data[4];
    };
} Vec4;
```

Each component within a vector can be accessed through a specific field (eg `foo.x`) or through an array (eg `foo[0]`). This syntax sugar is convenient at times.

### Matrices

3x3 and 4x4 matrices are defined in `linear_algebra.h` as follows:

```
typedef struct Mat3{
    float data [3][3];
} Mat3;

typedef struct Mat4{
    float data [4][4];
} Mat4;
```

### Triangles

The triangle object is defined in `common_includes.h` as follows:

```
typedef struct Triangle{
    union {
        Vec3 points[3];
        Mat3 data;
    };
    Vec3 normal;
    Vec3 edge1;
    Vec3 edge2;
} Triangle;
```

The triangles vertices are stored in `points`, or alternatively as a 3x3 matrix `data`. Being able to treat the triangle's vertices as a matrix makes some operations simpler as it allows us to use a single matrix multiplication instead of multiple vector multiplications.

`normal` contains the un normalised normal of the triangle - calculated through the cross product of its 2 edges. It is used in ray-triangle intersection testing

`edge1` is the vector given by `points[1] - points[0]`.

`edge2` is the vector given by `points[2] - points[0]`.


### Rays

The ray object is defined in `ray.h` as follows:

```
typedef struct ray{
    Vec3 origin;
    Vec3 direction;
    Vec3 inverse;
} Ray;
```
The ray's origin and direction in camera space are given by `origin` and `direction`.

`inverse` is the component wise inverse of `direction`, ie `inverse = (1 / direction.x, 1 / direction.y, 1 / direction.z)`. This inverse is used every time we perform a ray-AABB intersection test for this ray, so only performing 1 set of division is more efficient than potentially hundreds or more per ray.


### Lights

The light object is defined in `light.h` as follows:

```
typedef struct Light{
    Vec3 pos;
    Vec3 colour;
    float strength;
} Light;
```

`pos` is its position in camera space.

`colour` is its RGB colour, with each channel in the range 0 to 1.

`strength` is the light's intensity - a larger value will make the light brighter and cause its light to reach further.



