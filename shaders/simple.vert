#version 430

layout (location = 0) in vec3 positions;
layout (location = 1) in vec3 normals;
layout (location = 2) in vec2 texCoords;

out vec3 vertexNormal;
out vec2 vertexTextureCoord;

uniform mat4 matrix;

void main(){
    gl_Position = matrix * vec4(positions, 1);
    vertexNormal = normals;
    vertexTextureCoord = texCoords;
}