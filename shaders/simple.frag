#version 430

in vec3 vertexNormal;
in vec2 vertexTextureCoord;

layout (binding = 0) uniform sampler2D rgbaTexture;

uniform bool useTexture;
uniform vec3 diffuseColour;

out vec4 colour;


void main(){
    vec2 sampleCoords = vertexTextureCoord;
    sampleCoords.y = 1 - sampleCoords.y;
    // sampleCoords.x = 1 - sampleCoords.x;
    
    if (useTexture){
        colour = texture(rgbaTexture, sampleCoords);
        if (colour.w < 0.005) discard;
    }
    else{
        colour = vec4(diffuseColour.xyz, 1);
    }

    float shade = dot(vertexNormal, normalize(vec3(1, 1, 1)));
    shade = abs(shade);
    colour.xyz *= shade;
}