#version 430

vec2 coords[6] = {

    vec2(-1.f, 1.f),  vec2(-1.f, -1.f), vec2(1.f, 1.f),
    vec2(-1.f, -1.f), vec2(1.f, 1.f),   vec2(1.f, -1.f)

};

void main(){
    vec2 pos = coords[gl_VertexID];
    gl_Position = vec4(pos.xy, 0.5f, 1.f);
}

