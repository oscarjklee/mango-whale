#version 430

layout (binding = 0) uniform sampler2D imageTexture;

uniform float screenWidth;
uniform float screenHeight;

out vec4 colour;

void main(){

    vec2 coords = gl_FragCoord.xy / vec2(1920, 1080);
    coords.y = 1 - coords.y;
    colour = texture(imageTexture, coords);
}